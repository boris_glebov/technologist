﻿namespace Hqub.Technologist.Math
{
    public static class TestMaterialMath
    {
        /// <summary>
        /// Расчет % содержания MgO
        /// </summary>
        /// <param name="volumeCaMg"></param>
        /// <param name="volumeCa"></param>
        /// <param name="titreMgO"></param>
        /// <param name="weightMgO"></param>
        /// <returns></returns>
        public static float CalcContentsMgO(float volumeCaMg, float volumeCa, float titreMgO, float weightMgO)
        {
            return TestProductMath.Round(((volumeCaMg - volumeCa) * titreMgO * 500) / weightMgO); 
        }

        /// <summary>
        /// Расчет % содержания CaO
        /// </summary>
        /// <param name="volume"></param>
        /// <param name="titre"></param>
        /// <param name="weight"></param>
        /// <returns></returns>
        public static float CalcContentsCaO(float volume, float titre, float weight)
        {
            return TestProductMath.Round((volume*titre*100)/weight);
        }

        /// <summary>
        /// Оценка удельной поверхности по методу Товарова
        /// </summary>
        /// <param name="k">постоянная прибора</param>
        /// <param name="y">удельный вес материала г/см.куб</param>
        /// <param name="g">Навеска,г</param>
        /// <param name="n">вязкость воздуха при температуре опыта</param>
        /// <param name="t">время снижения уровня жидкости</param>
        /// /// <param name="v">Объем cлоя</param>
        /// <returns></returns>
        public static float CalcSpecificSurfaceByTovarovMethod(float k, float y, float g, float n, float t,float v)
        {
            var m = ((v*y) - g)/(v*y);
            var s = (k/y)*
                    System.Math.Sqrt(System.Math.Pow(m, 3)/System.Math.Pow(1 - m, 2))*System.Math.Sqrt(1/n)*
                    System.Math.Sqrt(t);

            return TestProductMath.Round(s);
        }

    }
}
