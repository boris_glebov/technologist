﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Technologist.Math
{
    public static class TestProductMath
    {
        public static float Round(double summa)
        {
            return (float) System.Math.Round(summa, 2);
        }

        /// <summary>
        /// Расчет влажности
        /// </summary>
        /// <param name="m0">масса тары(в нашем случае бюксы)</param>
        /// <param name="m1">масса навески без тары (навески, исх)</param>
        /// <param name="m2">масса после сушки (после сушки)</param>
        /// <returns></returns>
        public static float GetHumidity(float m0, float m1, float m2)
        {
            double divider = m2 - m0;
            if (divider == 0)
            {
                return 0;
            }
                double val = ((m1 - (m2 - m0))/(m2 - m0))*100;
                return Round(val);
            
        }

        /// <summary>
        /// Расчет плотности бетона во влажном состоянии
        /// </summary>
        /// <param name="length">длина</param>
        /// <param name="width">ширина</param>
        /// <param name="height">высота</param>
        /// <param name="mass">масса образца</param>
        /// <returns></returns>
        public static float GetWetDensity(float length, float width, float height, float mass)
        {
            double divider = width*height*length;
            if (divider == 0)
            {
                return 0;
            }
            double val = (mass/divider)*1000;
            return Round(val);
        }

        /// <summary>
        /// Расчет плотности бетона в сухом состоянии
        /// </summary>
        /// <param name="wetDensity">Плотность бетона во влажном состоянии</param>
        /// <param name="humidity">Влажность</param>
        /// <returns></returns>
        public static float GetDryDensity(float wetDensity, float humidity)
        {
            double divider = 1 + 0.01*humidity;
            if (divider == 0)
            {
                return 0;
            }
            double val = wetDensity / divider;
            return Round(val);
        }

        /// <summary>
        /// Предел прочности при сжатии
        /// </summary>
        /// <param name="dryDensity">плотность в сухом состоянии,</param>
        /// <param name="length">длина</param>
        /// <param name="width">ширина</param>
        /// <param name="conversionFactor">переводной коэффициент(зависит от настроек еденицы измерения)</param>
        /// <param name="correctionFactor">поправочный коэффициент</param>
        /// <param name="scaleFactor">масштабный коэффициент</param>
        /// <returns></returns>
        public static float GetCrushingStress(double dryDensity, float length, float width, float conversionFactor,float correctionFactor,float scaleFactor)
        {
            double divider = length*width;
            if (divider == 0)
            {
                return 0;
            }

            double val = (dryDensity*correctionFactor*scaleFactor*conversionFactor)/(length*width);
            return Round(val);
        }

        /// <summary>
        ///  среднеквадратичное отклонение прочности либо плотности бетона в партии
        /// </summary>
        /// <param name="swingStrength">размах прочности или плотности бетона в партии (разница между максимальным и минимальным значением предела прочности или плотности при сжатии образцов партии)</param>
        /// <param name="factorNumberSamples">коэффициент, зависящих от количества образцов, испытанных для данной партии(класс в NSI NumberSamplesFactor)</param>
        /// <returns></returns>
        public static float GetDeviationStrength(float swingStrength, float factorNumberSamples)
        {
            if (factorNumberSamples == 0)
            {
                return 0;
            }

            double val = swingStrength / factorNumberSamples;
            return Round(val);
        }

        /// <summary>
        /// Текущий коэффициент вариации (по прочности) или (по плотности) в партии
        /// </summary>
        /// <param name="deviationStrength">среднеквадратичное отклонение прочности или плотности бетона в партии</param>
        /// <param name="averageCrushingStress">среднее значение прочности или плотности среди образцов партии</param>
        /// <returns></returns>
        public static float GetCoefficientVariationStrength(float deviationStrength, float averageCrushingStress)
        {
            if (averageCrushingStress == 0)
            {
                return 0;
            }

            double val = (deviationStrength / averageCrushingStress) * 100;
            return Round(val);
        }
       
    }
}
