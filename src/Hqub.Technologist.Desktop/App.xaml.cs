﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Threading;
using Hqub.Technologist.Desktop.Localization;
using Telerik.Windows.Controls;



namespace Hqub.Technologist.Desktop
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly Bootstrap _bootstrap = new Bootstrap();

        public App()
        {
            LocalizationManager.Manager = new LocalizationManager()
            {
                ResourceManager = GridViewResources.ResourceManager
            };
            
            DispatcherUnhandledException += App_DispatcherUnhandledException;
            Dispatcher.UnhandledException += Dispatcher_UnhandledException;

            SetupDB();

            Exit += App_Exit;

            DispatcherUnhandledException +=
                (sender, dispatcherUnhandled) =>
                {
                    Logger.Main.Error("Возникло не обработанное исключение в Dispatcher.",
                        dispatcherUnhandled.Exception);

                    dispatcherUnhandled.Handled = true;
                };
        }

        private void SetupDB()
        {
#if !DEBUG
            var baseDir = AppDomain.CurrentDomain.BaseDirectory;
            var path = Path.Combine(baseDir, "Data");
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.GetFullPath(path));
#else
            var path = "C:\\git\\technologist_db\\";
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
#endif
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Logger.Main.Trace("Запуск приложения {0}", typeof (App).Assembly.GetName());

            _bootstrap.Run(true);
        }

        private void App_Exit(object sender, ExitEventArgs e)
        {
            Logger.Main.Trace("Закрытие приложения {0}", typeof (App).Assembly.GetName());
        }

        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var message = string.Format("Ошибка в приложении {0}", typeof (App).Assembly.GetName());
            Logger.Main.Error(message, e.Exception);
            e.Handled = true;
        }

        private void Dispatcher_UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var message = string.Format("Ошибка в приложении {0}", typeof (App).Assembly.GetName());
            Logger.Main.Error(message, e.Exception);

            e.Handled = true;
        }
        

         
    }
}
