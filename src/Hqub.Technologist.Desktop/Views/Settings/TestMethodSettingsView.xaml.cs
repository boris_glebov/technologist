﻿namespace Hqub.Technologist.Desktop.Views.Settings
{
    /// <summary>
    /// Interaction logic for TestMethodSettingsView.xaml
    /// </summary>
    public partial class TestMethodSettingsView : BaseUserControlView
    {
        public TestMethodSettingsView(ViewModel.Settings.TestMethodSettingsViewModel vm) : base(vm)
        {
            InitializeComponent();
        }
    }
}
