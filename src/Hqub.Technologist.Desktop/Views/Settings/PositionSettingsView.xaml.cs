﻿using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Settings;

namespace Hqub.Technologist.Desktop.Views.Settings
{
    /// <summary>
    /// Interaction logic for PositionSettingsView.xaml
    /// </summary>
    public partial class PositionSettingsView 
    {
        public PositionSettingsView(PositionSettingsViewModel vm) : base(vm)
        {
            InitializeComponent();
        }

        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((PositionSettingsViewModel)ViewModel).DoubleClickCommand.Execute(null);
        }
    }
}
