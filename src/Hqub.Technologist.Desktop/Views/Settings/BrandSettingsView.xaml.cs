﻿using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Settings;

namespace Hqub.Technologist.Desktop.Views.Settings
{
    /// <summary>
    /// Interaction logic for BrandSettingsView.xaml
    /// </summary>
    public partial class BrandSettingsView : BaseUserControlView
    {
        public BrandSettingsView(BrandSettingsViewModel vm) : base(vm)
        {
            InitializeComponent();
        }

        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((BrandSettingsViewModel)ViewModel).DoubleClickCommand.Execute(null);
        }
    }
}
