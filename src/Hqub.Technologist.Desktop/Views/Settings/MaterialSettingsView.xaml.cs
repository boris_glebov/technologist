﻿using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Settings;

namespace Hqub.Technologist.Desktop.Views.Settings
{
    /// <summary>
    /// Interaction logic for MaterialSettingsView.xaml
    /// </summary>
    public partial class MaterialSettingsView : BaseUserControlView
    {
        public MaterialSettingsView(MaterialSettingsViewModel vm) : base(vm)
        {
            InitializeComponent();
        }

        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((MaterialSettingsViewModel)ViewModel).DoubleClickCommand.Execute(null);
        }
    }
}
