﻿using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Settings;

namespace Hqub.Technologist.Desktop.Views.Settings
{
    /// <summary>
    /// Interaction logic for QualitySettingsView.xaml
    /// </summary>
    public partial class QualitySettingsView : BaseUserControlView
    {
        public QualitySettingsView(QualitySettingsViewModel vm) : base(vm)
        {
            InitializeComponent();
        }

        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((QualitySettingsViewModel)ViewModel).DoubleClickCommand.Execute(null);
        }
    }
}
