﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hqub.Technologist.Desktop.Comparers;
using Hqub.Technologist.Desktop.Model.Store;
using Telerik.Windows.Data;

namespace Hqub.Technologist.Desktop.Views.Product
{
    /// <summary>
    /// Interaction logic for StatisticProductTestView.xaml
    /// </summary>
    public partial class StatisticProductTestView : BaseUserControlView
    {
        private ProductTestModel _selectedProduct;
        private List<int> _months;
        private List<int> _years;
        private int _selectedYear;
        private int _selectedMonth;
        private List<string> _marks;
        private string _selectedMark;
        private List<string> _classes;
        private string _selectedClass;
        private Events.RefreshEvent _refreshEvent;

        public StatisticProductTestView(ViewModel.Product.StatisticProductTestViewModel vm) : base(vm)
        {
            InitializeComponent();

            DataContext = ViewModel;

            Subsribe();
            Init();
        }

        private void Init()
        {
            LoadMonthsAndYears();
            LoadMarksAndClasses();
            LoadNomenclatures();
        }

        private void Subsribe()
        {
            var eventService = Events.AggregationEventService.Instance;
            _refreshEvent = eventService.GetEvent<Events.RefreshEvent>();
            _refreshEvent.Subscribe(AfterRefresh);
        }

        private void AfterRefresh(object args)
        {
            App.Current.Dispatcher.BeginInvoke(new Action(Init));
        }

        private void LoadMonthsAndYears()
        {
            var vm = ((ViewModel.Product.StatisticProductTestViewModel) ViewModel);

            if (vm.Products == null)
                return;

            Months =
                new List<int>(
                    vm.Products.Select(x => x.TestDate.Month)
                        .OrderBy(x => x).Distinct(new IntegerEqualityComparer()));

            Years =
                new List<int>(
                    vm.Products.Select(x => x.TestDate.Year)
                        .OrderBy(x => x).Distinct(new IntegerEqualityComparer()));

        }

        private void LoadMarksAndClasses()
        {
            var vm = ((ViewModel.Product.StatisticProductTestViewModel) ViewModel);

            if (vm.Products == null)
                return;

            Marks =
                new List<string>(
                    vm.Products.Select(
                        x => x.MarkNameRequired).Distinct(new StringEqualityComparer()));
            Classes =
                new List<string>(
                    vm.Products.Select(
                        x => x.ClassNameRequired).Distinct(new StringEqualityComparer()));
        }

        private void LoadNomenclatures()
        {
            var vm = ((ViewModel.Product.StatisticProductTestViewModel)ViewModel);
            if (vm.Nomenclatures == null || vm.Nomenclatures.Count == 0)
                return;

            NomenclatureNames = vm.Nomenclatures.Select(n => n.ProductName).OrderBy(x => x).ToList();
        }

        #region Filtrations

        private IFilterDescriptor LotFilterDescriptor { get; set; }
        private IFilterDescriptor MonthFilterDescriptor { get; set; }
        private IFilterDescriptor YearFilterDescriptor { get; set; }
        private IFilterDescriptor MarkFilterDescriptor { get; set; }
        private IFilterDescriptor ClassFilterDescriptor { get; set; }
        private IFilterDescriptor NomenclatureNameFilterDescriptor { get; set; }

        private void SetLotNumberFilter(string number)
        {
            if (LotFilterDescriptor != null)
            {
                MainGrid.FilterDescriptors.Remove(LotFilterDescriptor);
            }

            LotFilterDescriptor = new FilterDescriptor("SelectedLot.LotNumber", FilterOperator.Contains, number);
            MainGrid.FilterDescriptors.Add(LotFilterDescriptor);
        }

        private void SetMonthFilter(int month)
        {
            if (MonthFilterDescriptor != null)
            {
                MainGrid.FilterDescriptors.Remove(MonthFilterDescriptor);
            }

            MonthFilterDescriptor = new FilterDescriptor("TestDate.Month", FilterOperator.IsEqualTo, month);
            MainGrid.FilterDescriptors.Add(MonthFilterDescriptor);
        }

        private void SetYearFilter(int year)
        {
            if (YearFilterDescriptor != null)
            {
                MainGrid.FilterDescriptors.Remove(YearFilterDescriptor);
            }

            YearFilterDescriptor = new FilterDescriptor("TestDate.Year", FilterOperator.IsEqualTo, year);
            MainGrid.FilterDescriptors.Add(YearFilterDescriptor);
        }

        private void SetMarkFilter(string mark)
        {
            if (MarkFilterDescriptor != null)
            {
                MainGrid.FilterDescriptors.Remove(MarkFilterDescriptor);
            }

            MarkFilterDescriptor = new FilterDescriptor("MarkNameRequired", FilterOperator.IsEqualTo, mark);
            MainGrid.FilterDescriptors.Add(MarkFilterDescriptor);
        }

        private void SetClassFilter(string className)
        {
            if (ClassFilterDescriptor != null)
            {
                MainGrid.FilterDescriptors.Remove(ClassFilterDescriptor);
            }

            ClassFilterDescriptor = new FilterDescriptor("ClassNameRequired", FilterOperator.IsEqualTo, className);
            MainGrid.FilterDescriptors.Add(ClassFilterDescriptor);
        }

        private void SetNomenclatureNameFilter(string nomenclatureName)
        {
            if(NomenclatureNameFilterDescriptor != null)
            {
                MainGrid.FilterDescriptors.Remove(NomenclatureNameFilterDescriptor);
            }

            NomenclatureNameFilterDescriptor = new FilterDescriptor("SelectedLot.NomenclatureEntity.ProductName", FilterOperator.IsEqualTo, nomenclatureName);
            MainGrid.FilterDescriptors.Add(NomenclatureNameFilterDescriptor);
        }


        #endregion

        #region Properties

        public ProductTestModel SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                _selectedProduct = value;
                SetLotNumberFilter(value.SelectedLot.LotNumber);
                OnPropertyChanged();
            }
        }

        public List<int> Months
        {
            get { return _months; }
            set
            {
                _months = value;
                OnPropertyChanged();
            }
        }

        public int SelectedMonth
        {
            get { return _selectedMonth; }
            set
            {
                _selectedMonth = value;
                SetMonthFilter(value);

                OnPropertyChanged();
            }
        }

        public List<int> Years
        {
            get { return _years; }
            set
            {
                _years = value;
                OnPropertyChanged();
            }
        }

        public int SelectedYear
        {
            get { return _selectedYear; }
            set
            {
                _selectedYear = value;
                SetYearFilter(value);
                OnPropertyChanged();
            }
        }

        public List<string> Marks
        {
            get { return _marks; }
            set
            {
                _marks = value; 
                OnPropertyChanged();
            }
        }

        public string SelectedMark
        {
            get { return _selectedMark; }
            set
            {
                _selectedMark = value;
                SetMarkFilter(value);
                OnPropertyChanged();
            }
        }

        public List<string> Classes
        {
            get { return _classes; }
            set
            {
                _classes = value;
                OnPropertyChanged();
            }
        }

        public string SelectedClass
        {
            get { return _selectedClass; }
            set
            {
                _selectedClass = value;
                SetClassFilter(value);
                OnPropertyChanged();
            }
        }

        private List<string> _nomenclatureNames;
        public List<string> NomenclatureNames
        {
            get { return _nomenclatureNames; }
            set
            {
                _nomenclatureNames = value;
                OnPropertyChanged();
            }
        }

        private string _selectedNomenclatureName;
        public string SelectedNomenclatureName
        {
            get { return _selectedNomenclatureName; }
            set
            {
                _selectedNomenclatureName = value;
                SetNomenclatureNameFilter(value);
                OnPropertyChanged();
            }
        }

        #endregion
    }
}
