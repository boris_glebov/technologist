﻿using System.Collections.Generic;
using Hqub.Technologist.Desktop.Model;

namespace Hqub.Technologist.Desktop.Views.Product
{
    /// <summary>
    /// Interaction logic for ReportProductTestView.xaml
    /// </summary>
    public partial class ReportProductTestView : BaseUserControlView
    {
        private List<TypeReport> _reportTypes;
        private TypeReport _selectedTypeReport;

        public ReportProductTestView(ViewModel.Product.ReportProductTestViewModel vm) : base(vm)
        {
            InitializeComponent();

            ReportTypes = new List<TypeReport>
            {
                TypeReport.Full,
                TypeReport.Default,
                TypeReport.Characters
            };
        }

        public List<TypeReport> ReportTypes
        {
            get { return _reportTypes; }
            set
            {
                _reportTypes = value;
                OnPropertyChanged();
            }
        }

        public TypeReport SelectedTypeReport
        {
            get { return _selectedTypeReport; }
            set
            {
                _selectedTypeReport = value;
                ChangeTableView(value);
                OnPropertyChanged();
            }
        }

        private void ResetColumns()
        {
            foreach (var clm in MainGrid.Columns)
            {
                clm.IsVisible = true;
            }
        }

        private void ChangeTableView(TypeReport typeReport)
        {
            ResetColumns();

            switch (typeReport)
            {
                case TypeReport.Default:
                    for (int i = 10; i < MainGrid.Columns.Count; i++)
                    {
                        MainGrid.Columns[i].IsVisible = false;
                    }
                    break;
                case TypeReport.Characters:
                    for (int i = 0; i < MainGrid.Columns.Count; i++)
                    {
                        if (!((i >= 0 && i <= 2) || (i >= 5 && i <= 9)))
                        {
                            MainGrid.Columns[i].IsVisible = false;
                        }
                    }
                    break;
            }
        }
    }
}
