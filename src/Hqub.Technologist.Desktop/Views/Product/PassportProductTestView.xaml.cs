﻿namespace Hqub.Technologist.Desktop.Views.Product
{
    /// <summary>
    /// Interaction logic for PassportProductTestView.xaml
    /// </summary>
    public partial class PassportProductTestView : BaseUserControlView
    {
        public PassportProductTestView(ViewModel.Product.PassportProductTestViewModel vm) : base(vm)
        {
            InitializeComponent();
        }
    }
}
