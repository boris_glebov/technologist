﻿using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Product;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Hqub.Technologist.Desktop.Views.Product
{
    /// <summary>
    /// Испытания ГП
    /// </summary>
    public partial class ProductTestView : BaseUserControlView
    {
        public ProductTestView()
        {
            
        }

        public ProductTestView(ProductTestViewModel vm) : base(vm)
        {
            InitializeComponent();
        }

        private void SampleTable_OnDeleting(object sender, GridViewDeletingEventArgs e)
        {
            ((ProductTestViewModel) ViewModel).DeleteSampleCommand.Execute(null);
            e.Cancel = true;
        }

        private void SampleBottlesTable_OnDeleting(object sender, GridViewDeletingEventArgs e)
        {
            ((ProductTestViewModel) ViewModel).DeleteSampleBottleCommand.Execute(null);
            e.Cancel = true;
        }

        private void SampleTable_OnDataError(object sender, DataErrorEventArgs e)
        {
            
        }

        private void SampleBottlesTable_OnDataError(object sender, DataErrorEventArgs e)
        {

        }
    }
}
