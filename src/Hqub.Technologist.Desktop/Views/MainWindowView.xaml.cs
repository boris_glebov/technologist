﻿using Hqub.Technologist.Desktop.ViewModel;

namespace Hqub.Technologist.Desktop.Views
{
    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : BaseWindowView
    {
        public MainWindowView(MainViewModel viewModel) : base(viewModel)
        {
            InitializeComponent();
        }
    }
}
