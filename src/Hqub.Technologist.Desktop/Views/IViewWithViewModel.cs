﻿using Hqub.Technologist.Desktop.ViewModel;

namespace Hqub.Technologist.Desktop.Views
{
    public interface IViewWithViewModel
    {
        BindableViewModel ViewModel { get; }
    }
}
