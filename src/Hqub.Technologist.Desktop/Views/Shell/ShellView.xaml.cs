﻿using Hqub.Technologist.Desktop.ViewModel.Shell;

namespace Hqub.Technologist.Desktop.Views.Shell
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : BaseUserControlView
    {
        public ShellView(ShellViewModel viewModel) : base(viewModel)
        {
            InitializeComponent();
        }
    }
}
