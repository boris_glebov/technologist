﻿using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Materials;

namespace Hqub.Technologist.Desktop.Views.Materials
{
    /// <summary>
    /// Interaction logic for Test_lime.xaml
    /// </summary>
    public partial class TestLimeView : BaseUserControlView
    {
        public TestLimeView(TestLimeViewModel vm) : base(vm)
        {
            InitializeComponent();
        }

        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((TestLimeViewModel)ViewModel).DoubleClickCommand.Execute(null);
        }
    }
}
