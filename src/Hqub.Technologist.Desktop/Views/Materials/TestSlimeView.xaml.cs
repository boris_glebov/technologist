﻿
using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Materials;

namespace Hqub.Technologist.Desktop.Views.Materials
{
    /// <summary>
    /// Interaction logic for TestSlimeModule.xaml
    /// </summary>
    
        public partial class TestSlimeView : BaseUserControlView
        {
            public TestSlimeView(TestSlimeViewModel vm)
                : base(vm)
            {
                InitializeComponent();
            }
            private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
            {
                ((TestSlimeViewModel)ViewModel).DoubleClickCommand.Execute(null);
            }
        }
    
}
