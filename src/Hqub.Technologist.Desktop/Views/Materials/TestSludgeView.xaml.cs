﻿using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Materials;

namespace Hqub.Technologist.Desktop.Views.Materials
{
    /// <summary>
    /// Interaction logic for TestSludgeView.xaml
    /// </summary>
    public partial class TestSludgeView : BaseUserControlView
    {
        public TestSludgeView(TestSludgeViewModel vm)
            : base(vm)
        {
            InitializeComponent();
        }
        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((TestSludgeViewModel)ViewModel).DoubleClickCommand.Execute(null);
        }
    }
}
