﻿using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Materials;

namespace Hqub.Technologist.Desktop.Views.Materials
{
    /// <summary>
    /// Interaction logic for TestCementView.xaml
    /// </summary>
    public partial class TestCementView : BaseUserControlView
    {
        public TestCementView(TestCementViewModel vm): base(vm)
        {
            InitializeComponent();
        }
        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((TestCementViewModel)ViewModel).DoubleClickCommand.Execute(null);
        }
    }
}
