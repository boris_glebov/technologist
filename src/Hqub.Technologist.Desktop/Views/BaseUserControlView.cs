﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using Hqub.Technologist.Desktop.ViewModel;
using JetBrains.Annotations;

namespace Hqub.Technologist.Desktop.Views
{
    public class BaseUserControlView : UserControl, IViewWithViewModel, INotifyPropertyChanged
    {
        public BaseUserControlView()
        {
            
        }

        public BaseUserControlView(BindableViewModel viewModel)
        {
            if (DesignerProperties.GetIsInDesignMode(this))
            {
                return;
                // Design-mode specific functionality
            }

            ViewModel = viewModel;

            Loaded += (loadedSender, loadedArgs) => viewModel.LoadCommand.Execute(null);
        }


        public BindableViewModel ViewModel
        {
            get { return (BindableViewModel)DataContext; }
            private set { DataContext = value; }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
