﻿using System.Linq;
using System.Windows.Controls;

namespace Hqub.Technologist.Desktop.Views.Navigation
{
    /// <summary>
    /// Interaction logic for NavigationRootPanelView.xaml
    /// </summary>
    public partial class NavigationRootPanelView : UserControl
    {
        public NavigationRootPanelView(Model.Navigation.NavigationModel model)
        {
            InitializeComponent();

            Init(model);
        }

        private void Init(Model.Navigation.NavigationModel model)
        {
            DataContext = model;

            foreach (var itemControl in model.Items.Select(item => new Controls.NavigationRootItemControl(item)))
            {
                Panel.Children.Add(itemControl);
            }
        }
    }
}
