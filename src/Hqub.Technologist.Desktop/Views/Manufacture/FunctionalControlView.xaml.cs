﻿using System.Linq;
using System.Windows;
using System.Windows.Data;
using Hqub.Technologist.Desktop.ViewModel.Manufacture;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;
using Telerik.Windows.Controls;

namespace Hqub.Technologist.Desktop.Views.Manufacture
{
    /// <summary>
    /// Interaction logic for FunctionalControlView.xaml
    /// </summary>
    public partial class FunctionalControlView : BaseUserControlView
    {
        private IQualityControlSettingsRepository _qualityControlSettings;

        public FunctionalControlView(FunctionalControlViewModel vm) : base(vm)
        {
            InitializeComponent();

            _qualityControlSettings = Locator.Get<IQualityControlSettingsRepository>();
            SetupGrid();
        }

        private void SetupGrid()
        {
            var columns =
                _qualityControlSettings.Where(x => x.ControlType == (int) QualityControlType.FunctionalControl)
                    .OrderBy(x => x.Order)
                    .ToList();

            foreach (var column in columns)
            {
                GridView.Columns.Insert(1, new GridViewDataColumn
                {
                    Header = column.Name,
                    DataMemberBinding =
                        new Binding(string.Format("XExtend[{0}]", column.Name)) {Mode = BindingMode.TwoWay}
                });
            }
        }
    }
}
