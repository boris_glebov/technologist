﻿using System.Linq;

using System.Windows.Data;
using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Manufacture;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;
using Telerik.Windows.Controls;

namespace Hqub.Technologist.Desktop.Views.Manufacture
{
    /// <summary>
    /// Interaction logic for AssessmentQualityView.xaml
    /// </summary>
    public partial class AssessmentQualityView : BaseUserControlView

    {
        private IQualityControlSettingsRepository _qualityControlSettings;
        public AssessmentQualityView(AssessmentQualityViewModel vm) : base(vm)
        {
            InitializeComponent();
            _qualityControlSettings = Locator.Get<IQualityControlSettingsRepository>();
            SetupGrid();
        }

        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((AssessmentQualityViewModel) ViewModel).DoubleClickCommand.Execute(null);
        }

        private void SetupGrid()
        {
            var columns =
                _qualityControlSettings.Where(x => x.ControlType == (int)QualityControlType.QualityArray)
                    .OrderBy(x => x.Order)
                    .ToList();

            foreach (var column in columns)
            {
                GridView.Columns.Insert(GridView.Columns.Count-1, new GridViewDataColumn
                {
                    Header = column.Name,
                    DataMemberBinding =
                        new Binding(string.Format("XExtend[{0}]", column.Name)) { Mode = BindingMode.TwoWay }
                });
            }
        }
    }
}
