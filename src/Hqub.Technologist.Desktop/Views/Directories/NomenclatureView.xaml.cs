﻿using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Directories;

namespace Hqub.Technologist.Desktop.Views.Directories
{
    /// <summary>
    /// Interaction logic for NomenclatureView.xaml
    /// </summary>
    public partial class NomenclatureView : BaseUserControlView
    {
        public NomenclatureView(NomenclatureViewModel vm) : base(vm)
        {
            InitializeComponent();
        }

        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((NomenclatureViewModel)ViewModel).DoubleClickCommand.Execute(null);
        }
    }
}
