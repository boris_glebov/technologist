﻿using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Directories;

namespace Hqub.Technologist.Desktop.Views.Directories
{
    /// <summary>
    /// Interaction logic for ProudctTypeView.xaml
    /// </summary>
    public partial class ProudctTypeView : BaseUserControlView
    {
        public ProudctTypeView(ProductTypeViewModel vm) : base(vm)
        {
            InitializeComponent();
        }

        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((ProductTypeViewModel)ViewModel).DoubleClickCommand.Execute(null);
        }
    }
}
