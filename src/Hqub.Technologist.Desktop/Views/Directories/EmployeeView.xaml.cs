﻿using System.Windows.Controls;
using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Directories;

namespace Hqub.Technologist.Desktop.Views.Directories
{
    /// <summary>
    /// Interaction logic for EmployeeView.xaml
    /// </summary>
    public partial class EmployeeView : BaseUserControlView
    {
        public EmployeeView(EmployeeViewModel vm) : base(vm)
        {
            InitializeComponent();
        }

        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((EmployeeViewModel)ViewModel).DoubleClickCommand.Execute(null);
        }
    }
}
