﻿using System.Windows.Input;
using Hqub.Technologist.Desktop.ViewModel.Directories;

namespace Hqub.Technologist.Desktop.Views.Directories
{
    /// <summary>
    /// Interaction logic for ProductCharacterView.xaml
    /// </summary>
    public partial class ProductCharacterView : BaseUserControlView
    {
        public ProductCharacterView(ProductCharacterViewModel vm) : base(vm)
        {
            InitializeComponent();
        }

        private void Control_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((ProductCharacterViewModel)ViewModel).DoubleClickCommand.Execute(null);
        }
    }
}
