﻿namespace Hqub.Technologist.Desktop.Modules.Settings
{
    public class QualitySettingsModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Контроль производства"; }
        }

        public QualitySettingsModule(Views.Settings.QualitySettingsView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
