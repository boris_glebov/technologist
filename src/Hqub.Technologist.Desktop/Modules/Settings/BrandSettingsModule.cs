﻿namespace Hqub.Technologist.Desktop.Modules.Settings
{
    public class BrandSettingsModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Марки выпускаемой продукции"; }
        }

        public BrandSettingsModule(Views.Settings.BrandSettingsView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
