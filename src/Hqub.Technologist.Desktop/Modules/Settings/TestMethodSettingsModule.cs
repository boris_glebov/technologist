﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Technologist.Desktop.Modules.Settings
{
    public class TestMethodSettingsModule : BaseNavigationModule
    {
         public override string Name
        {
            get { return "Настройка методики испытаний"; }
        }

         public TestMethodSettingsModule(Views.Settings.TestMethodSettingsView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
