﻿namespace Hqub.Technologist.Desktop.Modules.Settings
{
    public class MaterialSettingsModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Сырье"; }
        }

        public MaterialSettingsModule(Views.Settings.MaterialSettingsView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
