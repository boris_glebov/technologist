﻿namespace Hqub.Technologist.Desktop.Modules.Settings
{
    public class PositionSettingsModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Должности"; }
        }

        public PositionSettingsModule(Views.Settings.PositionSettingsView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}