﻿namespace Hqub.Technologist.Desktop.Modules.Manufacture
{
    public class FunctionalControlModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Параметры процесса"; }
        }

        public FunctionalControlModule(Views.Manufacture.FunctionalControlView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
