﻿namespace Hqub.Technologist.Desktop.Modules.Manufacture
{
    public class AssessmentQualityModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Оценка качества массивов"; }
        }

        public AssessmentQualityModule(Views.Manufacture.AssessmentQualityView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
