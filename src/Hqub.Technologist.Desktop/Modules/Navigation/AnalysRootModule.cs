﻿using Hqub.Technologist.Desktop.Model.Navigation;

namespace Hqub.Technologist.Desktop.Modules.Navigation
{
    public class AnalysRootModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Анализ"; }
        }

        public AnalysRootModule()
        {
            var rootPanel = new Views.Navigation.NavigationRootPanelView(new NavigationModel());
            RegisterView(RegionNames.MainRegionName, rootPanel);
        }
    }
}
