﻿using System.Collections.Generic;
using Hqub.Technologist.Desktop.Model.Navigation;
using Hqub.Technologist.Desktop.Navigation;

namespace Hqub.Technologist.Desktop.Modules.Navigation
{
    public class SettingsRootModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Настройки"; }
        }

        public SettingsRootModule()
        {
            var rootPanel = new Views.Navigation.NavigationRootPanelView(new NavigationModel
            {
                Name = Name,
                Items = new List<NavigationItemModel>
                {
                    new NavigationItemModel
                    {
                        Title = "Марка выпускаемой продукции",
                        Description = "Описание раздела марка выпускаемой продукции",
                        GoToAction = NavigationHelper.GoToBrandSettings,
                        ImagePath = "../Content/mark.png"
                        
                    },
                    new NavigationItemModel
                    {
                        Title = "Настройка методики испытаний",
                        Description = "Описание раздела настройка методики испытаний",
                        GoToAction = NavigationHelper.GoToTestMethodSettings,
                        ImagePath = "../Content/testMethod.png"
                    },
                     new NavigationItemModel
                    {
                        Title = "Контроль производства",
                        Description = "Описание раздела контроль производства",
                        GoToAction = NavigationHelper.GoToQualitySettings,
                        ImagePath = "../Content/control.png"
                    },
                     new NavigationItemModel
                    {
                        Title = "Сырье",
                        Description = "Описание раздела сырье",
                        GoToAction = NavigationHelper.GoToMaterialSettings,
                        ImagePath = "../Content/materials.png"
                    },
                     new NavigationItemModel
                    {
                        Title = "Должности",
                        Description = "Описание раздела должности",
                        GoToAction = NavigationHelper.GoToPositionSettings,
                        ImagePath = "../Content/position.png"
                    }
                }
            });
            RegisterView(RegionNames.MainRegionName, rootPanel);
        }
    }
}
