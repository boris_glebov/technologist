﻿using System.Collections.Generic;
using Hqub.Technologist.Desktop.Model.Navigation;
using Hqub.Technologist.Desktop.Navigation;

namespace Hqub.Technologist.Desktop.Modules.Navigation
{
    public class DirectoryRootModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Библиотека"; }
        }

        public DirectoryRootModule()
        {
            var rootPanel = new Views.Navigation.NavigationRootPanelView(new NavigationModel
            {
                Name = Name,
                Items = new List<NavigationItemModel>
                {
                    new NavigationItemModel
                    {
                        Title = "Номенклатура",
                        Description = "Описание раздела Номенклатура",
                        GoToAction = NavigationHelper.GoToNomenclature,
                        ImagePath = "../Content/character.png"
                    },
                    new NavigationItemModel
                    {
                        Title = "Виды номенклатуры",
                        Description = "Описание раздела Виды номенклатуры",
                        GoToAction = NavigationHelper.GoToNomenclatureTypes,
                        ImagePath = "../Content/typeN.png"
                    },
                    new NavigationItemModel
                    {
                        Title = "Сотрудники",
                        Description = "Описание раздела сотрудники",
                        GoToAction = NavigationHelper.GoToEmployees,
                        ImagePath = "../Content/employees.png"
                    },
                    new NavigationItemModel
                    {
                        Title = "Характеристики АГБ",
                        Description = "Описание раздела Характеристики АГБ",
                        GoToAction = NavigationHelper.GoToProductCharacter,
                        ImagePath = "../Content/char.png"
                    }
                     
                }
            });
            RegisterView(RegionNames.MainRegionName, rootPanel);
            
        }
    }
}
