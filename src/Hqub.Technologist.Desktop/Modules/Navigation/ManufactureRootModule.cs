﻿using System.Collections.Generic;
using Hqub.Technologist.Desktop.Model.Navigation;
using Hqub.Technologist.Desktop.Navigation;

namespace Hqub.Technologist.Desktop.Modules.Navigation
{
    public class ManufactureRootModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Производство"; }
        }

        public ManufactureRootModule()
        {
            var rootPanel = new Views.Navigation.NavigationRootPanelView(new NavigationModel
            {
                Name = Name,
                Items = new List<NavigationItemModel>
                {
                     new NavigationItemModel
                    {
                        Title = "Параметры процесса",
                        Description = "Описание раздела Параметры процесса",
                        GoToAction = NavigationHelper.GoToFunctionalControl,
                        ImagePath = "../Content/check_control.png"
                        
                    },

                    new NavigationItemModel
                    {
                        Title = "Качество массивов",
                        Description = "Описание раздела качество массивов",
                        GoToAction = NavigationHelper.GoToAssessmentQuality,
                        ImagePath = "../Content/quality.png"
                        
                    }
                }
            });
            RegisterView(RegionNames.MainRegionName, rootPanel);
            
        }
    }
}
