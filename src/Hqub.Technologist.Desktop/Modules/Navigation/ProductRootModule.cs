﻿using System;
using System.Collections.Generic;
using Hqub.Technologist.Desktop.Model.Navigation;
using Hqub.Technologist.Desktop.Navigation;

namespace Hqub.Technologist.Desktop.Modules.Navigation
{
    public class ProductRootModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Продукция"; }
        }

        public ProductRootModule()
        {
            var rootPanel = new Views.Navigation.NavigationRootPanelView(new NavigationModel
            {
                Name = Name,
                Items = new List<NavigationItemModel>
                {
                    new NavigationItemModel
                    {
                        Title = "Испытания ГП",
                        Description = "Описание раздела испытания ГП",
                        GoToAction = NavigationHelper.GoToProductTest,
                        ImagePath = "../Content/producttest.png"
                        
                    },
                    new NavigationItemModel
                    {
                        Title = "Статистика",
                        Description = "Описание раздела статистики",
                        GoToAction = NavigationHelper.GoToStatisiticProductTest,
                        ImagePath = "../Content/statist.png"
                        
                    },
                     new NavigationItemModel
                    {
                        Title = "Отчет по ГП",
                        Description = "Описание раздела отчет по ГП",
                        GoToAction = NavigationHelper.GoToOpenReportProductTest,
                        ImagePath = "../Content/report.png"
                    },
                     new NavigationItemModel
                    {
                        Title = "Паспорт",
                        Description = "Описание раздела Паспорт",
                        GoToAction = NavigationHelper.GoToOpenPassportProductTest,
                        ImagePath = "../Content/pasport.png"
                    }
                }
            });
            RegisterView(RegionNames.MainRegionName, rootPanel);
        }
    }
}
