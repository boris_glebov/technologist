﻿using System;
using System.Collections.Generic;
using Hqub.Technologist.Desktop.Model.Navigation;
using Hqub.Technologist.Desktop.Navigation;

namespace Hqub.Technologist.Desktop.Modules.Navigation
{
    public class MaterialRootModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Сырье"; }
        }

        public MaterialRootModule()
        {
            var rootPanel = new Views.Navigation.NavigationRootPanelView(new NavigationModel
            {
                Name = Name,
                Items = new List<NavigationItemModel>
                {
                    new NavigationItemModel
                    {
                        Title = "Известь",
                        Description = "Описание раздела известь",
                        GoToAction = NavigationHelper.GoToTestLime,
                        ImagePath = "../Content/materials.png"
                        
                    },
                    new NavigationItemModel
                    {
                        Title = "Цемент",
                        Description = "Описание раздела цемент",
                        GoToAction = NavigationHelper.GoToTestCement,
                        ImagePath = "../Content/materials.png"
                    },
                     new NavigationItemModel
                    {
                        Title = "SiO",
                        Description = "Описание раздела зола",
                        GoToAction = NavigationHelper.GoToTestSludge,
                        ImagePath = "../Content/materials.png"
                    },
                    new NavigationItemModel
                    {
                        Title = "Шлам",
                        Description = "Описание раздела шлам",
                        GoToAction = NavigationHelper.GoToTestSlime,
                        ImagePath = "../Content/materials.png"
                    }
                }
            });
            RegisterView(RegionNames.MainRegionName, rootPanel);
        }
    }
}
