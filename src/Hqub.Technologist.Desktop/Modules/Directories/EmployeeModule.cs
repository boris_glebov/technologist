﻿namespace Hqub.Technologist.Desktop.Modules.Directories
{
    public class EmployeeModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Сотрудники"; }
        }

        public EmployeeModule(Views.Directories.EmployeeView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
