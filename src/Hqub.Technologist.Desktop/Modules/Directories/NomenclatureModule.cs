﻿namespace Hqub.Technologist.Desktop.Modules.Directories
{
    public class NomenclatureModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Номенклатура"; }
        }

        public NomenclatureModule(Views.Directories.NomenclatureView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
