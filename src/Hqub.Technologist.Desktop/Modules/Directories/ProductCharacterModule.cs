﻿namespace Hqub.Technologist.Desktop.Modules.Directories
{
    public class ProductCharacterModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Характеристики продукции"; }
        }

        public ProductCharacterModule(Views.Directories.ProductCharacterView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
