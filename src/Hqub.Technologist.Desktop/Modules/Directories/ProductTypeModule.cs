﻿namespace Hqub.Technologist.Desktop.Modules.Directories
{
    public class ProductTypeModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Виды номенклатуры"; }
        }

        public ProductTypeModule(Views.Directories.ProudctTypeView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
