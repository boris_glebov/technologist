﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Technologist.Desktop.Modules.Product
{
    public class PassportProductTestModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Паспорт ГП"; }
        }

        public PassportProductTestModule(Views.Product.PassportProductTestView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
