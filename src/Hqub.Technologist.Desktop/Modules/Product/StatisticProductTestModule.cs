﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Technologist.Desktop.Modules.Product
{
    public class StatisticProductTestModule : BaseNavigationModule
    {
        public StatisticProductTestModule(Views.Product.StatisticProductTestView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }

        public override string Name
        {
            get { return "Статистика ГП"; }
        }
    }
}
