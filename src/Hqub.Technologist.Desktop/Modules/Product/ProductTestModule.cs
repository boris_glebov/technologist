﻿namespace Hqub.Technologist.Desktop.Modules.Product
{
    public class ProductTestModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Испытания ГП"; }
        }

        public ProductTestModule(Views.Product.ProductTestView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
