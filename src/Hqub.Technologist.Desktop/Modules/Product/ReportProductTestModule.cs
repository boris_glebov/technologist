﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Technologist.Desktop.Modules.Product
{
    public class ReportProductTestModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Отчет по ГП"; }
        }

        public ReportProductTestModule(Views.Product.ReportProductTestView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
