﻿using Hqub.Technologist.Desktop.Views.Shell;

namespace Hqub.Technologist.Desktop.Modules
{
    public class ShellModule : BaseNavigationModule
    {
        public ShellModule(ShellView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }

        public override string Name
        {
            get { return "Меню"; }
        }
    }
}
