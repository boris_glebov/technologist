﻿using System.Collections.Generic;
using Hqub.Technologist.Desktop.Utitlities;
using Hqub.Technologist.Desktop.ViewModel;
using Hqub.Technologist.Desktop.Views;

namespace Hqub.Technologist.Desktop.Modules
{
	public abstract class BaseModule
	{
		readonly Dictionary<string, object> _region2View = 
			new Dictionary<string, object>();

        readonly List<BindableViewModel> _viewModels = new List<BindableViewModel>(); 

		public virtual void Show()
		{
			foreach (var kv in _region2View)
			{
				RegionManagerUtils.AddToRegion(kv.Key, kv.Value);
			}
		}

		public virtual void Hide()
		{
			foreach (var kv in _region2View)
			{
				RegionManagerUtils.ClearRegion(kv.Key);
			}
		}

	    protected void RegisterView(string regionName, object view)
	    {
	        _region2View.Add(regionName, view);

	        var vmView = view as IViewWithViewModel;
            if (vmView != null && vmView.ViewModel != null)
	        {
	            _viewModels.Add(vmView.ViewModel);
	        }
	    }

        protected void RegisterViewModel(BindableViewModel viewModel)
        {                        
            _viewModels.Add(viewModel);
        }

	    public IEnumerable<object> GetAllViews()
        {
            return _region2View.Values;
        }

        public IEnumerable<BindableViewModel> GetAllViewModels()
        {
            return _viewModels;
        }
	}
}
