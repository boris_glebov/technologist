﻿using Hqub.Technologist.Desktop.Views;

namespace Hqub.Technologist.Desktop.Modules
{
	public abstract class BaseNavigationModule : BaseModule
	{
        public virtual string Name { get { return string.Empty; } }

        public virtual string BackName { get { return string.Empty; } }

        public virtual bool BeforeBack()
        {
            return true;
        }

        public virtual bool BeforeNext()
        {
            return true;
        }

        public bool IsCurrent { get; set; }

        public bool IsFirst { get; set; }

		public virtual void OnUnload()
		{
			foreach (var vm in GetAllViewModels())
			{
				vm.Unload();
			}
		}

        public BaseUserControlView View { get; set; }
	}
}
