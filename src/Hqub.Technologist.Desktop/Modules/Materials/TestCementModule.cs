﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Technologist.Desktop.Modules.Materials
{
    public class TestCementModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Испытание цемента"; }
        }

        public TestCementModule(Views.Materials.TestCementView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
