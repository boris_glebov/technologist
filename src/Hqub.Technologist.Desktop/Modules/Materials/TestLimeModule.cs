﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Technologist.Desktop.Modules.Materials
{
    public class TestLimeModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Испытание извести"; }
        }
        public TestLimeModule(Views.Materials.TestLimeView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
