﻿namespace Hqub.Technologist.Desktop.Modules.Materials
{
    public class TestSludgeModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Испытание золы"; }
        }
        public TestSludgeModule(Views.Materials.TestSludgeView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
