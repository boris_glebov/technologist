﻿namespace Hqub.Technologist.Desktop.Modules.Materials
{
    public class TestSlimeModule : BaseNavigationModule
    {
        public override string Name
        {
            get { return "Испытание шлама"; }
        }
        public TestSlimeModule(Views.Materials.TestSlimeView view)
        {
            RegisterView(RegionNames.MainRegionName, view);
        }
    }
}
