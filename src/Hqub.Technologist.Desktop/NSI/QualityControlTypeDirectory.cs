﻿using System.Collections.Generic;
using Hqub.Technologist.Desktop.Model.NSI;
using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.NSI
{
    public static class QualityControlTypeDirectory
    {
        public static List<DirectoryItem<QualityControlType>> GetQualityControlTypes()
        {
            return new List<DirectoryItem<QualityControlType>>
            {
                new DirectoryItem<QualityControlType>("Параметры процесса", QualityControlType.FunctionalControl),
                new DirectoryItem<QualityControlType>("Качество массивов", QualityControlType.QualityArray)
            };
        }
    }
}
