﻿using System.Collections.Generic;
using System.Linq;
using Hqub.Technologist.Desktop.Model.NSI;

namespace Hqub.Technologist.Desktop.NSI
{
    public static class AlphaByTestNumberFactory
    {
        public static List<CorrectionFactorModel> GetDirectory()
        {
            return new List<CorrectionFactorModel>
            {
                new CorrectionFactorModel(2, 1.13f),
                new CorrectionFactorModel(3, 1.69f),
                new CorrectionFactorModel(4, 2.06f),
                new CorrectionFactorModel(5, 2.33f),
                new CorrectionFactorModel(6, 2.5f)
            };
        }

        public static float Get(int number)
        {
            var items = GetDirectory();

            if (number < 2)
                return 1;

            if (number > 6)
                return items[items.Count - 1].Factor;

            return items.First(x=>x.Density == number).Factor;
        }
    }
}
