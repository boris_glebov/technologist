﻿using System.Collections.Generic;

namespace Hqub.Technologist.Desktop.NSI
{
    public static class PlacesDirectory
    {
        public static List<string> GetPlaces()
        {
            return new List<string>
            {
                "Шнек 1",
                "Место 2"
            };
        }
    }
}
