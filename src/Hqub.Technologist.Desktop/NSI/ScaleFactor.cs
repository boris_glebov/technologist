﻿using System.Collections.Generic;
using System.Linq;
using Hqub.Technologist.Desktop.Model.NSI;
using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.NSI
{
    public static class ScaleFactor
    {
        public static List<ScalingFactorModel> GetScaleFactor()
        {
            return new List<ScalingFactorModel>
            {
                new  ScalingFactorModel(SampleType.Type07, 0.9f),
                new  ScalingFactorModel(SampleType.Type10,0.95f),
                new  ScalingFactorModel(SampleType.Type15,1.0f)
            };
        }

        public static float Get(SampleType type)
        {
            var items = GetScaleFactor();

            return items.First(x => x.Value == type).Factor;
        }
    }
}
