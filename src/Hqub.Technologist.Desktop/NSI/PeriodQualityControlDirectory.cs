﻿using System.Collections.Generic;
using Hqub.Technologist.Desktop.Model.NSI;
using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.NSI
{
    public static class PeriodQualityControlDirectory
    {
        public static List<DirectoryItem<PeriodQualityControlType>> GetPeriodQualityControlTypes()
        {
          return new List<DirectoryItem<PeriodQualityControlType>>
          {
              new DirectoryItem<PeriodQualityControlType>("Каждый массив", PeriodQualityControlType.Every),  
              new DirectoryItem<PeriodQualityControlType>("Автоклав", PeriodQualityControlType.Autoclave),  
              new DirectoryItem<PeriodQualityControlType>("Партия", PeriodQualityControlType.Party)  
          };
        }
    }
}
