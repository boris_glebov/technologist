﻿using System.Collections.Generic;
using System.Linq;
using Hqub.Technologist.Desktop.Model.NSI;

namespace Hqub.Technologist.Desktop.NSI
{
    public static class CorrectionFactor
    {
        public static List<CorrectionFactorModel> GetCorrectionFactor()
        {
            var correlationFactors = new List<CorrectionFactorModel>();

            var c = 0.78f;
            for (var i = 0; i <= 25; i++)
            {
                if (i <= 10)
                    c += 0.02f;
                else
                    c += 0.01f;

                correlationFactors.Add(new CorrectionFactorModel(i, c));
            }
            return correlationFactors;
        }

        public static float Get(int humidity)
        {
            var items = GetCorrectionFactor();

            //return humidity > 25 ? items[items.Count - 1].Factor : items.First(x => x.Density == humidity).Factor;
            if (humidity > 25)
            {
                return items[items.Count - 1].Factor;
            }
            var correctionFactorModel = items.FirstOrDefault(x => x.Density == humidity);
            if (correctionFactorModel != null)
            {
                return correctionFactorModel.Factor;

            }
            return 0;
        }
    }
}
