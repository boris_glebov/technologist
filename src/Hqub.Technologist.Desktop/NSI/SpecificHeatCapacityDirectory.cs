﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Desktop.Model.NSI;
using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.NSI
{
    public static class SpecificHeatCapacityDirectory
    {
        public static List<DirectoryItem<SpecificHeatCapacityType>> GetSpecificHeatCapacityTypes()
        {
            return new List<DirectoryItem<SpecificHeatCapacityType>>
            {
                new DirectoryItem<SpecificHeatCapacityType>("По методу Товарова", SpecificHeatCapacityType.TovarovMethod),
                new DirectoryItem<SpecificHeatCapacityType>("Другая", SpecificHeatCapacityType.Other)
            };
        }
    }
}
