﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Desktop.Model.NSI;
using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.NSI
{
    public static class SampleTypeDirectory
    {
        public static List<DirectoryItem<SampleType>> GetSampleTypes()
        {
            return new List<DirectoryItem<SampleType>>
            {
                new DirectoryItem<SampleType>("7,07 * 7,07 * 7,07", SampleType.Type07),
                new DirectoryItem<SampleType>("10 * 10 * 10", SampleType.Type10),
                new DirectoryItem<SampleType>("15 * 15 * 15", SampleType.Type15)
            };
        }
    }
}
