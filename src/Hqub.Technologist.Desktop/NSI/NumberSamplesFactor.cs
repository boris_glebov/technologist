﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Desktop.Model.NSI;
using Hqub.Technologist.Store;


namespace Hqub.Technologist.Desktop.NSI
{
    public static class NumberSamplesFactor
    {
        public static List<NumberSamplesFactorModel> GetScaleFactor()
        {
            return new List<NumberSamplesFactorModel>
            {
                new  NumberSamplesFactorModel(2, 1.13f),
                new  NumberSamplesFactorModel(3,1.69f),
                new  NumberSamplesFactorModel(4,2.06f),
                new  NumberSamplesFactorModel(5,2.33f),
                new  NumberSamplesFactorModel(6,2.5f)
            };

        }
    }
}
