﻿using System.Collections.Generic;
using System.Windows.Documents;
using Hqub.Technologist.Desktop.Model.NSI;

namespace Hqub.Technologist.Desktop.NSI
{
    public class RequiredDensityDirectory
    {
        public static new Dictionary<int, float> GetDirectory()
        {
            return new Dictionary<int, float>
            {
                {2, 1.07f},
                {3, 1.06f},
                {4, 1.05f},
                {5, 1.04f},
                {6, 1.02f},
                {7, 1.00f},
                {8, 0.98f},
                {9, 0.97f}
            };
        }

        public static float Get(int key)
        {
            var items = GetDirectory();

            if (key < 2)
                return items[2];

            if (key > 9)
                return items[9];

            return items[key];
        }
    }
}
