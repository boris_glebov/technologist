﻿using System.Collections.Generic;
using Hqub.Technologist.Desktop.Model.NSI;
using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.NSI
{
    public static class SlakedLimeTypeDirectory
    {
        public static List<DirectoryItem<SlakedLimeMethodType>> GetSlakedLimeTypes()
        {
            return new List<DirectoryItem<SlakedLimeMethodType>>
            {
                new DirectoryItem<SlakedLimeMethodType>("ГОСТ22688-77", SlakedLimeMethodType.GOST22688),
                new DirectoryItem<SlakedLimeMethodType>("Т60", SlakedLimeMethodType.T60)
            };
        }
    }
}
