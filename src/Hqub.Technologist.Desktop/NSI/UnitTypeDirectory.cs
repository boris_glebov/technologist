﻿using System.Collections.Generic;
using Hqub.Technologist.Desktop.Model.NSI;
using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.NSI
{
    public static class UnitTypeDirectory
    {
        public static List<DirectoryItem<UnitType>> GetUnitTypes()
        {
            return new List<DirectoryItem<UnitType>>
            {
                new DirectoryItem<UnitType>("кгс", UnitType.Kgf),
                new DirectoryItem<UnitType>("кN", UnitType.kN),
                new DirectoryItem<UnitType>("МПа", UnitType.MPa)
            };
        }
    }
}
