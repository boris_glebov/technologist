﻿using System.Collections.Generic;
using Hqub.Technologist.Desktop.Model.NSI;

namespace Hqub.Technologist.Desktop.NSI
{
    public class RequiredStrengthDirectory
    {
        public static Dictionary<int, float> GetDirectory()
        {
            return new Dictionary<int, float>
            {
                {6, 1.08f},
                {7, 1.10f},
                {8, 1.11f},
                {9, 1.12f},
                {10, 1.13f},
                {11, 1.14f},
                {12, 1.17f},
                {13, 1.22f},
                {14, 1.26f},
                {15, 1.32f},
                {16, 1.37f},
                {17, 1.43f},
                {18, 1.50f},
                {19, 1.57f},
            };
        }

        public static float Get(int key)
        {
            var items = GetDirectory();

            if (key < 2)
                return items[0];

            if (key > 19)
                return items[19];

            return items[key];
        }
    }
}
