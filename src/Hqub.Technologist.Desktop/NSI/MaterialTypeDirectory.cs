﻿using System.Collections.Generic;
using System.Linq;
using Hqub.Technologist.Desktop.Model.NSI;
using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.NSI
{

    public static class MaterialTypeDirectory
    {
        public static List<MaterialTypeModel> GetMaterialTypes()
        {
            return new List<MaterialTypeModel>
            {
                new MaterialTypeModel("Зола-унос", MaterialTypeEnum.Ash),
                new MaterialTypeModel("Зола отвал", MaterialTypeEnum.Ash2),
                new MaterialTypeModel("Известь", MaterialTypeEnum.Izvest),
                new MaterialTypeModel("Песок", MaterialTypeEnum.Sand),
                new MaterialTypeModel("Портланд", MaterialTypeEnum.Portland),
                new MaterialTypeModel("Цемент", MaterialTypeEnum.Cement)
                
                
            };
        }
    }
}
