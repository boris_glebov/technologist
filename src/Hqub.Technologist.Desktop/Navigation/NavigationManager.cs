﻿using System;
using System.Collections.Generic;
using Hqub.Technologist.Desktop.Modules;
using Hqub.Technologist.Desktop.ViewModel;

namespace Hqub.Technologist.Desktop.Navigation
{
	public interface INavigationManager
	{
		event NavigationManagerStateChanged NavigationManagerStateChanged;

		void GoNext(BaseNavigationModule module);

        void GoBack(BaseNavigationModule firstModule = null);

	    void GoToMain();

	    void GoTo(BaseNavigationModule module);

	    void Push(BaseNavigationModule module);

		void Refresh();

	    void Clear();

	    string GetLastModuleName();
	    void SetLastModuleName(string name);

		BaseNavigationModule GetTop();

        BaseNavigationModule GetPrev();

		BaseNavigationModule GetCurrent();

		IEnumerable<BaseNavigationModule> GetModulesStack();
	}
}
