﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Hqub.Technologist.Desktop.Events;
using Hqub.Technologist.Desktop.Model.Events;
using Hqub.Technologist.Desktop.Modules;
using Hqub.Technologist.Desktop.ViewModel;

namespace Hqub.Technologist.Desktop.Navigation
{
    public delegate void NavigationManagerStateChanged(INavigationManager sender);

    public class NavigationManagerImpl : INavigationManager
    {
        private AggregationEventService _aggregationEventService = AggregationEventService.Instance;
        private readonly Stack<BaseNavigationModule> _modulesStack = new Stack<BaseNavigationModule>();

        public event NavigationManagerStateChanged NavigationManagerStateChanged;

        public void GoNext(BaseNavigationModule module)
        {
//            Clear();

            Push(module);
            module.Show();

            SetModuleAsCurrent(module);

            NotifyNavigationStackChanged();
        }

        public void GetNextChain(BaseNavigationModule module)
        {
            Push(module);
            module.Show();

            SetModuleAsCurrent(module);

            NotifyNavigationStackChanged();
        }

        public void Push(BaseNavigationModule module)
        {
            _modulesStack.Push(module);
        }

        public void Refresh()
        {
            var currentModule = _modulesStack.Peek();
            foreach (var baseViewModel in currentModule.GetAllViewModels())
            {
                BindableViewModel model = baseViewModel;
                ThreadPool.QueueUserWorkItem(state => model.Refresh());
            }
        }

        public void Clear()
        {
            while (_modulesStack.Count > 0)
            {
                var currentModule = _modulesStack.Pop();
                LastModuleName = currentModule.Name;

                currentModule.BeforeBack();
                currentModule.Hide();
                currentModule.OnUnload();
            }
        }

        private string _lastModuleName;
        public string GetLastModuleName()
        {
            var temp = _lastModuleName;
            _lastModuleName = string.Empty;

            return temp;
        }

        public void SetLastModuleName(string name)
        {
            _lastModuleName = name;
        }

        public string LastModuleName { get; set; }

        public void GoBack(BaseNavigationModule firstModule = null)
        {
            if (_modulesStack.Count < 2)
                return;

            if (firstModule == null)
            {
                // Remove top element
                var currentModule = _modulesStack.Pop();
                currentModule.Hide();
                currentModule.OnUnload();

                firstModule = _modulesStack.Peek();
            }

            BaseNavigationModule topModule = null;

            while (true)
            {
                topModule = _modulesStack.Peek();
                if (topModule == firstModule)
                {
                    topModule.Show();
                    SetModuleAsCurrent(topModule);
                    break;
                }

                topModule.Hide();
                topModule.OnUnload();
                _modulesStack.Pop();
            }

            NotifyNavigationStackChanged();
        }

        public void GoToMain()
        {

        }

        public void GoTo(BaseNavigationModule module)
        {
            Clear();

            Push(module);
            module.Show();
            SetModuleAsCurrent(module);

            NotifyNavigationStackChanged();
        }

        public BaseNavigationModule GetTop()
        {
            return _modulesStack.Count == 0 ? null : _modulesStack.Peek();
        }

        public IEnumerable<BaseNavigationModule> GetModulesStack()
        {
            var modules = new BaseNavigationModule[_modulesStack.Count];
            _modulesStack.CopyTo(modules, 0);
            return modules.Reverse();
        }

        public BaseNavigationModule GetPrev()
        {
            var items = GetModulesStack().ToArray();
            if (items.Length < 2)
                return null;

            return items[items.Length - 2];
        }

        public BaseNavigationModule GetCurrent()
        {
            return _modulesStack.Count == 0 ? null : _modulesStack.FirstOrDefault(q => q.IsCurrent);
        }

        #region Private Methods

        private void SetModuleAsCurrent(BaseNavigationModule module)
        {
            foreach (var baseNavigationModule in _modulesStack)
            {
                baseNavigationModule.IsCurrent = false;
            }

            module.IsCurrent = true;
        }

        private void NotifyNavigationStackChanged()
        {
            var stackChangeEvent = _aggregationEventService.GetEvent<Events.Navigation.NavigationStackChange>();
            stackChangeEvent.Publish(new NavigationStackState(_modulesStack.ToList()));
        }

        #endregion
    }
}