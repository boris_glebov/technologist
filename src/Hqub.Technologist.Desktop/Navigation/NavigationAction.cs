namespace Hqub.Technologist.Desktop.Navigation
{
    public enum NavigationAction
    {
        Backward,
        Forward
    }
}