﻿    namespace Hqub.Technologist.Desktop.Navigation
{
    public static class NavigationHelper
    {
        private static INavigationManager _navigationManager;

        public static INavigationManager NavigationManager
        {
            get { return _navigationManager = _navigationManager ?? Locator.GetNavigationManager(); }
        }

        public static void GoMain()
        {
            NavigationManager.GoNext(Locator.Get<Modules.ShellModule>());
        }


        #region Продукт

        public static void GoToProductTest()
        {
            OpenProductRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Product.ProductTestModule>());
        }

        public static void GoToStatisiticProductTest()
        {
            OpenProductRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Product.StatisticProductTestModule>());
        }

        public static void GoToOpenReportProductTest()
        {

            OpenProductRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Product.ReportProductTestModule>());
        }

        public static void GoToOpenPassportProductTest()
        {
            OpenProductRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Product.PassportProductTestModule>());
        }

        public static void OpenProductRootPanel(bool isClear = true)
        {
            if (isClear)
                NavigationManager.Clear();

            GoMain();
            NavigationManager.GoNext(Locator.Get<Modules.Navigation.ProductRootModule>());
        }

        #endregion

        #region Настройки

        public static void GoToBrandSettings()
        {
            OpenSettingsRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Settings.BrandSettingsModule>());
        }

        public static void GoToMaterialSettings()
        {
            OpenSettingsRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Settings.MaterialSettingsModule>());
        }

        public static void GoToPositionSettings()
        {
            OpenSettingsRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Settings.PositionSettingsModule>());
        }

        public static void GoToTestMethodSettings()
        {
            OpenSettingsRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Settings.TestMethodSettingsModule>());
        }

        public static void GoToQualitySettings()
        {
            OpenSettingsRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Settings.QualitySettingsModule>());
        }
        
       
        public static void OpenSettingsRootPanel(bool isClear = true)
        {
            if (isClear)
                NavigationManager.Clear();

            GoMain();
            NavigationManager.GoNext(Locator.Get<Modules.Navigation.SettingsRootModule>());
        }

        #endregion

        #region Сырье

        public static void GoToTestCement()
        {
            OpenMaterialRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Materials.TestCementModule>());
        }

        public static void GoToTestLime()
        {
            OpenMaterialRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Materials.TestLimeModule>());
        }

        public static void GoToTestSludge()
        {
            OpenMaterialRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Materials.TestSludgeModule>());
        }

        public static void GoToTestSlime()
        {
            OpenMaterialRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Materials.TestSlimeModule>());
        }

        
        public static void OpenMaterialRootPanel(bool isClear= true)
        {
            if (isClear)
                NavigationManager.Clear();

            GoMain();
            NavigationManager.GoNext(Locator.Get<Modules.Navigation.MaterialRootModule>());
        }

        #endregion

        #region Производство

        public static void GoToAssessmentQuality()
        {
            OpenAssessmentRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Manufacture.AssessmentQualityModule>());
        }

        public static void GoToFunctionalControl()
        {
            OpenAssessmentRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Manufacture.FunctionalControlModule>());
        }
       
        public static void OpenAssessmentRootPanel(bool isClear = true)
        {
            if (isClear)
                NavigationManager.Clear();

            GoMain();
            NavigationManager.GoNext(Locator.Get<Modules.Navigation.ManufactureRootModule>());
        }

        #endregion

        #region Библиотека

        public static void GoToEmployees()
        {
            OpenDirectoryRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Directories.EmployeeModule>());
        }
        
        public static void GoToProductCharacter()
        {
            OpenDirectoryRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Directories.ProductCharacterModule>());
        }

        public static void GoToNomenclature()
        {
            OpenDirectoryRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Directories.NomenclatureModule>());
        }

        public static void GoToNomenclatureTypes()
        {
            OpenDirectoryRootPanel();
            NavigationManager.GoNext(Locator.Get<Modules.Directories.ProductTypeModule>());
        }


        public static void OpenDirectoryRootPanel(bool isClear = true)
        {
            if (isClear)
                NavigationManager.Clear();

            GoMain();
            NavigationManager.GoNext(Locator.Get<Modules.Navigation.DirectoryRootModule>());
        }

        #endregion
    }
}
