﻿namespace Hqub.Technologist.Desktop
{
    public class Metadata
    {
        public const string Version = "0.1.5";

        public const string Title = "АГБ Технолог (x86) v" + Version;
    }
}
