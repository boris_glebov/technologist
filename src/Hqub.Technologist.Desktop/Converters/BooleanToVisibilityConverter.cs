﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Hqub.Technologist.Desktop.Converters
{
	/// <summary>
	/// Позволяте цеплять свойтсво Visibility к булевым полям. 
	/// Параметр определяет, какое значение булева поля соответствует видимому состоянию, true по умолчанию.
	/// </summary>
	public class BooleanToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(targetType!= typeof(Visibility))
				throw new NotSupportedException();

			var par = true;
			if(parameter!= null)
				Boolean.TryParse(parameter.ToString(), out par);

			var valueB = (Boolean)value;
			if (!par) valueB = !valueB;

			return valueB ? Visibility.Visible : Visibility.Collapsed;
		}

		public object ConvertBack(object value, Type targetType, object parameter,
		                          CultureInfo culture)
		{
			var ret =  (Visibility)value == Visibility.Visible;

			var par = true;
			if (parameter != null)
				Boolean.TryParse(parameter.ToString(), out par);

			return par ? ret : !ret;
		}
	}
}