﻿using System;
using System.Globalization;
using System.Windows.Data;
using Hqub.Technologist.Desktop.Model;

namespace Hqub.Technologist.Desktop.Converters
{
   public class TypeReportToStringConverter : IValueConverter
    {
       public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
       {
           var reportType = (TypeReport) value;

           switch (reportType)
           {
               case TypeReport.Full:
                   return "Полный";
               case TypeReport.Default:
                   return "Стандартный";
               case TypeReport.Characters:
                   return "Характеристики";
               default:
                   return "Не известный тип";
           }
       }

       public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
       {
           throw new NotImplementedException();
       }
    }
}
