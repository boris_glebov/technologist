﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Hqub.Technologist.Desktop.Converters
{
	public class NullToBooleanConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool nullIsTrue = bool.Parse((string)parameter ?? "false");

			if (nullIsTrue)
				return value == null;
			else
				return value != null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}