﻿using System;
using System.Collections;
using System.Linq;
using System.Windows.Data;

namespace Hqub.Technologist.Desktop.Converters
{
    public class PluralizingConverter : IValueConverter
    {
        public object Convert(object v, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int count = 0;

            if (v == null)
                count = 0;
            else if (v is int) 
                count = (int) v;
            else if (v is IEnumerable)
                 count = ((IEnumerable) v).Cast<object>().Count();
            else
                throw new ArgumentException();

            var paramParts = ((string) parameter).Split(';');
            var singular = paramParts[0];
            var plural = paramParts[1];
            return count == 1 ? singular : plural;
        }
        
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
