﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Hqub.Technologist.Desktop.Converters
{
	public class InvertStringToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool nullIsCollapse = bool.Parse((string)parameter ?? "true");

            if (nullIsCollapse)
                return string.IsNullOrEmpty((string)value) ? Visibility.Visible : Visibility.Collapsed;
            else
                return string.IsNullOrEmpty((string)value) ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
