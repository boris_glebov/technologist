﻿using System;
using System.Globalization;
using System.Windows.Data;
using Hqub.Technologist.Desktop.Cache;

namespace Hqub.Technologist.Desktop.Converters
{
    public class EmployeeIdToEmployeeNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var id = (Guid) value;

            var entity = EmployeeCache.Get(id);

            return entity != null ? entity.FullName : String.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
