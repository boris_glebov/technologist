﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Hqub.Technologist.Desktop.Converters
{
	public class PluralizingValueConverter : IValueConverter
	{
		/// <summary>
		/// Метод выбирает слово с правильным окончанием для числительного
		/// </summary>
		/// <param name="value">например 3</param>
		/// <param name="words">например {день;дня;дней} </param>
		/// <returns>результат будет "дня"</returns>
		public static string SelectPluralizeWord(int value, string[] words)
		{
			if (words.Length < 3)
				throw new Exception("Не переданы три формы слова, например 'звонок;звонка;звонков'");// день;дня;дней
			
			string ending;

			value = value % 100;
			if (value >= 11 && value <= 19)
				ending = words[2];
			else
			{
				var i = value % 10;
				switch (i)
				{
					case 1:
						ending = words[0];
						break;
					case 2:
					case 3:
					case 4:
						ending = words[1];
						break;
					default:
						ending = words[2];
						break;
				}
			}
			return ending;
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var number = (int)value;
			var endings = ((string)parameter).Split(';');
			
			var result = SelectPluralizeWord(number, endings);

			return result;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}
	}
}
