﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;

namespace Hqub.Technologist.Desktop.Converters
{
	public class EnumToDescriptionConverter : IValueConverter
	{
		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{

			if (value == null)
				return string.Empty;

			var enumType = value.GetType();
			if (!enumType.IsEnum)
				throw new ArgumentException("Not a enum", "value");

			var enumMember = enumType.GetMember(value.ToString()).FirstOrDefault();
			if (enumMember == null)
				return value.ToString();

			var descAttr =
				(DescriptionAttribute)enumMember.GetCustomAttributes(typeof(DescriptionAttribute), false).FirstOrDefault();
			if (descAttr != null)
				return descAttr.Description;

			return value.ToString();
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotSupportedException();
		}

		#endregion
	}
}