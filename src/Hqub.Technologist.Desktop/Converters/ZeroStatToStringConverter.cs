﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Hqub.Technologist.Desktop.Converters
{
    public class ZeroStatToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = System.Math.Round((float) value);

            if (v == 0)
                return "недостаточно данных";

            return v;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
