﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using Telerik.Windows.Controls.Data;

namespace Hqub.Technologist.Desktop.Converters
{
    public class ErrorToVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var errors = (IList<ErrorInfo>)value;
            var fieldName = (string)parameter;

            var error = errors.FirstOrDefault(e => e.SourceFieldDisplayName == fieldName);

            return error != null ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
