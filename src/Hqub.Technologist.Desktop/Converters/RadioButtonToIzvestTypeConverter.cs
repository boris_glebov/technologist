﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Hqub.Technologist.Desktop.Converters
{
    public class RadioButtonToIzvestTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return false;

            var izvestType = (int) value;
            var newIzvestType = int.Parse((string)parameter);

            return izvestType == newIzvestType;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var newIzvestType = int.Parse((string)parameter);

            return newIzvestType;
        }
    }
}
