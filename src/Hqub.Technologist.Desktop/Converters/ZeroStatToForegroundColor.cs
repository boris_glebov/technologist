﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Hqub.Technologist.Desktop.Converters
{
    public class ZeroStatToForegroundColor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = System.Math.Round((float) value, 2);

            if (v == 0)
                return new SolidColorBrush(Colors.Red);

            return new SolidColorBrush(Colors.Black);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
