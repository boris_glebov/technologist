﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Hqub.Technologist.Desktop.Converters
{
    public class InvertBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool? boolValue = value as bool?;

            if (boolValue.HasValue)
            {
                return !boolValue.Value;
            }
            else
            {
                return true;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
    }
}
