﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Hqub.Technologist.Desktop.Converters
{
	public class ZeroToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var itemCount = 0;
			int.TryParse(value.ToString(), out itemCount);
			
			bool zeroIsColapse = bool.Parse((string)parameter ?? "true");

			if (zeroIsColapse)
				return itemCount <= 0 ? Visibility.Collapsed : Visibility.Visible;
		    return itemCount <= 0 ? Visibility.Visible : Visibility.Collapsed;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}