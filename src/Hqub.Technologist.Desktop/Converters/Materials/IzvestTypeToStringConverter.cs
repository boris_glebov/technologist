﻿using System;
using System.Globalization;
using System.Windows.Data;
using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.Converters.Materials
{
    public class IzvestTypeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return string.Empty;

            var izvestType = (IzvestType) value;

            return izvestType == IzvestType.Calcium ? "Кальциевая" : "Магниевая";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
