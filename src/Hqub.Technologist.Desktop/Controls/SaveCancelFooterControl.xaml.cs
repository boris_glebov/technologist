﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Hqub.Technologist.Desktop.Controls
{
    /// <summary>
    /// Interaction logic for SaveCancelFooterControl.xaml
    /// </summary>
    public partial class SaveCancelFooterControl : UserControl
    {
        public  static readonly DependencyProperty SaveCommandProperty = DependencyProperty.Register(
            "SaveCommand", typeof (ICommand), typeof (SaveCancelFooterControl), new PropertyMetadata(default(ICommand)));

        public ICommand SaveCommand
        {
            get { return (ICommand) GetValue(SaveCommandProperty); }
            set { SetValue(SaveCommandProperty, value); }
        }

        public static readonly DependencyProperty AcceptCommandProperty = DependencyProperty.Register(
            "AcceptCommand", typeof (ICommand), typeof (SaveCancelFooterControl), new PropertyMetadata(default(ICommand)));

        public ICommand AcceptCommand
        {
            get { return (ICommand) GetValue(AcceptCommandProperty); }
            set { SetValue(AcceptCommandProperty, value); }
        }

        public static readonly DependencyProperty AddCommandProperty = DependencyProperty.Register(
            "AddCommand", typeof (ICommand), typeof (SaveCancelFooterControl), new PropertyMetadata(default(ICommand)));

        public ICommand AddCommand
        {
            get { return (ICommand) GetValue(AddCommandProperty); }
            set { SetValue(AddCommandProperty, value); }
        }

        public static readonly DependencyProperty AddVisibilityProperty = DependencyProperty.Register(
            "AddVisibility", typeof (Visibility), typeof (SaveCancelFooterControl), new PropertyMetadata(Visibility.Collapsed));

        public Visibility AddVisibility
        {
            get { return (Visibility) GetValue(AddVisibilityProperty); }
            set { SetValue(AddVisibilityProperty, value); }
        }

        public static readonly DependencyProperty CancelCommandProperty = DependencyProperty.Register(
            "CancelCommand", typeof (ICommand), typeof (SaveCancelFooterControl), new PropertyMetadata(default(ICommand)));

        public ICommand CancelCommand
        {
            get { return (ICommand) GetValue(CancelCommandProperty); }
            set { SetValue(CancelCommandProperty, value); }
        }

        public static readonly DependencyProperty SaveCommandVisibilityProperty = DependencyProperty.Register(
            "SaveCommandVisibility", typeof (Visibility), typeof (SaveCancelFooterControl), new PropertyMetadata(Visibility.Visible));

        public Visibility SaveCommandVisibility
        {
            get { return (Visibility) GetValue(SaveCommandVisibilityProperty); }
            set { SetValue(SaveCommandVisibilityProperty, value); }
        }

        public static readonly DependencyProperty CancelCommandVisibilityProperty = DependencyProperty.Register(
            "CancelCommandVisibility", typeof(Visibility), typeof(SaveCancelFooterControl), new PropertyMetadata(Visibility.Visible));

        public Visibility CancelCommandVisibility
        {
            get { return (Visibility) GetValue(CancelCommandVisibilityProperty); }
            set { SetValue(CancelCommandVisibilityProperty, value); }
        }

        public static readonly DependencyProperty AcceptCommandVisibilityProperty = DependencyProperty.Register(
            "AcceptCommandVisibility", typeof(Visibility), typeof(SaveCancelFooterControl), new PropertyMetadata(Visibility.Collapsed));

        public Visibility AcceptCommandVisibility
        {
            get { return (Visibility) GetValue(AcceptCommandVisibilityProperty); }
            set { SetValue(AcceptCommandVisibilityProperty, value); }
        }

        public SaveCancelFooterControl()
        {
            InitializeComponent();
        }
    }
}
