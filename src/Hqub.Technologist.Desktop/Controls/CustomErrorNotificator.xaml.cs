﻿using System.Windows;
using System.Windows.Controls;

namespace Hqub.Technologist.Desktop.Controls
{
    /// <summary>
    /// Interaction logic for CustomErrorNotificator.xaml
    /// </summary>
    public partial class CustomErrorNotificator : UserControl
    {
        public static readonly DependencyProperty ErrorContentProperty = DependencyProperty.Register(
            "ErrorContent", typeof (string), typeof (CustomErrorNotificator), new PropertyMetadata(default(string)));

        public string ErrorContent
        {
            get { return (string) GetValue(ErrorContentProperty); }
            set { SetValue(ErrorContentProperty, value); }
        }

        public CustomErrorNotificator()
        {
            InitializeComponent();
        }
    }
}
