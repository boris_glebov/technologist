﻿using System.Windows.Controls;
using System.Windows.Input;
using Telerik.Windows.Controls;

namespace Hqub.Technologist.Desktop.Controls
{
    /// <summary>
    /// Interaction logic for NavigationRootItemControl.xaml
    /// </summary>
    public partial class NavigationRootItemControl : UserControlNotifyable
    {
        public NavigationRootItemControl(Model.Navigation.NavigationItemModel model)
        {
            InitializeComponent();

            Model = model;
            DataContext = model;
        }

        #region Properties

        public Model.Navigation.NavigationItemModel Model { get; set; }

        #endregion

        #region Commands

        public ICommand GoCommand { get { return new DelegateCommand(GoCommandExecute);} }
        private void GoCommandExecute(object obj)
        {
            if (Model.GoToAction != null)
                Model.GoToAction();
        }

        #endregion
    }
}
