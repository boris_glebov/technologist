﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Events;
using Hqub.Technologist.Desktop.Modules;
using Hqub.Technologist.Desktop.Navigation;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.Controls
{
    /// <summary>
    /// Interaction logic for NavigationChainControl.xaml
    /// </summary>
    public partial class NavigationChainControl : UserControlNotifyable
    {
        #region Fields

        private readonly INavigationManager _navigationManager;

        private readonly AggregationEventService _aggregationEventService = AggregationEventService.Instance;
        private ObservableCollection<BaseNavigationModule> _module;
        private BaseNavigationModule _moduleSelected;
        private Visibility _panelVisibility = Visibility.Collapsed;

        #endregion

        #region .ctor

        public NavigationChainControl()
        {
            InitializeComponent();

            _navigationManager = Locator.GetNavigationManager();
            
            LoadModules();
            Subscribe();
        }

        #endregion

        #region Commands

        public ICommand BackCommand { get { return new DelegateCommand(BackCommandExecute);} }

        private void BackCommandExecute()
        {
            _navigationManager.GoBack(ModuleSelected);
        }

        #endregion

        #region Properties

        public ObservableCollection<BaseNavigationModule> Modules
        {
            get { return _module; }
            set
            {
                _module = value;
                OnPropertyChanged();
                OnPropertyChanged("BackButtonVisibility");
            }
        }

        public BaseNavigationModule ModuleSelected
        {
            get { return _moduleSelected; }
            set
            {
                if (value != null && _moduleSelected != null && _moduleSelected.Name == value.Name)
                    return;

                SetSelectedModule(value);
            }
        }

        public Visibility PanelVisibility
        {
            get { return _panelVisibility; }
            set
            {
                _panelVisibility = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        #region Private Methods

        private void SetSelectedModule(BaseNavigationModule module, bool manual = false)
        {
            _moduleSelected = module;
            if (module != null && !manual)
                _navigationManager.GoBack(module);
            OnPropertyChanged("ModuleSelected");
        }

        private void LoadModules()
        {
            Modules = new ObservableCollection<BaseNavigationModule>(_navigationManager.GetModulesStack().ToList());
        }

        private void Subscribe()
        {
            var navigationStackChangeEvent = _aggregationEventService.GetEvent<Events.Navigation.NavigationStackChange>();
            navigationStackChangeEvent.Subscribe((data) =>
            {
                data.Modules.Reverse();
                Modules = new ObservableCollection<BaseNavigationModule>(data.Modules);
                PanelVisibility = Modules.Count == 0 ? Visibility.Collapsed : Visibility.Visible;

                if (data.Modules.Count > 0)
                    SetSelectedModule(data.Modules[data.Modules.Count - 1], true);
            });
        }

        #endregion
    }
}
