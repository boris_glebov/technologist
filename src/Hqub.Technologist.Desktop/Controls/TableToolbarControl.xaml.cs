﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Hqub.Technologist.Desktop.Controls
{
    /// <summary>
    /// Interaction logic for TableToolbarControl.xaml
    /// </summary>
    public partial class TableToolbarControl : UserControl
    {
        #region AddCommand

        public static readonly DependencyProperty AddCommandProperty = DependencyProperty.Register(
            "AddCommand", typeof (ICommand), typeof (TableToolbarControl), new PropertyMetadata(default(ICommand)));

        public ICommand AddCommand
        {
            get { return (ICommand) GetValue(AddCommandProperty); }
            set { SetValue(AddCommandProperty, value); }
        }

        #endregion

        #region RemoveCommand

        public static readonly DependencyProperty RemoveCommandProperty = DependencyProperty.Register(
            "RemoveCommand", typeof (ICommand), typeof (TableToolbarControl), new PropertyMetadata(default(ICommand)));

        public ICommand RemoveCommand
        {
            get { return (ICommand) GetValue(RemoveCommandProperty); }
            set { SetValue(RemoveCommandProperty, value); }
        }

        #endregion

        #region ReloadCommand

        public static readonly DependencyProperty ReloadCommandProperty = DependencyProperty.Register(
            "ReloadCommand", typeof(ICommand), typeof(TableToolbarControl), new PropertyMetadata(default(ICommand)));

        public ICommand ReloadCommand
        {
            get { return (ICommand) GetValue(ReloadCommandProperty); }
            set { SetValue(ReloadCommandProperty, value); }
        }

        public static readonly DependencyProperty ReloadVisibilityProperty = DependencyProperty.Register(
            "ReloadVisibility", typeof (Visibility), typeof (TableToolbarControl), new PropertyMetadata(default(Visibility)));

        public Visibility ReloadVisibility
        {
            get { return (Visibility) GetValue(ReloadVisibilityProperty); }
            set { SetValue(ReloadVisibilityProperty, value); }
        }

        #endregion

        #region SettingsCommand

        public static readonly DependencyProperty SettingsCommandProperty = DependencyProperty.Register(
            "SettingsCommand", typeof (ICommand), typeof (TableToolbarControl), new PropertyMetadata(default(ICommand)));

        public ICommand SettingsCommand
        {
            get { return (ICommand) GetValue(SettingsCommandProperty); }
            set { SetValue(SettingsCommandProperty, value); }
        }

        public static readonly DependencyProperty SettingsVisibilityProperty = DependencyProperty.Register(
            "SettingsVisibility", typeof(Visibility), typeof(TableToolbarControl), new PropertyMetadata(default(Visibility)));

        public Visibility SettingsVisibility
        {
            get { return (Visibility) GetValue(SettingsVisibilityProperty); }
            set { SetValue(SettingsVisibilityProperty, value); }
        }

        #endregion

        public TableToolbarControl()
        {
            InitializeComponent();
        }
    }
}
