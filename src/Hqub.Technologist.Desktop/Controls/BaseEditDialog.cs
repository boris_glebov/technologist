﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Model;
using Microsoft.Practices.Prism.Commands;
using Telerik.Windows.Controls.Data;

namespace Hqub.Technologist.Desktop.Controls
{
    public abstract class BaseEditDialog : Window, INotifyPropertyChanged
    {
        private ObservableCollection<ErrorInfo> _errors;
        private bool _isNew;

        protected BaseEditDialog()
        {
            Owner = Application.Current.MainWindow;
            WindowStartupLocation = WindowStartupLocation.CenterOwner;

            Errors = new ObservableCollection<ErrorInfo>();
            DataContext = this;

            ListenEvents();
        }

        private void ListenEvents()
        {
            KeyDown += (sender, args) =>
            {
                if (args.Key == Key.Escape &&
                    MessageBox.Show("Все не сохраненные данные будут утеряны. Продолжить?", "Закрытие окна",
                        MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    Close();
                }
            };
        }

        #region Set Model

        public abstract void SetModel(EntityBase model);

        #endregion

        #region Commands

        public ICommand CancelCommand
        {
            get { return new DelegateCommand(CancelCommandExecute); }
        }

        private void CancelCommandExecute()
        {
            DialogResult = false;
            Close();
        }

        #endregion

        #region Validation

        /// <summary>
        /// Возвращает true, если форма не прошла валидацию
        /// </summary>
        /// <returns></returns>
        public bool IsHasError()
        {
            return (Errors != null && Errors.Count > 0) || ErrorCounter > 0;
        }

        protected virtual void Validate()
        {
            ResetCustomErrors();
        }

        #region Обрабатываем поступающие ошибки

        protected int ErrorCounter;
        protected void ValidationError(object sender, ValidationErrorEventArgs e)
        {
            if (e.Action == ValidationErrorEventAction.Added)
                ErrorCounter++;
            else
                ErrorCounter--;
        }

        #endregion

        /// <summary>
        /// Добавить ошибку в Summary
        /// </summary>
        /// <param name="error"></param>
        protected void AddError(ErrorInfo error)
        {
            Errors.Add(error);
            OnPropertyChanged("Errors");
        }


        protected void AddError(string fieldName, string message)
        {
            AddError(new ErrorInfo
            {
                ErrorContent = message,
                SourceFieldDisplayName = fieldName
            });
        }

        protected void ResetCustomErrors()
        {
            Errors.Clear();
            OnPropertyChanged("Errors");
        }

        #endregion

        #region Save

        public virtual void Save(ISaver model)
        {
            Validate();

            if (IsHasError()) return;

            BeforeSave();

            if (!model.Save())
            {
                AddError("", Properties.Settings.Default.UnknowError);
                return;
            }

            AfterSave();

            DialogResult = true;
            Close();
        }

        protected virtual void BeforeSave()
        {
            
        }

        protected virtual void AfterSave()
        {
            
        }

        #endregion

        #region Properties

        public bool IsNew
        {
            get { return _isNew; }
            set
            {
                _isNew = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<ErrorInfo> Errors
        {
            get { return _errors; }
            set
            {
                _errors = value;
                OnPropertyChanged();
            }
        }

        public Visibility IsAddVisibility
        {
            get { return IsNew ? Visibility.Visible : Visibility.Collapsed; }
        }

        public Visibility IsSaveVisibility
        {
            get { return !IsNew ? Visibility.Visible : Visibility.Collapsed; }
        }

        #endregion

        #region Implementation INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
