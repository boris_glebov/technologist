﻿using System.Windows.Controls;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Navigation;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.Controls
{
    /// <summary>
    /// Interaction logic for MainMenuControl.xaml
    /// </summary>
    public partial class MainMenuControl : UserControl
    {
        private readonly INavigationManager _navigationManager;

        public MainMenuControl()
        {
            InitializeComponent();

            _navigationManager = Locator.GetNavigationManager();
        }

        #region Производство

        public ICommand OpenAssessmentQuality
        {
            get { return new DelegateCommand(OpenAssessmentQualityExecute); }
        }

        private void OpenAssessmentQualityExecute()
        {
            NavigationHelper.GoToAssessmentQuality();
        }

        public ICommand OpenFunctionalControlCommand
        {
            get { return new DelegateCommand(OpenFunctionalControlCommandExecute); }
        }

        private void OpenFunctionalControlCommandExecute()
        {
            NavigationHelper.GoToFunctionalControl();
        }

        #endregion

        #region Продукт

        public ICommand OpenProductTestCommand
        {
            get { return new DelegateCommand(OpenProductTestCommandExecute); }
        }

        private void OpenProductTestCommandExecute()
        {
            NavigationHelper.GoToProductTest();
        }

        public ICommand OpenStatisticProductTestCommand
        {
            get { return new DelegateCommand(OpenStatisticProductTestCommandExecute); }
        }

        private void OpenStatisticProductTestCommandExecute()
        {
           NavigationHelper.GoToStatisiticProductTest();
        }

        public ICommand OpenReportProductTestCommand
        {
            get { return new DelegateCommand(OpenReportProductTestCommandExecute); }
        }

        private void OpenReportProductTestCommandExecute()
        {
            NavigationHelper.GoToOpenReportProductTest();
        }

        public ICommand OpenPassportProductTestCommand
        {
            get { return new DelegateCommand(OpenPassportProductTestCommandExecute); }
        }

        private void OpenPassportProductTestCommandExecute()
        {
           NavigationHelper.GoToOpenPassportProductTest();
        }

     

        #endregion

        #region Библиотека

        public ICommand OpenEmployeesCommand
        {
            get { return new DelegateCommand(OpenEmployeesCommandExecute); }
        }

        private void OpenEmployeesCommandExecute()
        {
            NavigationHelper.GoToEmployees();

        }

        public ICommand OpenProductCharacterCommand
        {
            get { return new DelegateCommand(OpenProductCharacterCommandExecute); }
        }

        private void OpenProductCharacterCommandExecute()
        {
            NavigationHelper.GoToProductCharacter();
        }


        public ICommand OpenNomenclatureTypesCharacterCommand
        {
            get { return new DelegateCommand(OpenNomenclatureTypesCharacterCommandExecute); }
        }

        private void OpenNomenclatureTypesCharacterCommandExecute()
        {
            NavigationHelper.GoToNomenclatureTypes();
        }


        public ICommand OpenNomenclatureCommand
        {
            get { return new DelegateCommand(OpenNomenclatureCommandExecute);}
        }

        private void OpenNomenclatureCommandExecute()
        {
            NavigationHelper.GoToNomenclature();
        }

        public ICommand OpenProductTypeCommand
        {
            get { return new DelegateCommand(OpenProductTypeCommandExecute); }
        }

        private void OpenProductTypeCommandExecute()
        {
            NavigationHelper.GoToNomenclatureTypes();
        }

        #endregion

        #region Сырье

        public ICommand OpenTestCementCommand
        {
            get { return new DelegateCommand(OpenTestCementCommandExecute); }
        }

        private void OpenTestCementCommandExecute()
        {
            NavigationHelper.GoToTestCement();
        }

        public ICommand OpenTestLimeCommand
        {
            get { return new DelegateCommand(OpenTestLimeCommandExecute); }
        }

        private void OpenTestLimeCommandExecute()
        {
            NavigationHelper.GoToTestLime();
        }

        public ICommand OpenTestSludgeCommand
        {
            get { return new DelegateCommand(OpenTestSludgeCommandExecute); }
        }

        private void OpenTestSludgeCommandExecute()
        {
           NavigationHelper.GoToTestSludge();
        }

        public ICommand OpenTestSlimeCommand
        {
            get { return new DelegateCommand(OpenTestSlimeCommandExecute); }
        }

        private void OpenTestSlimeCommandExecute()
        {
            NavigationHelper.GoToTestSlime();
        }
       

        #endregion

        #region Настройки

        public ICommand OpenBrandSettingsCommand
        {
            get { return new DelegateCommand(OpenBrandSettingsCommandExecute); }
        }

        private void OpenBrandSettingsCommandExecute()
        {
            NavigationHelper.GoToBrandSettings();
        }

        public ICommand OpenMaterialSettingsCommand
        {
            get { return new DelegateCommand(OpenMaterialSettingsCommandExecute); }
        }

        private void OpenMaterialSettingsCommandExecute()
        {
            NavigationHelper.GoToMaterialSettings();
        }

        public ICommand OpenPositionSettingsCommand
        {
            get { return new DelegateCommand(OpenPositionSettingsCommandExecute); }
        }

        private void OpenPositionSettingsCommandExecute()
        {
            NavigationHelper.GoToPositionSettings();
        }

        public ICommand OpenTestMethodSettingsCommand { get { return new DelegateCommand(OpenTestMethodSettingsCommandExecute);} }

        private void OpenTestMethodSettingsCommandExecute()
        {
            NavigationHelper.GoToTestMethodSettings();
        }

        public ICommand OpenQualitySettingsCommand { get { return new DelegateCommand(OpenQualitySettingsCommandExecute); } }

        private void OpenQualitySettingsCommandExecute()
        {
            NavigationHelper.GoToQualitySettings();
        }

       
        #endregion
    }
}
