﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;
using System.Linq.Expressions;


namespace Hqub.Technologist.Desktop.Utitlities
{
    public static class StatisticHelper
    {
        public static void UpdateAsync()
        {
            ThreadPool.QueueUserWorkItem(delegate
            {
                try
                {
                    Update();
                }
                catch (Exception exception)
                {
                    Logger.Main.InfoException("!!! Не удалось расчитать статистику.", exception);
                    Logger.Main.ErrorException("StatStatisticHelper.UpdateAsync", exception);
                }
            });
        }

        public static List<Statistic> Update(DateTime? period = null, bool isSave = true)
        {
            var brandRepository = Locator.GetBrandRepository();
            var productRepository = Locator.Get<IProductTestRepository>();
            var statisitcRepository = Locator.Get<IStatisticRepository>();

            if (period == null)
                period = DateTime.Now;

            var now = new DateTime(period.Value.Year, period.Value.Month, 1);

            var brands = brandRepository.List();

            var items = new List<Statistic>();

            foreach (var brand in brands)
            {
                foreach (var brandCalss in brand.ProductClasses.Split(';'))
                {
                    // 1. узнаем. Посчитана ли статистика за этот месяц. Если уже посчитана то ничего не делаем
                    var currentStatInfo = statisitcRepository.Get(brand.Id, brandCalss, now.Month, now.Year);
                    if (currentStatInfo != null)
                        continue;

                    // 2.
                    currentStatInfo = new Statistic
                    {
                        Id = Guid.NewGuid(),
                        MarkId = brand.Id,
                        MarkName = brand.Name,
                        MarkClass = brandCalss,
                        Month = DateTime.Now.Month,
                        Year = DateTime.Now.Year
                    };


                    // 3. Пытаемся получить данные за прошлый месяц 
                    var prevDateTime = now.AddMonths(-1);
                    var prevStatistic = statisitcRepository.Get(brand.Id, brandCalss, prevDateTime.Month,
                        prevDateTime.Year);
                    if (prevStatistic == null)
                    {
                        currentStatInfo.StrengthRequired = 1.43*GetMarkClassStrength(brandCalss) * 10.2;
                        currentStatInfo.DensityRequired = GetMarkDensity(brand.Name);
                    }
                    else
                    {
                        var brand1 = brand;
                        var products = productRepository.Where(
                            p =>
                                p.IsAccepted &&
                                (p.TestDate.HasValue && p.TestDate >= prevDateTime && p.TestDate < now)
                                && p.Lot1.Nomenclature.Brand.Id == brand1.Id);

                        if (products.Count == 0)
                            continue;

                        var averageStrengthVariation =
                            (int) System.Math.Round(products.Average(p => p.StrenghtVariation));
                        var averageDensityVariation = (int) System.Math.Round(products.Average(p => p.DensityVariation));

                        currentStatInfo.StrengthRequired = GetMarkClassStrength(brandCalss)*
                                                           NSI.RequiredStrengthDirectory.Get(averageStrengthVariation);
                        currentStatInfo.DensityRequired = GetMarkDensity(brand.Name)*
                                                          NSI.RequiredDensityDirectory.Get(averageDensityVariation);
                    }

                    items.Add(currentStatInfo);
                    if (isSave)
                        statisitcRepository.Save(currentStatInfo);
                }
            }

            return items;
        }

        public static List<StatisticModel> Get(DateTime? date = null)
        {
            if (date == null)
                date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);


            return AutoMapper.Mapper.Map<List<StatisticModel>>(Update(date, false));
        }

        private static int GetMarkDensity(string markName)
        {
            var markDensityString = markName.Substring(1);

            int density;
            int.TryParse(markDensityString, out density);

            return density;
        }

        private static float GetMarkClassStrength(string markClassName)
        {
            var markStrengthString = markClassName.Substring(1);

            float strength;
            float.TryParse(markStrengthString, out strength);

            return strength;
        }

        public static string GetDefaultMark(float denisty)
        {
            int[] densities = new int[] { 200, 300, 350, 400, 500, 600, 700, 800 };
            var mark = densities.Where(d => d > denisty).OrderBy(d => d).First();
            return string.Format("D{0}", mark);
        }

        public static string GetDefaultClass(float strenght)
        {
            var strenghts = new float[] { 1.5f, 2.0f, 2.5f, 3.5f, 5.0f, 7.0f };
            var requireStrenghts = strenghts.Select(s => 1.43f * s * 10.2f).ToList();

            var classMarkIdx = requireStrenghts.IndexOf(requireStrenghts.Where(s => s > strenght).OrderBy(s=>s).First());
            return string.Format("B{0:0.0}", strenghts[classMarkIdx]);
        }
    }
}
