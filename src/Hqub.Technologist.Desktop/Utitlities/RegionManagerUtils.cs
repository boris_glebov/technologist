﻿using System.Linq;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Technologist.Desktop.Utitlities
{
    public static class RegionManagerUtils
    {
        public static void ClearRegion(string regionName)
        {
            var regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();

            foreach (var view in regionManager.Regions[regionName].ActiveViews.ToArray())
            {
                regionManager.Regions[regionName].Remove(view);
            }
        }

        public static void AddToRegion(string regionName, object view)
        {
            ClearRegion(regionName);

            var regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();

            regionManager.AddToRegion(regionName, view);
        }
    }
}
