﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.Utitlities
{
    public static class DynamicEntityUtils
    {
        public static Dictionary<string, string> GetSchema(QualityControlType t)
        {
            var repository = Locator.GetQualityControlSettingsRepository();

            var columns = repository.Where(x => x.ControlType == (int) t);
            var dict = columns.ToDictionary(clm => clm.Name, clm => String.Empty);
            return dict;
        } 
    }
}
