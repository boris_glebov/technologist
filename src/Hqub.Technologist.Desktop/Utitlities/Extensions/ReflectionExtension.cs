﻿namespace Hqub.Technologist.Desktop.Utitlities.Extensions
{
    public static class ReflectionExtension
    {
        public static object GetPropertyValue(this object o, string name)
        {
            return o.GetType().GetProperty(name).GetValue(o);
        }

        public static void SetPropertyValue(this object o, string name, object val)
        {
            o.GetType().GetProperty(name).SetValue(o, val);
        }
    }
}
