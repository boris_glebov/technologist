﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Hqub.Technologist.Desktop.Model;

namespace Hqub.Technologist.Desktop.Utitlities.Extensions
{
    public class Item : Notifyable
    {
        public Item()
        {
            
        }

        public Item(int id, float value)
        {
            Id = id.ToString();
            Value = value.ToString();
        }

        public Item(string id, string value)
        {
            Id = id;
            Value = value;
        }

        private string _id;
        private string _value;

        [XmlAttribute(AttributeName = "id")]
        public string Id
        {
            get { return _id; }
            set
            {
                _id = value; 
                OnPropertyChanged();
            }
        }

        [XmlAttribute(AttributeName = "value")]
        public string Value
        {
            get { return _value; }
            set
            {
                _value = value; 
                OnPropertyChanged();
            }
        }
    }

    public static class SerializeExtension
    {
        public static string SerializeDictionary(Dictionary<string, string> d)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Item[]),
                                 new XmlRootAttribute() { ElementName = "items" });

            using (var textWriter = new StringWriter())
            {
                serializer.Serialize(textWriter,
                    d.Select(kv => new Item() {Id = kv.Key, Value = kv.Value}).ToArray());

                return textWriter.ToString();
            }
        }

        public static Dictionary<string, string> DeserializeDictionary(string source)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Item[]),
                                new XmlRootAttribute() { ElementName = "items" });

            using (var textReader = new StringReader(source))
            {
                var orgDict = ((Item[])serializer.Deserialize(textReader))
                .ToDictionary(i => i.Id, i => i.Value);

                return orgDict;
            }
        }

        public static string SerializeObject<T>(this T toSerialize)
        {
            var xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (var textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public static T DeserializeObject<T>(this string source)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var textReader = new StringReader(source))
            {
                var o = xmlSerializer.Deserialize(textReader);
                return (T)o;
            }
        }
    }
}
