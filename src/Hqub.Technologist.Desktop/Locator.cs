﻿using Hqub.Technologist.Desktop.Navigation;
using Hqub.Technologist.Store.Repository.Interfaces;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Technologist.Desktop
{
    public static class Locator
    {
        #region CacheRepository Instance

        private static ISampleRepository _sampleRepository;
        private static ISampleBottleRepository _sampleBottleRepository;
        private static IEmployeeRepository _employeeRepository;
        private static ILotRepository _lotRepository;
        private static IBrandRepository _brandRepository;
        private static ITestMethodRepository _testMethodRepository;
        private static IProductCharacterRepository _productCharacterRepository;
        private static IProductTypeRepository _productTypeRepository;
        private static IProductTestRepository _productTestRepository;
        private static INomenclatureRepository _nomenclatureRepository;
        private static IStatisticRepository _statisticRepository;
        private static IPassportRepository _passportRepository;
        private static IQualityControlSettingsRepository _qualityControlSettingsRepository;
        private static IFunctionControlRepository _funcationControlRepository;
        private static ICementTestInheritResultRepository _cementTestInheritResultRepository;
        private static ILimeTestInheritResultRepository _limeTestRepository;
        private static ISludgeTestInheritResultRepository _sludgeTestInheritResultRepository;
        private static ISlimeTestInheritResultRepository _slimeTestInheritResultRepository;
        private static IMaterialRepository _materialRepository;

        #endregion

        public static ISampleRepository GetSampleRepository()
        {
            return _sampleRepository = _sampleRepository ??  Get<ISampleRepository>();
        }

        public static ISampleBottleRepository GetSampleBottleRepository()
        {
            return _sampleBottleRepository = _sampleBottleRepository ?? Get<ISampleBottleRepository>();
        }

        public static IEmployeeRepository GetEmployeeRepository()
        {
            return _employeeRepository = _employeeRepository ?? Get<IEmployeeRepository>();
        }

        public static ILotRepository GetLotRepository()
        {
            return _lotRepository =_lotRepository ?? Get<ILotRepository>();
        }

        public static IBrandRepository GetBrandRepository()
        {
            return _brandRepository = _brandRepository ?? Get<IBrandRepository>();
        }

        public static INavigationManager GetNavigationManager()
        {
            return ServiceLocator.Current.GetInstance<INavigationManager>();
        }

        public static ITestMethodRepository GetTestMethodRepository()
        {
            return _testMethodRepository = _testMethodRepository ?? Get<ITestMethodRepository>();
        }

        public static IProductCharacterRepository GetProductCharacterRepository()
        {
            return _productCharacterRepository = _productCharacterRepository ?? Get<IProductCharacterRepository>();
        }

        public static IProductTypeRepository GetProductTypeRepository()
        {
            return _productTypeRepository = _productTypeRepository ?? Get<IProductTypeRepository>();
        }

        public static INomenclatureRepository GetNomenclatureRepository()
        {
            return _nomenclatureRepository = _nomenclatureRepository ?? Get<INomenclatureRepository>();
        }

        public static IProductTestRepository GetProductTestRepository()
        {
            return _productTestRepository = _productTestRepository ?? Get<IProductTestRepository>();
        }

        public static IStatisticRepository GetStatisticRepository()
        {
            return _statisticRepository = _statisticRepository ?? Get<IStatisticRepository>();
        }

        public static IPassportRepository GetPassportRepository()
        {
            return _passportRepository = _passportRepository ?? Get<IPassportRepository>();
        }

        public static IQualityControlSettingsRepository GetQualityControlSettingsRepository()
        {
            return
                _qualityControlSettingsRepository =
                    _qualityControlSettingsRepository ?? Get<IQualityControlSettingsRepository>();
        }

        public static IFunctionControlRepository GetFunctionControlRepository()
        {
            return _funcationControlRepository = _funcationControlRepository ?? Get<IFunctionControlRepository>();
        }

        public static ICementTestInheritResultRepository GetCementTestInheritResultRepository()
        {
            return _cementTestInheritResultRepository = _cementTestInheritResultRepository ?? Get<ICementTestInheritResultRepository>();
        }
        
        public static ILimeTestInheritResultRepository GetLimeTestInheritResultRepository()
        {
            return _limeTestRepository = _limeTestRepository ?? Get<ILimeTestInheritResultRepository>();
        }

        public static ISludgeTestInheritResultRepository GetSludgeTestInheritResultRepository()
        {
            return _sludgeTestInheritResultRepository = _sludgeTestInheritResultRepository ?? Get<ISludgeTestInheritResultRepository>();
        }
        public static ISlimeTestInheritResultRepository GetSlimeTestInheritResultRepository()
        {
            return _slimeTestInheritResultRepository = _slimeTestInheritResultRepository ?? Get<ISlimeTestInheritResultRepository>();
        }

        public static IMaterialRepository GetMaterialRepository()
        {
            return _materialRepository = _materialRepository ?? Get<IMaterialRepository>();
        }

        public static T Get<T>()
        {
            return ServiceLocator.Current.GetInstance<T>();
        }
    }
}
