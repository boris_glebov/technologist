﻿using System;
using Hqub.Technologist.Desktop.Modules;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Technologist.Desktop
{
    public static class ManagerModule
    {
        public static BaseNavigationModule LoadModule(Type module)
        {
            var m = (BaseNavigationModule)ServiceLocator.Current.GetInstance(module);
            m.Show();

            return m;
        }
    }
}
