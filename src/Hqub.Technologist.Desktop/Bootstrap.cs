﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Bootstrap.cs" company="HQUB">
//   Glebov Boris
// </copyright>
// <summary>
//   Defines the Bootstrap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Windows;
using AutoMapper;
using Hqub.Technologist.Desktop.Events;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Desktop.Modules;
using Hqub.Technologist.Desktop.Navigation;
using Hqub.Technologist.Desktop.Views;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Implementation;
using Hqub.Technologist.Store.Repository.Interfaces;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;

namespace Hqub.Technologist.Desktop
{
    public class Bootstrap : UnityBootstrapper
    {
        /// <summary>
        /// The create shell.
        /// </summary>
        /// <returns>
        /// The <see cref="DependencyObject"/>.
        /// </returns>
        protected override DependencyObject CreateShell()
        {
            //Устанавливаем дефолтную тему:
            StyleManager.ApplicationTheme = new SummerTheme();

            var main = Container.Resolve<MainWindowView>();

            main.Show();

            return main;
        }

        /// <summary>
        /// The configure module catalog.
        /// </summary>
        protected override void ConfigureModuleCatalog()
        {
            ModuleCatalog.AddModule(new ModuleInfo
            {
                ModuleName = "ShellModule",
                ModuleType =
                    typeof (ShellModule)
                        .AssemblyQualifiedName,
                InitializationMode =
                    InitializationMode.OnDemand
            });
        }

        /// <summary>
        /// The configure container.
        /// </summary>
        protected override void ConfigureContainer()
        {
            Container.RegisterInstance(Container);

            // Repository
            ConfigureRepository();

            // Mapping
            ConfigureMapper();

            // Other
            ConfigureOtherTypes();

            base.ConfigureContainer();
        }

        /// <summary>
        /// The configure other types.
        /// </summary>
        private void ConfigureOtherTypes()
        {
            Container.RegisterInstance(typeof (IEventAggregator), AggregationEventService.Instance);
            Container.RegisterInstance(typeof (INavigationManager), new NavigationManagerImpl());
        }

        private void ConfigureMapper()
        {
            // Mapping Lot <-> LotModel
            SimpleMap<Lot, LotModel>();

            // Mapping Brand <-> BrandModel
            SimpleMap<Brand, BrandModel>();

            // Mapping Position <-> PositionModel
            SimpleMap<Position, PositionModel>();

            // Mapping Material <-> MaterialModel
            SimpleMap<Material, MaterialModel>();

            // Mapping Employee <-> EmployeeModel
            SimpleMap<Employee, EmployeeModel>();

            // Mapping TestMethod <-> TestMethodModel
            SimpleMap<TestMethod, TestMethodModel>();

            // Mapping QualitySettings <-> QualitySettingsModel
            SimpleMap<QualitySetting, QualitySettingModel>();

            // Mapping ProductTest <-> ProductTestModel
            SimpleMap<ProductTest, ProductTestModel>();

            // Mapping Sample <-> SampleModel
            SimpleMap<Sample, SampleModel>();

            // Mapping SampleBottle <-> SampleBottleModel
            SimpleMap<SampleBottle, SampleBottleModel>();

            // Mapping Statistic <-> StatisticModel
            SimpleMap<Statistic, StatisticModel>();

            // Mapping ProductCharacter <-> ProductCharacterModel
            SimpleMap<ProductCharacter, ProductCharacterModel>();

            // Mapping Nomenclature <-> NomenclatureModel
            SimpleMap<Nomenclature, NomenclatureModel>();

            // Mapping ProductType <-> ProductTypeModel
            SimpleMap<ProductType, ProductTypeModel>();

            // Mapping Passport <-> PassportModel
            SimpleMap<Passport, PassportModel>();

            // Mapping FunctionalControl <-> FunctionalControlModel
            SimpleMap<FunctionalControl, FunctionalControlModel>();

            SimpleMap<CementTestInheritResult, CementTestInheritResultModel>();
            SimpleMap<SludgeTestInheritResult, SludgeTestInheritResultModel>();
            SimpleMap<SlimeTestInheritResult, SlimeTestInheritResultModel>();
            SimpleMap<LimeTestInheritResult, LimeTestInheritResultModel>();
        }

        private void SimpleMap<TEntity, TDto>()
        {
            Mapper.CreateMap<TEntity, TDto>();
            Mapper.CreateMap<TDto, TEntity>();
        }

        private void ConfigureRepository()
        {
            Container
                .RegisterType
                <IBrandRepository, BrandRepository>();

            Container
                .RegisterType
                <ICompanyRepository, CompanyRepository>();

            Container
                .RegisterType
                <IEmployeeRepository, EmployeeRepository>();

            Container
                .RegisterType<ILotRepository, LotRepository>
                ();

            Container
                .RegisterType
                <IMaterialRepository, MaterialRepository>();

            Container
                .RegisterType
                <INomenclatureRepository,
                    NomenclatureRepository>();

            Container
                .RegisterType
                <IPositionRepository, PositionRepository>();

            Container
                .RegisterType
                <IProductTestRepository,
                    ProductTestRepository>();

            Container
                .RegisterType
                <ISampleBottleRepository,
                    SampleBottleRepository>();

            Container
                .RegisterType
                <ISampleRepository, SampleRepository>();

            Container
                .RegisterType
                <ITestMethodRepository, TestMethodRepository
                    >();

            Container
                .RegisterType
                <IQualityControlSettingsRepository, QualityControlSettingsRepository>();

            Container.RegisterType<IStatisticRepository,
                StatisticRepository>();

            Container.RegisterType<IProductCharacterRepository, ProductCharacterRepository>();

            Container.RegisterType<IProductTypeRepository, ProductTypeRepository>();

            Container
                .RegisterType
                <IPassportRepository, PassportRepository>();

            Container.RegisterType<ICementTestInheritResultRepository, CementTestInheritResultRepository>();
            Container.RegisterType<ISludgeTestInheritResultRepository, SludgeTestInheritResultRepository>();
            Container.RegisterType<ISlimeTestInheritResultRepository, SlimeTestInheritResultRepository>();
            Container.RegisterType<IFunctionControlRepository, FunctionControlRepository>();

            Container.RegisterType<ILimeTestInheritResultRepository, LimeTestInheritResultRepository>();
        }
    }
}