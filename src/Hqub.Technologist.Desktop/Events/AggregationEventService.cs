﻿using Microsoft.Practices.Prism.PubSubEvents;

namespace Hqub.Technologist.Desktop.Events
{
    public class AggregationEventService : EventAggregator
    {
        private static AggregationEventService _service;
        public static AggregationEventService Instance
        {
            get { return _service = _service ?? new AggregationEventService(); }
        }

        private AggregationEventService()
        {
            
        }
    }
}
