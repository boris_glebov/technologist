﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.PubSubEvents;

namespace Hqub.Technologist.Desktop.Events.Navigation
{
    public class NavigationStackChange : PubSubEvent<Model.Events.NavigationStackState>
    {

    }
}
