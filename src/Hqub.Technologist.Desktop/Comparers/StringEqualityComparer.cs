﻿using System.Collections.Generic;

namespace Hqub.Technologist.Desktop.Comparers
{
    public class StringEqualityComparer: IEqualityComparer<string>
    {
        public bool Equals(string x, string y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(string obj)
        {
            return obj.GetHashCode();
        }
    }
}
