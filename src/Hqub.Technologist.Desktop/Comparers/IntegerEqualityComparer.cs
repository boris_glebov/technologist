﻿using System.Collections.Generic;

namespace Hqub.Technologist.Desktop.Comparers
{
    public class IntegerEqualityComparer : EqualityComparer<int>
    {
        public override bool Equals(int x, int y)
        {
            return x.Equals(y);
        }

        public override int GetHashCode(int obj)
        {
           return obj.GetHashCode();
        }
    }
}
