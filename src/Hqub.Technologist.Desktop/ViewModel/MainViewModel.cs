﻿using System.Threading;
using System.Windows;
using Hqub.Technologist.Desktop.Modules;

namespace Hqub.Technologist.Desktop.ViewModel
{
    public class MainViewModel : BindableViewModel
    {
        public override void Refresh(bool force = false)
        {
            throw new System.NotImplementedException();
        }

        protected override void LoadCommandExecute(RoutedEventArgs args)
        {
            ManagerModule.LoadModule(typeof(ShellModule));
            Utitlities.StatisticHelper.UpdateAsync();
        }
    }
}
