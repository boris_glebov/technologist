﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using AutoMapper;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model;
using Hqub.Technologist.Store.Repository;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.ViewModel
{
    public abstract class BindableViewModel : Microsoft.Practices.Prism.Mvvm.BindableBase
    {
        public abstract void Refresh(bool force = false);

        public virtual void Unload()
        {

        }

        #region LoadCommand

        public ICommand LoadCommand
        {
            get { return new DelegateCommand<RoutedEventArgs>(LoadCommandExecute); }
        }

        protected virtual void LoadCommandExecute(RoutedEventArgs args)
        {

        }

        #endregion

        #region ClosingCommands

        public ICommand ClosingCommand
        {
            get { return new DelegateCommand<CancelEventArgs>(ClosingCommandExecute); }
        }

        protected virtual void ClosingCommandExecute(CancelEventArgs args)
        {

        }

        #endregion
    }

    public abstract class BaseViewModel<T> : BindableViewModel
        where T : EntityBase 
    {
        private bool _isBusy;
        private T _selectedItem;
        private ObservableCollection<T> _items;

        protected BaseViewModel()
        {
            PreInit();

            RefreshAsync();
        }

        protected virtual void PreInit()
        {
            
        }

        #region Common Commands

        #region DoubleClickCommand



        public ICommand DoubleClickCommand
        {
            get
            {
                return new DelegateCommand(DoubleClickCommandExecute);
            }
        }

        private void DoubleClickCommandExecute()
        {
            if (SelectedItem == null)
                return;

            var model = GetModelById(((IHasId) SelectedItem).Id);

            var dialog = GetDialog();
            dialog.SetModel(model);

            Open(dialog);
        }

        #endregion

        #region RemoveCommand

        public ICommand RemoveCommand { get { return new DelegateCommand(RemoveCommandExecute); } }

        private void RemoveCommandExecute()
        {
            Remove(SelectedItem);
        }

        #endregion

        #region OpenEditDialog

        public ICommand OpenEditDialogCommand
        {
            get { return new DelegateCommand(OpenEditDialogCommandExecute); }
        }

        private void OpenEditDialogCommandExecute()
        {
            var dialog = GetDialog();
            dialog.SetModel(null);

            Open(dialog);
        }

        #endregion

        #region RefreshCommand

        public ICommand ReloadCommand { get { return new DelegateCommand(ReloadCommandExecute); } }

        private void ReloadCommandExecute()
        {
            RefreshAsync();
        }

        #endregion

        #endregion

        #region Properties

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged(() => IsBusy);
            }
        }

        public T SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged(() => SelectedItem);
            }
        }

        public ObservableCollection<T> Items
        {
            get { return _items; }
            set
            {
                _items = value;
                OnPropertyChanged(() => Items);
            }
        }

        #endregion

        #region Methods

        protected void RefreshAsync()
        {
            IsBusy = true;

            ThreadPool.QueueUserWorkItem(delegate
            {
                Refresh();
                AfterRefresh();

                IsBusy = false;
            });
        }

        protected virtual void AfterRefresh()
        {
            
        }

        /// <summary>
        /// Получить из репозитария модель по ID
        /// </summary>
        /// <param name="id">ID сущности</param>
        /// <returns></returns>
        public abstract object GetEntity(Guid id);

        public abstract IRepositoryCommon GetRepository();

        public T GetModelById(Guid id)
        {
            return Mapper.Map<T>(GetEntity(id));
        }

        /// <summary>
        /// Открывает диалог для редактирования
        /// </summary>
        /// <param name="dialog"></param>
        public void Open(BaseEditDialog dialog)
        {
            var result = dialog.ShowDialog();

            if (result == true)
            {
                Refresh();
            }
        }

        /// <summary>
        /// Общий метод удаления элемента
        /// </summary>
        /// <param name="selectedItem"></param>
        public void Remove(IHasId selectedItem)
        {
            if (selectedItem == null)
                return;

            if (
                MessageBox.Show("Вы действительно хотите удалить запись?", "Удаление должности",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                return;

            try
            {
                var repository = GetRepository();
                repository.Delete(selectedItem.Id);

                Refresh();
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(
                    string.Format("PositionSettingsViewModel.RemoveCommandExecute({0})", selectedItem.Id), exception);

                MessageBox.Show("Ошибка удаления. Возможно от этой записи зависят другие справочники.");
            }
        }

        /// <summary>
        /// Включает отображение процесса
        /// </summary>
        public void Busy()
        {
            IsBusy = true;
        }

        /// <summary>
        /// Выключает отображение процесса
        /// </summary>
        public void UnBusy()
        {
            IsBusy = false;
        }


        public abstract BaseEditDialog GetDialog();

        #endregion

    }
}
