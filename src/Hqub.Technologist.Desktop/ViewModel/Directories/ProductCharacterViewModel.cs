﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.ViewModel.Directories
{
    public class ProductCharacterViewModel : BaseViewModel<ProductCharacterModel>
    {
        private IProductCharacterRepository _productCharacterRepository;

        protected override void PreInit()
        {
            base.PreInit();

            _productCharacterRepository = Locator.GetProductCharacterRepository();
        }

        public override void Refresh(bool force = false)
        {
            Items =
                new ObservableCollection<ProductCharacterModel>(
                    AutoMapper.Mapper.Map<List<ProductCharacterModel>>(_productCharacterRepository.List()));
        }

        #region Implementation BaseViewModel

        public override object GetEntity(Guid id)
        {
            return _productCharacterRepository.Get(id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _productCharacterRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            var dialog = new Dialogs.Directories.ProductCharacterDialog();
            return dialog;
        }

        #endregion
    }
}
