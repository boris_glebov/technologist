﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.ViewModel.Directories
{
    public class EmployeeViewModel : BaseViewModel<EmployeeModel>
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeViewModel(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public override object GetEntity(Guid id)
        {
            return _employeeRepository.Get(id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _employeeRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            var dialog = new Dialogs.Directories.EmployeeEntityDialog();
            return dialog;
        }

        public override void Refresh(bool force = false)
        {
            Items =
                new ObservableCollection<EmployeeModel>(
                    AutoMapper.Mapper.Map<List<EmployeeModel>>(_employeeRepository.List()));
        }
    }
}
