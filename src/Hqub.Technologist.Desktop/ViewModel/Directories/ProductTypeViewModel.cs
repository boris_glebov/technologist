﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.ViewModel.Directories
{
  public  class ProductTypeViewModel : BaseViewModel<ProductTypeModel>
  {
      private readonly IProductTypeRepository _productTypeRepository;

      public ProductTypeViewModel(IProductTypeRepository productTypeRepository)
      {
          _productTypeRepository = productTypeRepository;
      }

      public override void Refresh(bool force = false)
      {
          var productType = _productTypeRepository.List();
          var items = AutoMapper.Mapper.Map<List<ProductTypeModel>>(productType);
          Items = new ObservableCollection<ProductTypeModel>(items);
      }

      public override object GetEntity(Guid id)
      {
         return _productTypeRepository.Get(id);
      }

      public override IRepositoryCommon GetRepository()
      {
          return _productTypeRepository;
      }

      public override BaseEditDialog GetDialog()
      {
          var dialog = new Dialogs.Directories.ProductTypeDialog();
          return dialog;
      }
  }
}
