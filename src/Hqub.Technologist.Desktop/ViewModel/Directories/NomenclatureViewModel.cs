﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.ViewModel.Directories
{
    public class NomenclatureViewModel : BaseViewModel<NomenclatureModel>
    {
        private INomenclatureRepository _nomenclatureRepository;

        protected override void PreInit()
        {
            base.PreInit();

            _nomenclatureRepository = Locator.GetNomenclatureRepository();
        }

        public override void Refresh(bool force = false)
        {
            var nomenclatures = _nomenclatureRepository.List();
            var items = AutoMapper.Mapper.Map<List<NomenclatureModel>>(nomenclatures);
            Items = new ObservableCollection<NomenclatureModel>(items);
        }

        public override object GetEntity(Guid id)
        {
            return _nomenclatureRepository.Get(id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _nomenclatureRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            var dialog = new Dialogs.Directories.NomenclatureDialog();
            return dialog;
        }
    }
}
