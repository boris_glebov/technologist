﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.ViewModel.Materials
{
    /// <summary>
    /// Испытание извести
    /// </summary>
    public class TestLimeViewModel : BaseViewModel<LimeTestInheritResultModel>
    {
        private ILimeTestInheritResultRepository _limeTestInheritResultRepository;

        protected override void PreInit()
        {
            _limeTestInheritResultRepository = Locator.GetLimeTestInheritResultRepository();
        }

        public override void Refresh(bool force = false)
        {
            var limeTestResults = _limeTestInheritResultRepository.List();
            var mapping = AutoMapper.Mapper.Map<List<LimeTestInheritResultModel>>(limeTestResults);
            Items = new ObservableCollection<LimeTestInheritResultModel>(mapping);
        }

        public ObservableCollection<Tuple<int, float>> TestLiming
        {
            get
            {
                return new ObservableCollection<Tuple<int, float>>
                {
                    new Tuple<int, float>(10, 4.0f),
                    new Tuple<int, float>(20, 5.0f),
                    new Tuple<int, float>(30, 6.0f),
                    new Tuple<int, float>(40, 7.0f),
                    new Tuple<int, float>(50, 8.5f),
                    new Tuple<int, float>(60, 9.0f),
                    new Tuple<int, float>(80, 4.0f),
                    new Tuple<int, float>(100, 3.0f),
                };
            }
        }
        public ObservableCollection<Tuple<int, float>> TestLiming1
        {
            get
            {
                return new ObservableCollection<Tuple<int, float>>
                {
                    new Tuple<int, float>(0, 1.0f),
                    new Tuple<int, float>(10, 2.0f),
                    new Tuple<int, float>(20, 3.0f),
                    new Tuple<int, float>(30, 4.0f),
                    new Tuple<int, float>(40, 5.5f),
                    new Tuple<int, float>(50, 6.0f),
                    new Tuple<int, float>(60, 7.0f),
                    new Tuple<int, float>(70, 8.0f),
                };
            }
        }

        public override object GetEntity(Guid id)
        {
            return _limeTestInheritResultRepository.Get(id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _limeTestInheritResultRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            var dialog = new Dialogs.Materials.DataInputLime();
            return dialog;
        }
    }
}
