﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Dialogs.Manufacture;
using Hqub.Technologist.Desktop.Dialogs.Materials;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;
using Telerik.Licensing;

namespace Hqub.Technologist.Desktop.ViewModel.Materials
{
    /// <summary>
    /// Испытание цемента
    /// </summary>
    public class TestCementViewModel : BaseViewModel<CementTestInheritResultModel>
    {
        private ICementTestInheritResultRepository _iCementTestInheritResultRepository;
        private List<CementTestInheritResultModel> _cementTestInheritResult;
        


        #region Ovveride BaseViewModel

        public override object GetEntity(Guid id)
        {
            return _iCementTestInheritResultRepository.Get(id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _iCementTestInheritResultRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            return new DataInputCement();
        }

        protected override void PreInit()
        {
            _iCementTestInheritResultRepository = Locator.GetCementTestInheritResultRepository();
        }

        public override void Refresh(bool force = false)
        {
            var cements = _iCementTestInheritResultRepository.List();
            var mappingCements = AutoMapper.Mapper.Map<List<CementTestInheritResultModel>>(cements);
           
            Items = new ObservableCollection<CementTestInheritResultModel>(mappingCements);
        }

        #endregion

        #region Properties

        public List<CementTestInheritResultModel> CementTestInheritResult
        {
            get { return _cementTestInheritResult; }
            set
            {
                _cementTestInheritResult = value;
                OnPropertyChanged(() => CementTestInheritResult);
            }
        }

        

        #endregion
    }
}
