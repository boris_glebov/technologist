﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Dialogs.Manufacture;
using Hqub.Technologist.Desktop.Dialogs.Materials;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.ViewModel.Materials
{
    /// <summary>
    /// Испытание шлама
    /// </summary>
    public class TestSlimeViewModel : BaseViewModel<SlimeTestInheritResultModel>
    {
        private ISlimeTestInheritResultRepository _iSlimeTestInheritResultRepository;
        private List<SlimeTestInheritResultModel> _slimeTestInheritResult;

        #region Ovveride BaseViewModel

        public override object GetEntity(Guid id)
        {
            return _iSlimeTestInheritResultRepository.Get(id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _iSlimeTestInheritResultRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            return new DataInputSlime();
        }

        protected override void PreInit()
        {
            _iSlimeTestInheritResultRepository = Locator.GetSlimeTestInheritResultRepository();
        }

        public override void Refresh(bool force = false)
        {
            var slimes= _iSlimeTestInheritResultRepository.List();
            var mappingSlimes = AutoMapper.Mapper.Map<List<SlimeTestInheritResultModel>>(slimes);

            Items = new ObservableCollection<SlimeTestInheritResultModel>(mappingSlimes);
        }

        #endregion

        #region Properties

        public List<SlimeTestInheritResultModel> CSlimeTestInheritResult
        {
            get { return _slimeTestInheritResult; }
            set
            {
                _slimeTestInheritResult = value;
                OnPropertyChanged(() => CSlimeTestInheritResult);
            }
        }



        #endregion
    }
}
