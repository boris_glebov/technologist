﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Dialogs.Manufacture;
using Hqub.Technologist.Desktop.Dialogs.Materials;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;
using Telerik.Licensing;

namespace Hqub.Technologist.Desktop.ViewModel.Materials
{   /// <summary>
    /// Испытание SIO
    /// </summary>
    public class TestSludgeViewModel : BaseViewModel<SludgeTestInheritResultModel>
    {
        private ISludgeTestInheritResultRepository _iSludgeTestInheritResultRepository;
        private List<SludgeTestInheritResultModel> _sludgeTestInheritResult;

        #region Ovveride BaseViewModel

        public override object GetEntity(Guid id)
        {
            return _iSludgeTestInheritResultRepository.Get(id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _iSludgeTestInheritResultRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            return new DataInputSludge();
        }

        protected override void PreInit()
        {
            _iSludgeTestInheritResultRepository = Locator.GetSludgeTestInheritResultRepository();
        }

        public override void Refresh(bool force = false)
        {
            var sludges = _iSludgeTestInheritResultRepository.List();
            var mappingSludges = AutoMapper.Mapper.Map<List<SludgeTestInheritResultModel>>(sludges);

            Items = new ObservableCollection<SludgeTestInheritResultModel>(mappingSludges);
        }

        #endregion
        #region Properties

        public List<SludgeTestInheritResultModel> CementTestInheritResult
        {
            get { return _sludgeTestInheritResult; }
            set
            {
                _sludgeTestInheritResult = value;
                OnPropertyChanged(() => CementTestInheritResult);
            }
        }



        #endregion
    }
}
