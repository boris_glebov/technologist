﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Desktop.Navigation;
using Hqub.Technologist.Store.Repository;
using Telerik.Windows.Controls;

namespace Hqub.Technologist.Desktop.ViewModel.Manufacture
{
    public class FunctionalControlViewModel : BaseViewModel<FunctionalControlModel>
    {
        private List<EmployeeModel> _employee;

        public FunctionalControlViewModel()
        {
            Items = new ObservableCollection<FunctionalControlModel>();
        }

        public override void Refresh(bool force = false)
        {
            var repository = Locator.GetFunctionControlRepository();
            var entities = repository.List();

            Items =
                new ObservableCollection<FunctionalControlModel>(
                    AutoMapper.Mapper.Map<List<FunctionalControlModel>>(entities));

            Employee = Cache.EmployeeCache.List(true);
        }

        #region Commands

        public ICommand AddItemCommand
        {
            get { return new DelegateCommand(AddItemCommandExecute); }
        }

        private void AddItemCommandExecute(object obj)
        {
            var item = FunctionalControlModel.Create();
            Items.Add(item);
        }

        public ICommand SaveCommand { get { return new DelegateCommand(SaveCommandExecute);} }

        private void SaveCommandExecute(object obj)
        {
            foreach (var item in Items)
            {
                item.Save();
            }
        }

        public ICommand OpenSettingsDialogCommand
        {
            get { return new DelegateCommand(OpenSettingsDialogCommandExecute); }
        }

        private void OpenSettingsDialogCommandExecute(object obj)
        {
            NavigationHelper.NavigationManager.SetLastModuleName("Параметры процесса");
            NavigationHelper.GoToQualitySettings();
        }

        #endregion

        #region Properties

        public List<EmployeeModel> Employee
        {
            get { return _employee; }
            set
            {
                _employee = value;
                OnPropertyChanged(() => Employee);
            }
        }

        #endregion

        #region Not Implemented

        public override object GetEntity(Guid id)
        {
            throw new NotImplementedException();
        }

        public override IRepositoryCommon GetRepository()
        {
            throw new NotImplementedException();
        }

        public override BaseEditDialog GetDialog()
        {
            return null;
        }

        #endregion
    }
}
