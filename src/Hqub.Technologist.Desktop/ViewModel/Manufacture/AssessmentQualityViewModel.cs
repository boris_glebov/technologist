﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Dialogs.Manufacture;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;
using Telerik.Licensing;

namespace Hqub.Technologist.Desktop.ViewModel.Manufacture
{
    public class AssessmentQualityViewModel : BaseViewModel<LotModel>
    {
        private ILotRepository _lotRepository;
        private List<BrandModel> _brands;

        #region Ovveride BaseViewModel

        public override object GetEntity(Guid id)
        {
            return _lotRepository.Get(id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _lotRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            return new AssessmentQualityEntityDialog();
        }

        protected override void PreInit()
        {
            _lotRepository = Locator.GetLotRepository();
        }

        public override void Refresh(bool force = false)
        {
            var lots = _lotRepository.List();
            var mappingLots = AutoMapper.Mapper.Map<List<LotModel>>(lots);
            Items = new ObservableCollection<LotModel>(mappingLots);
        }

        #endregion

        #region Properties

        public List<BrandModel> Brands
        {
            get { return _brands; }
            set
            {
                _brands = value;
                OnPropertyChanged(() => Brands);
            }
        }

        #endregion
    }
}
