﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using AutoMapper;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Telerik.Windows.Controls;

namespace Hqub.Technologist.Desktop.ViewModel.Product
{
    public class PassportProductTestViewModel : BaseViewModel<PassportModel>
    {
        private  IPassportRepository _passportRepository;
        private  IProductTestRepository _productTestRepository;
        private  IProductCharacterRepository _productCharacterRepository;
        private  INomenclatureRepository _nomenclatureRepository;
        private bool _isEnabled;
        private List<ProductTestModel> _products;
        private ProductTestModel _selectedProduct;
        private PassportModel _model;
        private List<NomenclatureModel> _nomenclatures;
        private NomenclatureModel _selectedNomenclature;
        private string _nomenclatureTextSearch;

        protected override void PreInit()
        {
            base.PreInit();

            _passportRepository = Locator.GetPassportRepository();
            _productTestRepository = Locator.GetProductTestRepository();
            _productCharacterRepository = Locator.GetProductCharacterRepository();
            _nomenclatureRepository = Locator.GetNomenclatureRepository();
        }

        public override void Refresh(bool force = false)
        {
            var products = _productTestRepository.List();
            Products = new List<ProductTestModel>(Mapper.Map<List<ProductTestModel>>(products));

            var nomenclatures = _nomenclatureRepository.List();
            Nomenclatures = new List<NomenclatureModel>(Mapper.Map<List<NomenclatureModel>>(nomenclatures));
        }

        private void LoadPassport(ProductTestModel product)
        {
            if (product == null)
                return;

            var passport = _passportRepository.FirstOrDefault(p => p.LotNumber == product.SelectedLot.LotNumber);
            if (passport != null)
            {
                Model = Mapper.Map<PassportModel>(passport);
//                Model.DateRelease = DateTime.Now;
                return;
            }
          

            Model = new PassportModel
            {
                LotNumber = product.SelectedLot.LotNumber,
                MarkName = product.MarkNameRequired,
                ClassStrenghtName = product.ClassNameRequired,
                Wetness = product.Wetness,
                StrenghtLimit = product.StrenghtLimitAverage,
                RequiredStrenght = product.StrenghtRequired,
                AverageDensity = product.WetDensityAverage,
                VariationStrenght = product.StrenghtVariation,
                DateRelease = product.TestDate,
                NomenclatureId = product.SelectedLot.NomenclatureId
                
            };

            SelectedNomenclature = Nomenclatures.FirstOrDefault(n => n.Id == Model.NomenclatureId);

            var productCharacter =
                Mapper.Map<ProductCharacterModel>(_productCharacterRepository.GetByMark(product.MarkDensity));

            if (productCharacter == null) return;
            
            Model.DryingShrinkage = productCharacter.DryingShrinkage;
            Model.ThermalConductivity = productCharacter.ThermalConductivity;
            Model.SpecificActivity = productCharacter.SpecificActivity;
            Model.FrostResist = productCharacter.FrostResist;
            Model.Notes = productCharacter.Notes;
            Model.WaterVaporPermeability = productCharacter.WaterVaporPermeability;
        }

        #region Commands

        public ICommand SaveCommand
        {
            get
            {
                return new DelegateCommand(SaveCommandExecute);
            }
        }

        private void SaveCommandExecute(object obj)
        {
            IsBusy = true;
            ThreadPool.QueueUserWorkItem(delegate
            {
                if (!Model.Save())
                {
                    MessageBox.Show("Не удалось сохранить паспорт. Обратитетесь к администратору.");
                }
                IsBusy = false;
            });
        }

        public ICommand PrintCommand
        {
            get
            {
                return new DelegateCommand(PrintCommandExecute);
            }
        }

        private void PrintCommandExecute(object obj)
        {

            //создаем новый объект класса документ 
            Document doc = new Document();
            var memory = new MemoryStream();
            PdfWriter writer;

            try
            {
                //указываем общий путь где будут валяться все pdf
                string fontPath = AppDomain.CurrentDomain.BaseDirectory + "Fonts\\arial.ttf"; 

                //настраиваем шрифт
                BaseFont baseFont = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

                //тут как бы список переменныx используемых затем в pdf на самом деле они по сути не нужны, потому что ниже при добавлении фраз можно сразу вставить эти строки, без оюъявления заранее
                string jurname = "ООО ПСО ТЕПЛИТ";

                string addr = "623704 Свердловская обл., г. Берёзовский, ул. Чапаева, 39/4";

                string tel = "Тел/факс (343)266-29-14(15, 55)";

                string emeil = "info@teplit.ru, www.teplit.ru";

                string perem = jurname + "\n" + addr + "\n" + tel + "\n" + emeil;

                string passport = "ПАСПОРТ №" + Model.LotNumber;

                string name = "Наименование: " + "Изделия стеновые неармированные из ячеистого бетона автоклавного твердения ГОСТ 31360-2007";

                string appointment = "Назначение: " + "конструкционно-теплоизоляционные";

                string productType = "Вид продукции:" + Model.NomenclatureEntity.ProductName;

                string lotNumber = "Номер партии:" + Model.LotNumber;
                string productionAmount = "Объём продукции, м3:" + "";

                string releaseDate = "Дата выпуска:" + String.Format("{0:dd.MM.yyyy}", Model.DateRelease);

                string shippingDate = "Дата отгрузки:" + "";

                string symbol = "Условное обозначение:" + "Блок I/625 х 200 х 250/D400/B2.5/F100 ГОСТ 31360-2007";


                //начинаем заполнять всякие поля
                Phrase jurPhrase = new Phrase(perem,
                    new Font(baseFont, 12f, Font.NORMAL));


                //указываем картинку, в нашем случае будет логотип
                var gif = Image.GetInstance(AppDomain.CurrentDomain.BaseDirectory + "Content\\logo.jpg");
                writer = PdfWriter.GetInstance(doc, memory);

                //открываем документ для добавления туда всякой лабуды
                doc.Open();

                //создаем таблицу
                PdfPTable title = new PdfPTable(2);

                //указываем что колонки будут в соотношении 1/4 и 3/4
                float[] widths = new float[] { 1f, 3f };
                title.SetWidths(widths);

                //добавляем колонку с картинкой
                title.AddCell(gif);

                //создаем колонку с последующим форматированием
                PdfPCell cell = new PdfPCell(new Phrase(jurPhrase));
                cell.HorizontalAlignment = 1;
                title.AddCell(cell);
                title.SpacingAfter = 18f;

                //создаем заголовок паспорт

                iTextSharp.text.Paragraph heading = new iTextSharp.text.Paragraph(passport,
                    new Font(baseFont, 12f, Font.BOLD));
                heading.SpacingAfter = 2f;
                heading.Alignment = Element.PARAGRAPH;

                heading.Alignment = 1;

                //создаем поля заголовка
                iTextSharp.text.Paragraph nameI = new iTextSharp.text.Paragraph(name,
                    new Font(baseFont, 12f, Font.NORMAL));
                nameI.SpacingAfter = 1f;
                nameI.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.Paragraph appointmentI = new iTextSharp.text.Paragraph(appointment,
                   new Font(baseFont, 12f, Font.NORMAL));
                appointmentI.SpacingAfter = 1f;
                appointmentI.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.Paragraph productTypeI = new iTextSharp.text.Paragraph(productType,
                    new Font(baseFont, 12f, Font.NORMAL));
                productTypeI.SpacingAfter = 1f;
                productTypeI.Alignment = Element.ALIGN_LEFT;


                iTextSharp.text.Paragraph lotNumberI = new iTextSharp.text.Paragraph(lotNumber + " " + productionAmount,
                    new Font(baseFont, 12f, Font.NORMAL));
                lotNumberI.SpacingAfter = 1f;
                lotNumberI.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.Paragraph releaseDateI = new iTextSharp.text.Paragraph(releaseDate + " " + shippingDate,
                    new Font(baseFont, 12f, Font.NORMAL));
                releaseDateI.SpacingAfter = 1f;
                releaseDateI.Alignment = Element.ALIGN_LEFT;

                iTextSharp.text.Paragraph symbolI = new iTextSharp.text.Paragraph(symbol,
                    new Font(baseFont, 12f, Font.NORMAL));
                symbolI.SpacingAfter = 1f;
                symbolI.Alignment = Element.ALIGN_LEFT;

                //создаем таблицу
                PdfPTable mainTable = new PdfPTable(3);

                float[] widthsMain = new float[] { 1f, 1f, 1f };
                mainTable.SetWidths(widthsMain);

                mainTable.SpacingBefore = 18f;
                mainTable.HorizontalAlignment = 0;

                PdfPCell indicators = new PdfPCell(new Phrase("Показатели", new Font(baseFont, 12f, Font.NORMAL)));
                indicators.Colspan = 2;
                indicators.HorizontalAlignment = 1;

                PdfPCell testData = new PdfPCell(new Phrase("Результаты испытаний", new Font(baseFont, 12f, Font.NORMAL)));
                testData.HorizontalAlignment = 1;

                PdfPCell geometricalDimensions = new PdfPCell(new Phrase("Геометрические размеры, мм", new Font(baseFont, 12f, Font.NORMAL)));

                geometricalDimensions.HorizontalAlignment = 1;


                PdfPTable dimensions = new PdfPTable(1);
                dimensions.HorizontalAlignment = 1;

                dimensions.AddCell(new Phrase("длина", new Font(baseFont, 12f, Font.NORMAL)));
                dimensions.AddCell(new Phrase("толщина", new Font(baseFont, 12f, Font.NORMAL)));
                dimensions.AddCell(new Phrase("высота", new Font(baseFont, 12f, Font.NORMAL)));
                PdfPCell dimensionsing = new PdfPCell(dimensions);



                PdfPTable dimensionsValue = new PdfPTable(1);
                dimensionsValue.HorizontalAlignment = 1;
                dimensionsValue.AddCell(new Phrase("625", new Font(baseFont, 12f, Font.NORMAL)));
                dimensionsValue.AddCell(new Phrase("200", new Font(baseFont, 12f, Font.NORMAL)));
                dimensionsValue.AddCell(new Phrase("250", new Font(baseFont, 12f, Font.NORMAL)));
                PdfPCell dimensionsValueI = new PdfPCell(dimensionsValue);


                PdfPCell tensileStrength = new PdfPCell(new Phrase("Предел прочности при сжатии, кгс/см2", new Font(baseFont, 12f, Font.NORMAL)));
                tensileStrength.Colspan = 2;
                tensileStrength.HorizontalAlignment = 0;

                PdfPCell tensileStrengthValue = new PdfPCell(new Phrase(Model.StrenghtLimit + " ( " + Model.ClassStrenghtName + " )", new Font(baseFont, 12f, Font.NORMAL)));
                tensileStrengthValue.HorizontalAlignment = 0;



                PdfPCell requiredStrength = new PdfPCell(new Phrase("Требуемая прочность, кгс/см2", new Font(baseFont, 12f, Font.NORMAL)));
                requiredStrength.Colspan = 2;
                requiredStrength.HorizontalAlignment = 0;

                PdfPCell requiredStrengthValue = new PdfPCell(new Phrase(Model.RequiredStrenght.ToString(), new Font(baseFont, 12f, Font.NORMAL)));
                requiredStrengthValue.HorizontalAlignment = 0;

                PdfPCell averageDensity = new PdfPCell(new Phrase("Средняя плотность, кг/м3", new Font(baseFont, 12f, Font.NORMAL)));
                averageDensity.Colspan = 2;
                averageDensity.HorizontalAlignment = 0;

                PdfPCell averageDensityValue = new PdfPCell(new Phrase(Model.AverageDensity+"( "+Model.MarkName+" )", new Font(baseFont, 12f, Font.NORMAL)));
                averageDensityValue.HorizontalAlignment = 0;

                PdfPCell permeability = new PdfPCell(new Phrase("Коэффициент паропроницаемости, мг/м ч Па", new Font(baseFont, 12f, Font.NORMAL)));
                permeability.Colspan = 2;
                permeability.HorizontalAlignment = 0;

                PdfPCell permeabilityValue = new PdfPCell(new Phrase(Model.WaterVaporPermeability.ToString(), new Font(baseFont, 12f, Font.NORMAL)));
                permeabilityValue.HorizontalAlignment = 0;

                PdfPCell markFrost = new PdfPCell(new Phrase("Марка по морозостойкости", new Font(baseFont, 12f, Font.NORMAL)));
                markFrost.Colspan = 2;
                markFrost.HorizontalAlignment = 0;

                PdfPCell markFrostValue = new PdfPCell(new Phrase(Model.FrostResist, new Font(baseFont, 12f, Font.NORMAL)));
                markFrostValue.HorizontalAlignment = 0;

                PdfPCell thermalConductivity = new PdfPCell(new Phrase("Коэффициент теплопроводности в сухом состоянии, Вт/(м 0С)", new Font(baseFont, 12f, Font.NORMAL)));
                thermalConductivity.Colspan = 2;
                thermalConductivity.HorizontalAlignment = 0;

                PdfPCell thermalConductivityValue = new PdfPCell(new Phrase(Model.ThermalConductivity.ToString(), new Font(baseFont, 12f, Font.NORMAL)));
                thermalConductivityValue.HorizontalAlignment = 0;

                PdfPCell dryingShrinkage = new PdfPCell(new Phrase("Усадка при высыхании, мм/м", new Font(baseFont, 12f, Font.NORMAL)));
                dryingShrinkage.Colspan = 2;
                dryingShrinkage.HorizontalAlignment = 0;

                PdfPCell dryingShrinkageValue = new PdfPCell(new Phrase(Model.DryingShrinkage.ToString(), new Font(baseFont, 12f, Font.NORMAL)));
                dryingShrinkageValue.HorizontalAlignment = 0;

                PdfPCell specificEffectiveActivity = new PdfPCell(new Phrase("Удельная эффективная активность ЕРН, Бк/кг", new Font(baseFont, 12f, Font.NORMAL)));
                specificEffectiveActivity.Colspan = 2;
                specificEffectiveActivity.HorizontalAlignment = 0;

                PdfPCell specificEffectiveActivityValue = new PdfPCell(new Phrase(Model.SpecificActivity, new Font(baseFont, 12f, Font.NORMAL)));
                specificEffectiveActivityValue.HorizontalAlignment = 0;

                mainTable.AddCell(indicators);
                mainTable.AddCell(testData);
                mainTable.AddCell(geometricalDimensions);
                mainTable.AddCell(dimensionsing);
                mainTable.AddCell(dimensionsValueI);
                mainTable.AddCell(tensileStrength);
                mainTable.AddCell(tensileStrengthValue);

                mainTable.AddCell(requiredStrength);
                mainTable.AddCell(requiredStrengthValue);

                mainTable.AddCell(averageDensity);
                mainTable.AddCell(averageDensityValue);

                mainTable.AddCell(permeability);
                mainTable.AddCell(permeabilityValue);

                mainTable.AddCell(markFrost);
                mainTable.AddCell(markFrostValue);

                mainTable.AddCell(thermalConductivity);
                mainTable.AddCell(thermalConductivityValue);

                mainTable.AddCell(dryingShrinkage);
                mainTable.AddCell(dryingShrinkageValue);

                mainTable.AddCell(specificEffectiveActivity);
                mainTable.AddCell(specificEffectiveActivityValue);

                var fio = new iTextSharp.text.Paragraph("Должности и ФИО ответственного ИТР",
                   new Font(baseFont, 12f, Font.NORMAL));
                fio.SpacingAfter = 1f;
                fio.Alignment = Element.ALIGN_LEFT;


                //добавление всех элементов по порядку в документ
                doc.Add(title);
                doc.Add(heading);
                doc.Add(nameI);
                doc.Add(appointmentI);
                doc.Add(productTypeI);
                doc.Add(lotNumberI);
                doc.Add(releaseDateI);
                doc.Add(symbolI);
                doc.Add(mainTable);
                doc.Add(fio);

                writer.CloseStream = false;
            }
            /* catch (Exception ex)
             {
                 //log
             }*/
            finally
            {
                doc.Close();

                memory.Position = 0;
                var pdfViewer = new Dialogs.PdfPreviewDialog(memory);
                pdfViewer.Show();
            }
            
        }

        #endregion

        #region Properties

        public List<ProductTestModel> Products
        {
            get { return _products; }
            set
            {
                _products = value;
                OnPropertyChanged(() => Products);
            }
        }

        public ProductTestModel SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                _selectedProduct = value;
                IsEnabled = _selectedProduct != null;
                LoadPassport(_selectedProduct);

                OnPropertyChanged(() => SelectedProduct);
            }
        }

        public List<NomenclatureModel> Nomenclatures
        {
            get { return _nomenclatures; }
            set
            {
                _nomenclatures = value; 
                OnPropertyChanged(()=>Nomenclatures);
            }
        }

        public NomenclatureModel SelectedNomenclature
        {
            get { return _selectedNomenclature; }
            set
            {
               _selectedNomenclature = value;
               if (Model != null && value != null)
                   Model.NomenclatureId = value.Id;
                
                OnPropertyChanged(() => SelectedNomenclature);
            }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                _isEnabled = value;
                OnPropertyChanged(() => IsEnabled);
            }
        }

        public PassportModel Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged(() => Model);
            }
        }

        #endregion

        #region Methods



        #endregion

        #region Implementation PassportProductTest

        public override object GetEntity(Guid id)
        {
            throw new NotImplementedException();
        }

        public override IRepositoryCommon GetRepository()
        {
            throw new NotImplementedException();
        }

        public override BaseEditDialog GetDialog()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
