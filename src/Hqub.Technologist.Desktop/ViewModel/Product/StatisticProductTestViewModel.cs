﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.ViewModel.Product
{
    /// <summary>
    /// Статистика испытаний ГП
    /// </summary>
    public class StatisticProductTestViewModel : BaseViewModel<StatisticModel>
    {
        private IProductTestRepository _productTestRepository;
        private IStatisticRepository _statisticRepository;
        private IBrandRepository _brandRepository;
        private INomenclatureRepository _nomenclatureRepository;

        private ObservableCollection<ProductTestModel> _products;
        private ObservableCollection<NomenclatureModel> _nomenclatures;

        private List<Tuple<string, float>> _futureMarkDensity;
        private List<Tuple<string, float>> _futureClassStrenght;

        protected override void PreInit()
        {
            base.PreInit();

            _productTestRepository = Locator.GetProductTestRepository();
            _statisticRepository = Locator.GetStatisticRepository();
            _brandRepository = Locator.GetBrandRepository();
            _nomenclatureRepository = Locator.GetNomenclatureRepository();
        }

        public override void Refresh(bool force = false)
        {
            Products =
                new ObservableCollection<ProductTestModel>(
                    AutoMapper.Mapper.Map<List<ProductTestModel>>(
                        _productTestRepository.List()).OrderByDescending(x => x.SelectedLot.LotNumber).ToList());

            Nomenclatures =
                new ObservableCollection<NomenclatureModel>(
                    AutoMapper.Mapper.Map<List<NomenclatureModel>>(_nomenclatureRepository.List()));

            LoadStatistics();
        }

        private void LoadStatistics()
        {
            var nextMonth = DateTime.Now.AddMonths(1);
            var statistic = Utitlities.StatisticHelper.Get(nextMonth);

            var groupByClass = statistic.GroupBy(x => x.MarkClass);

            FutureMarkDensity = new List<Tuple<string, float>>();
            FutureClassStrenght = new List<Tuple<string, float>>();

            var brands = _brandRepository.List();
            foreach (var brand in brands)
            {
                var brand1 = brand;
                var mark = statistic.FirstOrDefault(b => b.MarkId == brand1.Id);
                FutureMarkDensity.Add(mark == null
                    ? new Tuple<string, float>(brand1.Name, 0)
                    : new Tuple<string, float>(brand1.Name, mark.DensityRequired));
            }

            foreach (var markClass in groupByClass.SelectMany(classes => classes))
            {
                FutureClassStrenght.Add(new Tuple<string, float>(markClass.MarkClass, markClass.StrengthRequired));
            }
        }

        protected override void AfterRefresh()
        {
            var refreshEvent = Events.AggregationEventService.Instance.GetEvent<Events.RefreshEvent>();
            refreshEvent.Publish(new Events.RefreshEvent());
        }

        #region Properties

        public ObservableCollection<ProductTestModel> Products
        {
            get { return _products; }
            set
            {
                _products = value;
                OnPropertyChanged(() => Products);
            }
        }

        public ObservableCollection<NomenclatureModel> Nomenclatures
        {
            get { return _nomenclatures; }
            set
            {
                _nomenclatures = value;
                OnPropertyChanged(() => Nomenclatures);
            }
        }

        public List<Tuple<string, float>> FutureMarkDensity
        {
            get { return _futureMarkDensity; }
            set
            {
                _futureMarkDensity = value;
                OnPropertyChanged(() => FutureMarkDensity);
            }
        }

        public List<Tuple<string, float>> FutureClassStrenght
        {
            get { return _futureClassStrenght; }
            set
            {
                _futureClassStrenght = value;
                OnPropertyChanged(() => FutureClassStrenght);
            }
        }

        public Action AfterRefreshAction { get; set; }

        #endregion

        #region BaseViewModel

        public override object GetEntity(Guid id)
        {
            throw new NotImplementedException();
        }

        public override IRepositoryCommon GetRepository()
        {
            throw new NotImplementedException();
        }

        public override BaseEditDialog GetDialog()
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
