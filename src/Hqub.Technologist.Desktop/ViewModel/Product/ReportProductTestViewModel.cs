﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Documents;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Desktop.Model.Wrappers;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.ViewModel.Product
{
    /// <summary>
    /// Отчет по ГП
    /// </summary>
    public class ReportProductTestViewModel : BaseViewModel<ProductCharacterModel>
    {
        private readonly IProductTestRepository _productTestRepository;
        private readonly IProductCharacterRepository _characterRepository;
        private readonly ILotRepository _lotRepository;
        private ObservableCollection<ReportProductTestWrapper> _reportItems;
        private ProductTestModel _selectedProduct;
        private DateTime _endDate;
        private DateTime _startDate;

        public ReportProductTestViewModel(IProductTestRepository productTestRepository,
            IProductCharacterRepository characterRepository, ILotRepository lotRepository)
        {
            _productTestRepository = productTestRepository;
            _characterRepository = characterRepository;
            _lotRepository = lotRepository;
        }

        protected override void PreInit()
        {
            _startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            _endDate = StartDate.AddMonths(1);
        }

        public override void Refresh(bool force = false)
        {
            LoadReport(StartDate, EndDate);
        }

        private void LoadReport(DateTime start, DateTime end)
        {
            var products =
                AutoMapper.Mapper.Map<List<ProductTestModel>>(
                    _productTestRepository.Where(x => x.TestDate >= start && x.TestDate <= end));

            ReportItems = new ObservableCollection<ReportProductTestWrapper>();

            foreach (var productTest in products)
            {
                var reportItem = new ReportProductTestWrapper
                {
                    Product = productTest,
                    Lot = productTest.SelectedLot,
                    ProductCharacter = productTest.SelectedCharacter
                };

                ReportItems.Add(reportItem);
            }
            
        }

        #region Properties

        public ObservableCollection<ReportProductTestWrapper> ReportItems
        {
            get { return _reportItems; }
            set
            {
                _reportItems = value;
                OnPropertyChanged(() => ReportItems);
            }
        }

        public ProductTestModel SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                _selectedProduct = value;
                OnPropertyChanged(() => SelectedProduct);
            }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                _startDate = value;
                _endDate = _startDate.AddMonths(1);

                RefreshAsync();

                OnPropertyChanged(() => StartDate);
                OnPropertyChanged(() => EndDate);
            }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;

                RefreshAsync();
                OnPropertyChanged(() => EndDate);
            }
        }

        #endregion

        public override object GetEntity(Guid id)
        {
            throw new NotImplementedException();
        }

        public override IRepositoryCommon GetRepository()
        {
            throw new NotImplementedException();
        }

        public override BaseEditDialog GetDialog()
        {
            throw new NotImplementedException();
        }
    }
}
