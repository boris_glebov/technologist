﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Events;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Desktop.Model.TestProduct;
using Hqub.Technologist.Desktop.NSI;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;
using Hqub.Technologist.Store;
using System.Collections.Generic;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.ViewModel.Product
{
    /// <summary>
    /// Испытания ГП
    /// </summary>
    public class ProductTestViewModel : BaseViewModel<ProductTestModel>
    {
        #region Fields

        private List<EmployeeModel> _employees;
        private EmployeeModel _selectedEmployee;
        private  IProductTestRepository _productTest;
        private  IEmployeeRepository _employeeRepository;
        private  ILotRepository _lotRepository;
        private  ISampleRepository _sampleRepository;
        private  ITestMethodRepository _testMethodRepository;
        private  IStatisticRepository _statisticRepository;
        private List<LotModel> _lots;
        private LotModel _selectedLot;
        private ProductTestModel _model;
        private ObservableCollection<SampleModel> _samples;
        private ObservableCollection<SampleBottleModel> _sampleBottles;
        private ObservableCollection<TestProductCalcItem> _calcItems;
        private SampleModel _selectedSample;
        private SampleBottleModel _selectedSampleBottle;
        private RecalcEvent _recalcEvent;
        private bool _isOk;

        private TestMethod _testMethod;
        private string _markName;
        private string _className;
        private Tuple<string, float> _markStatistic;
        private Tuple<string, float> _markClassStatistic;

        #endregion

        #region  .ctor

        public ProductTestViewModel()
        {
            Subscribe();
        }

        private void CheckConfigure()
        {
          
        }

        private void LoadTestProduct()
        {
            IsOk = true;


            var product = AutoMapper.Mapper.Map<ProductTestModel>(_productTest.GetByMarkId(SelectedLot.Id));
            if (product == null)
            {
                Model = new ProductTestModel();

                GenerateSamples();
                GenerateSampleBottles();
            }
            else
            {
                Model = product;

                Samples = AutoMapper.Mapper.Map<ObservableCollection<SampleModel>>(Model.SampleModels);
                SampleBottles = AutoMapper.Mapper.Map<ObservableCollection<SampleBottleModel>>(Model.SampleBottles);
                GenerateCalcItems();

                SelectedEmployee = Employees.FirstOrDefault(x => x.Id == Model.SelectedEmployee.Id);
            }

            Model.Lot = SelectedLot.Id;
        }

        #endregion

        #region Ovveride BaseViewModel

        public override object GetEntity(Guid id)
        {
            return _productTest.Get(id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _productTest;
        }

        public override BaseEditDialog GetDialog()
        {
            throw new NotImplementedException();
        }

        public override void Unload()
        {
            Unsubscribe();
        }

        protected override void PreInit()
        {
            base.PreInit();

            CheckConfigure();

            _productTest = Locator.GetProductTestRepository();
            _employeeRepository = Locator.GetEmployeeRepository();
            _lotRepository = Locator.GetLotRepository();
            _sampleRepository = Locator.GetSampleRepository();
            _testMethodRepository = Locator.GetTestMethodRepository();
            _statisticRepository = Locator.GetStatisticRepository();
        }

        public override void Refresh(bool force = false)
        {
            _testMethod = _testMethodRepository.Get();
            

            if (SelectedLot == null)
            {
                Employees =
                    new List<EmployeeModel>(AutoMapper.Mapper.Map<List<EmployeeModel>>(_employeeRepository.List()));
                Lots = new List<LotModel>(AutoMapper.Mapper.Map<List<LotModel>>(_lotRepository.List()));
            }
            else
            {
                SelectedEmployee = null;
                CalcItems = new ObservableCollection<TestProductCalcItem>();
                LoadTestProduct();

                Recalc();
            }
        }

        #endregion

        #region Private Methods

        private void Subscribe()
        {
           _recalcEvent = AggregationEventService.Instance.GetEvent<RecalcEvent>();
            _recalcEvent.Subscribe(Recalc);
        }

        private void Unsubscribe()
        {
            _recalcEvent.Unsubscribe(Recalc);
        }

        private void GenerateSamples()
        {
            Samples = new ObservableCollection<SampleModel>();
            CalcItems.Clear();

            for (var i = 1; i <= 6; i++)
            {
                AddSample();
            }
        }

        private void AddSample()
        {
            var order = Samples.Count + 1;
            var sample = new SampleModel
            {
                Order = order,
                OrderDescription = GetDirection(order),
                ProductTestId = Model.Id
            };

            Samples.Add(sample);
            AddCalcItem(order);
        }

        private void GenerateSampleBottles()
        {
            SampleBottles = new ObservableCollection<SampleBottleModel>();
            AddSampleBottle();
        }

        private void AddSampleBottle()
        {
            var sampleBottle = new SampleBottleModel
            {
                Order = 1,
                ProductTestId = Model.Id
            };

            SampleBottles.Add(sampleBottle);
            Recalc();
        }

        private string GetDirection(int order)
        {
            var directions = new[] {"верх", "середина", "низ"};

            string direction;
            if (order%2 == 0)
                direction = directions[1];
            else if (order%3 == 0)
                direction = directions[2];
            else
                direction = directions[0];

            return direction;
        }

        private void AddCalcItem(int order)
        {
            var calcItem = new TestProductCalcItem
            {
                Order = order
            };

            CalcItems.Add(calcItem);
            UpdateCalcItems();
        }

        private void UpdateCalcItems()
        {
            for (int i = 0; i < CalcItems.Count; ++i)
            {
                var item = CalcItems[i];
                var sample = Samples[i];

                item.WetDensity = Math.TestProductMath.GetWetDensity(sample.Length, sample.Width, sample.Height,
                    sample.Weight);
                item.DryDensity = Math.TestProductMath.GetDryDensity(item.WetDensity, GetAverageHumidity());
                item.StrenghtLimit = GetStrenghtLimit(sample.BreakingLoad, sample.Length, sample.Width);
            }

            if (CalcItems == null || CalcItems.Count <= 0) return;
            
            Model.WetDensityAverage = CalcItems.Average(x => x.WetDensity);
            Model.DryDensityAverage = CalcItems.Average(x => x.DryDensity);
            Model.StrenghtLimitAverage = CalcItems.Average(x => x.StrenghtLimit);

            Model.DensityVariation = GetDensityVariation();
            Model.StrenghtVariation = GetStrengthVariation();

            MarkStatistic = GetMark(Model.DryDensityAverage);
            MarkClassStatistic = GetMarkClass(Model.StrenghtLimitAverage);

            if(MarkStatistic == null)
            {
                Model.MarkNameRequired = Utitlities.StatisticHelper.GetDefaultMark(Model.DryDensityAverage);
                Model.DensityRequired = float.Parse(Model.MarkNameRequired.Substring(1));
            }
            else
            {
                Model.DensityRequired = MarkStatistic.Item2;
                Model.MarkNameRequired = MarkStatistic.Item1;
            }
            
            if(MarkClassStatistic == null)
            {
                Model.ClassNameRequired = Utitlities.StatisticHelper.GetDefaultClass(Model.StrenghtLimitAverage);
                Model.StrenghtRequired = float.Parse(Model.ClassNameRequired.Substring(1));
            }
            else
            {
                Model.StrenghtRequired = MarkClassStatistic.Item2;
                Model.ClassNameRequired = MarkClassStatistic.Item1;
            }

            if (SampleBottles != null && SampleBottles.Count > 0)
                Model.Wetness = SampleBottles.Average(x => x.Wetness);
        }

        #region Statisitic

        private Tuple<string, float> GetMark(float density)
        {
            return _statisticRepository.GetStatisticMark(density, Model.TestDate.Month, Model.TestDate.Year);
        }

        private Tuple<string, float> GetMarkClass(float strenght)
        {
            return _statisticRepository.GetStatisticMarkClass(strenght, Model.TestDate.Month, Model.TestDate.Year);
        }

        #endregion

        /// <summary>
        /// Расчет предела прочности
        /// </summary>
        /// <param name="strengthLimit">плотность в сухом состоянии</param>
        /// <param name="length"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        private float GetStrenghtLimit(float strengthLimit, float length, float width)
        {
            var humidity = (int) System.Math.Round(GetAverageHumidity());
            var correction = CorrectionFactor.Get(humidity);
            var scaleFactor = ScaleFactor.Get((SampleType) _testMethod.SampleType);

            var conversion = _testMethod.UnitsPress == (int) UnitType.kN ? 102 : 1;

            return Math.TestProductMath.GetCrushingStress(strengthLimit, length, width, conversion, correction,
                scaleFactor);
        }

        /// <summary>
        /// Вычислить текущий коэф. вариации по плотности
        /// </summary>
        /// <returns></returns>
        private float GetDensityVariation()
        {
            var maxDensity = CalcItems.Max(x => x.DryDensity);
            var minDensity = CalcItems.Min(x => x.DryDensity);

            // Размах значений:
            var swingDensity = maxDensity - minDensity;
            var alpha = AlphaByTestNumberFactory.Get(Samples.Count);

            return Math.TestProductMath.GetCoefficientVariationStrength((swingDensity/alpha), Model.DryDensityAverage);
        }

        /// <summary>
        /// Вычислить текущий коэф. вариации по прочности
        /// </summary>
        /// <returns></returns>
        private float GetStrengthVariation()
        {
            var maxStrength = CalcItems.Max(x => x.StrenghtLimit);
            var minStrength = CalcItems.Min(x => x.StrenghtLimit);

            // Размах значений:
            var swingStrength = maxStrength - minStrength;
            var alpha = AlphaByTestNumberFactory.Get(Samples.Count);

            return Math.TestProductMath.GetCoefficientVariationStrength((swingStrength/alpha),
                Model.StrenghtLimitAverage);
        }

        private float GetAverageHumidity()
        {
            if (SampleBottles == null || SampleBottles.Count == 0)
                return 0;

            return SampleBottles.Average(x => x.Wetness);
        }

        private void GenerateCalcItems()
        {
            for (var i=1; i<=Samples.Count; i++)
            {
                AddCalcItem(i);
            }
        }

        private void Recalc(object e = null)
        {
            UpdateCalcItems();

            OnPropertyChanged("Mark");
        }

        private bool Validate()
        {
            if (Samples == null || Samples.Count == 0)
                return false;

            if (SampleBottles == null || SampleBottles.Count == 0)
                return false;

            if (SelectedEmployee == null)
                return false;

            return true;
        }

        #endregion

        #region Commands

        public ICommand AddSampleCommand
        {
            get { return new DelegateCommand(AddSampleCommandExecute); }
        }

        private void AddSampleCommandExecute()
        {
            AddSample();
        }

        public ICommand DeleteSampleCommand
        {
            get { return new DelegateCommand(DeleteSampleCommandExecute); }
        }

        private void DeleteSampleCommandExecute()
        {
            if (SelectedSample == null) return;

            CalcItems.RemoveAt(SelectedSample.Order - 1);
            Samples.Remove(SelectedSample);
            SamplesRecalc();
        }

        private void SamplesRecalc()
        {
            for (var i = 1; i <= Samples.Count; ++i)
            {
                Samples[i - 1].Order = i;
                CalcItems[i - 1].Order = i;
            }
        }

        public ICommand AddSampleBottleCommand
        {
            get { return new DelegateCommand(AddSampleBottleCommandExecute); }
        }

        private void AddSampleBottleCommandExecute()
        {
            SampleBottles.Add(new SampleBottleModel
            {
                Order = SampleBottles.Count + 1
            });
        }

        public ICommand DeleteSampleBottleCommand
        {
            get { return new DelegateCommand(DeleteSampleBottleCommandExecute); }
        }

        private void DeleteSampleBottleCommandExecute()
        {
            if (SelectedSampleBottle == null)
                return;

            SampleBottles.Remove(SelectedSampleBottle);
            SampleBottlesRecalc();
        }

        private void SampleBottlesRecalc()
        {
            for (var i = 1; i <= SampleBottles.Count; ++i)
            {
                SampleBottles[i - 1].Order = i;
            }
        }

        public ICommand SaveCommand
        {
            get { return new DelegateCommand(SaveCommandExecute); }
        }

        private void SaveCommandExecute()
        {
            IsBusy = true;

            ThreadPool.QueueUserWorkItem(delegate
            {
                if (!Validate())
                {
                    IsBusy = false;
                    return;
                }

                Recalc();

                Model.Save(Samples.ToList(), SampleBottles.ToList());

                IsBusy = false;
            });
        }

        #endregion

        #region Properties

        private string _textSearch;
        public string TextSearch
        {
            get { return _textSearch; }
            set
            {
                _textSearch = value;

                if (_selectedLot != null && _selectedLot.LotNumber == value)
                    RefreshAsync();

                OnPropertyChanged(() => TextSearch);
            }
        }

        public List<EmployeeModel> Employees
        {
            get { return _employees; }
            set
            {
                _employees = value;
                OnPropertyChanged(() => Employees);
            }
        }

        public EmployeeModel SelectedEmployee
        {
            get { return _selectedEmployee; }
            set
            {
                _selectedEmployee = value;
                if (_selectedEmployee != null)
                {
                    Model.User = _selectedEmployee.Id;
                }

                OnPropertyChanged(() => SelectedEmployee);
            }
        }

        public List<LotModel> Lots
        {
            get { return _lots; }
            set
            {
                _lots = value;
                OnPropertyChanged(() => Lots);
            }
        }

        public LotModel SelectedLot
        {
            get { return _selectedLot; }
            set
            {
                _selectedLot = value;
                OnPropertyChanged(() => SelectedLot);
            }
        }

        public ProductTestModel Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged(() => Model);
            }
        }

        public ObservableCollection<SampleModel> Samples
        {
            get { return _samples; }
            set
            {
                _samples = value;
                OnPropertyChanged(() => Samples);
            }
        }

        public SampleModel SelectedSample
        {
            get { return _selectedSample; }
            set
            {
                _selectedSample = value;
                OnPropertyChanged(() => SelectedSample);
            }
        }

        public ObservableCollection<SampleBottleModel> SampleBottles
        {
            get { return _sampleBottles; }
            set
            {
                _sampleBottles = value;
                OnPropertyChanged(() => SampleBottles);
            }
        }

        public SampleBottleModel SelectedSampleBottle
        {
            get { return _selectedSampleBottle; }
            set
            {
                _selectedSampleBottle = value;
                OnPropertyChanged(() => SelectedSampleBottle);
            }
        }

        public ObservableCollection<TestProductCalcItem> CalcItems
        {
            get { return _calcItems; }
            set
            {
                _calcItems = value;
                OnPropertyChanged(() => CalcItems);
            }
        }

        #region Mark name and Class name

        public string MarkName
        {
            get { return _markName; }
            set
            {
                _markName = value;
                OnPropertyChanged(() => MarkName);
            }
        }

        public string ClassName
        {
            get { return _className; }
            set
            {
                _className = value;
                OnPropertyChanged(() => ClassName);
            }
        }

        public Tuple<string, float> MarkStatistic
        {
            get { return _markStatistic; }
            set
            {
                _markStatistic = value;
                OnPropertyChanged(() => MarkStatistic);
            }
        }

        public Tuple<string, float> MarkClassStatistic
        {
            get { return _markClassStatistic; }
            set
            {
                _markClassStatistic = value;
                OnPropertyChanged(() => MarkClassStatistic);
            }
        }

        #endregion

        public bool IsOk
        {
            get { return _isOk; }
            set
            {
                _isOk = value;
                OnPropertyChanged(() => IsOk);
            }
        }

        public List<string> Directions
        {
            get
            {
                return new List<string>
                {
                    "верх",
                    "середина",
                    "низ"
                };
            }
        } 

        #region Headers

        public string StrengthLimitHeader
        {
            get
            {
                if (_testMethod == null)
                    return "кгс";

                var unitName = _testMethod.UnitsPress == (int) UnitType.kN ? "kN" : "кгc";

                return string.Format("Разрушающая\nнагрузка, {0}", unitName);
            }
        }

        #endregion

        #endregion
    }
}
