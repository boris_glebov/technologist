﻿using System;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Navigation;
using Microsoft.Practices.Prism.Commands;
using Telerik.Windows.Controls;

namespace Hqub.Technologist.Desktop.ViewModel.Shell
{
    public class ShellViewModel : BindableViewModel
    {
        public ShellViewModel()
        {
            // Подписываемся на событие обработки:
            SubsribeOnEvents();
        }

        private void SubsribeOnEvents()
        {


        }

        public override void Refresh(bool force = false)
        {

        }

        #region Commands

        public ICommand GoCommand { get { return new DelegateCommand<string>(GoCommandExecute);} }

        private void GoCommandExecute(string unit)
        {
         
            switch (unit)
            {
                case "library":
                    NavigationHelper.OpenDirectoryRootPanel();
                    break;
                case "production":
                    NavigationHelper.OpenAssessmentRootPanel();
                    break;
                case "product":
                    NavigationHelper.OpenProductRootPanel();
                    break;
                case "materials":
                    NavigationHelper.OpenMaterialRootPanel();
                    break;
                case "tools":
                    NavigationHelper.OpenSettingsRootPanel();
                    break;
            }
        }

        #endregion
    }
}
