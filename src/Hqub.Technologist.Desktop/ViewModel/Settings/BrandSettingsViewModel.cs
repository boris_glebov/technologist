﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using AutoMapper;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Dialogs.Settings;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.ViewModel.Settings
{
    public class BrandSettingsViewModel : BaseViewModel<BrandModel>
    {
        private IBrandRepository _brandRepository;

        public override object GetEntity(Guid id)
        {
            return _brandRepository.Get(id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _brandRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            return new BrandEntityDialog();
        }

        protected override void PreInit()
        {
            base.PreInit();

            _brandRepository = Locator.GetBrandRepository();
        }

        public override void Refresh(bool force = false)
        {
            var brands = _brandRepository.List();
            var wrapperBrands = brands.Select(Mapper.Map<BrandModel>).ToList();

            Items = new ObservableCollection<BrandModel>(wrapperBrands);
        }
    }
}
