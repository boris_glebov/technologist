﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Dialogs.Settings;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Desktop.Navigation;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.ViewModel.Settings
{
    public class QualitySettingsViewModel : BaseViewModel<QualitySettingModel>
    {
        #region Field

        private IQualityControlSettingsRepository _qualityControlSettingsRepository;

        private bool _isTestGPSelected;
        private bool _isQualityArraySelected;
        private bool _isFunctionalControlSelected;
        private ObservableCollection<QualitySettingModel> _qualitySettings;
        private QualitySettingModel _selectedQualitySettingModel;

        #endregion


        #region Methods

        protected override void PreInit()
        {
            base.PreInit();

            _qualityControlSettingsRepository = Locator.GetQualityControlSettingsRepository();

            if (NavigationHelper.NavigationManager.GetLastModuleName() == "Параметры процесса")
            {
                IsFunctionalControlSelected = true;
            }
        }

        public override void Refresh(bool force = false)
        {
            QualitySettings =
                new ObservableCollection<QualitySettingModel>(
                    AutoMapper.Mapper.Map<List<QualitySettingModel>>(_qualityControlSettingsRepository.List()));
        }

        public override object GetEntity(Guid id)
        {
            return _qualityControlSettingsRepository.Get(id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _qualityControlSettingsRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            return
                new QualitySettingEntityDialog(IsFunctionalControlSelected
                    ? QualityControlType.FunctionalControl
                    : QualityControlType.QualityArray);
        }

        #endregion

        #region Properties

        public ObservableCollection<QualitySettingModel> QualitySettings
        {
            get { return _qualitySettings; }
            set
            {
                _qualitySettings = value;
                OnPropertyChanged(() => QualitySettings);
                OnPropertyChanged(() => QualitySettingQualityArray);
                OnPropertyChanged(() => QualitySettingFunctionalControl);
            }
        }

        public List<QualitySettingModel> QualitySettingQualityArray
        {
            get
            {
                return _qualitySettings == null
                    ? new List<QualitySettingModel>()
                    : _qualitySettings.Where(x => x.ControlType == QualityControlType.QualityArray).ToList();
            }
        }

        public List<QualitySettingModel> QualitySettingFunctionalControl
        {
            get
            {
                return _qualitySettings == null
                    ? null
                    : _qualitySettings.Where(x => x.ControlType == QualityControlType.FunctionalControl).ToList();
            }
        }

        public QualitySettingModel SelectedQualitySettingModel
        {
            get { return _selectedQualitySettingModel; }
            set
            {
                _selectedQualitySettingModel = value; 
                OnPropertyChanged(() => SelectedQualitySettingModel);
            }
        }

        public bool IsQualityArraySelected
        {
            get { return _isQualityArraySelected; }
            set
            {
                _isQualityArraySelected = value;
                OnPropertyChanged(() => IsQualityArraySelected);
            }
        }

        public bool IsFunctionalControlSelected
        {
            get { return _isFunctionalControlSelected; }
            set
            {
                _isFunctionalControlSelected = value;
                OnPropertyChanged(() => IsFunctionalControlSelected);
            }
        }

        #endregion
    }
}
