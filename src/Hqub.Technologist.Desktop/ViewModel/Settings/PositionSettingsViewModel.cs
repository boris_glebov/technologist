﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using AutoMapper;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Dialogs.Settings;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.ViewModel.Settings
{
    public class PositionSettingsViewModel : BaseViewModel<PositionModel>
    {
        private readonly IPositionRepository _positionRepository;

        public PositionSettingsViewModel(IPositionRepository positionRepository)
        {
            _positionRepository = positionRepository;
        }

        public override object GetEntity(Guid id)
        {
            return _positionRepository.Get(SelectedItem.Id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _positionRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            return new PositionEntityDialog();
        }

        public override void Refresh(bool force = false)
        {
            var positions = _positionRepository.List();
            var wrapperPositions = positions.Select(Mapper.Map<PositionModel>).ToList();

            Items = new ObservableCollection<PositionModel>(wrapperPositions);
        }
    }
}