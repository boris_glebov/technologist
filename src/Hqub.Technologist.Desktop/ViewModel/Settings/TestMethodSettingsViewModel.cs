﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model.NSI;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;
using Microsoft.Practices.Prism.Commands;
using Telerik.Windows.Controls.Data;

namespace Hqub.Technologist.Desktop.ViewModel.Settings
{
    public class TestMethodSettingsViewModel : BaseViewModel<TestMethodModel>
    {
        private ITestMethodRepository _testMethodRepository;

        protected override void PreInit()
        {
            base.PreInit();

            _testMethodRepository = Locator.GetTestMethodRepository();
        }

        public override void Refresh(bool force = false)
        {
            var entity = _testMethodRepository.Get() ?? new TestMethod();

            Model = AutoMapper.Mapper.Map<TestMethodModel>(entity);
            Model.Id = Guid.NewGuid();
            Model.CreateStamp = DateTime.Now;

            // Load directories:
            SlakedLimeDirectory = NSI.SlakedLimeTypeDirectory.GetSlakedLimeTypes();
            SlakedLimeSelected = SlakedLimeDirectory.First(x => x.Value == Model.SlakedLimeMethod);

            SpecificHeatCapacityDirectory = NSI.SpecificHeatCapacityDirectory.GetSpecificHeatCapacityTypes();
            SpecificHeatCapacitySelected =
                SpecificHeatCapacityDirectory.First(x => x.Value == Model.SpecificHeatCapacityMethod);

            SampleTypeDirectory = NSI.SampleTypeDirectory.GetSampleTypes();
            SampleTypeSelected = SampleTypeDirectory.First(x => x.Value == Model.SampleType);

            PeriodQualityControlDirectory = NSI.PeriodQualityControlDirectory.GetPeriodQualityControlTypes();
            PeriodQualityControlTypeSelected =
                PeriodQualityControlDirectory.First(x => x.Value == Model.PeriodQualityControl);

            UnitTypeDirectory = NSI.UnitTypeDirectory.GetUnitTypes();
            UnitTypeSelected = UnitTypeDirectory.First(x => x.Value == Model.UnitsPress);

            // Init errors collection:
            Errors = new ObservableCollection<ErrorInfo>();
        }

        public override object GetEntity(Guid id)
        {
            return _testMethodRepository.Get(id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _testMethodRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            throw new NotImplementedException();
        }

        #region Properties

        private TestMethodModel _model;
        public TestMethodModel Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged(() => Model);
            }
        }

        #region SlakedLimeDirectory

        private List<DirectoryItem<SlakedLimeMethodType>> _slakedLimeDirectory;
        public List<DirectoryItem<SlakedLimeMethodType>> SlakedLimeDirectory
        {
            get { return _slakedLimeDirectory; }
            set
            {
                _slakedLimeDirectory = value;
                OnPropertyChanged(() => SlakedLimeDirectory);
            }
        }

        private DirectoryItem<SlakedLimeMethodType> _slakedLimeSelected;
        public DirectoryItem<SlakedLimeMethodType> SlakedLimeSelected
        {
            get { return _slakedLimeSelected; }
            set
            {
                _slakedLimeSelected = value;
                Model.SlakedLimeMethod = value.Value;
                OnPropertyChanged(() => SlakedLimeSelected);
            }
        }

        #endregion

        #region SpecificHeatCapacityDirectory

        private List<DirectoryItem<SpecificHeatCapacityType>> _specificHeatCapacityDirectory;
        public List<DirectoryItem<SpecificHeatCapacityType>> SpecificHeatCapacityDirectory
        {
            get { return _specificHeatCapacityDirectory; }
            set
            {
                _specificHeatCapacityDirectory = value;
                OnPropertyChanged(() => SpecificHeatCapacityDirectory);
            }
        }

        private DirectoryItem<SpecificHeatCapacityType> _specificHeatCapacitySelected;
        public DirectoryItem<SpecificHeatCapacityType> SpecificHeatCapacitySelected
        {
            get { return _specificHeatCapacitySelected; }
            set
            {
                _specificHeatCapacitySelected = value;
                Model.SpecificHeatCapacityMethod = value.Value;
                OnPropertyChanged(() => SpecificHeatCapacitySelected);
            }
        }


        #endregion

        #region SampleTypeDirectory

        private List<DirectoryItem<SampleType>> _sampleTypeDirectory;
        public List<DirectoryItem<SampleType>> SampleTypeDirectory
        {
            get { return _sampleTypeDirectory; }
            set
            {
                _sampleTypeDirectory = value;
                OnPropertyChanged(() => SampleTypeDirectory);
            }
        }

        private DirectoryItem<SampleType> _sampleTypeSelected;
        public DirectoryItem<SampleType> SampleTypeSelected
        {
            get { return _sampleTypeSelected; }
            set
            {
                _sampleTypeSelected = value;
                Model.SampleType = value.Value;

                OnPropertyChanged(() => SampleTypeSelected);
            }
        }

        #endregion

        #region Units

        private List<DirectoryItem<UnitType>> _unitTypeDirectory;
        public List<DirectoryItem<UnitType>> UnitTypeDirectory
        {
            get { return _unitTypeDirectory; }
            set
            {
                _unitTypeDirectory = value;
                OnPropertyChanged(() => UnitTypeDirectory);
            }
        }

        private DirectoryItem<UnitType> _unitTypeSelected;
        public DirectoryItem<UnitType> UnitTypeSelected
        {
            get { return _unitTypeSelected; }
            set
            {
                _unitTypeSelected = value;
                Model.UnitsPress = value.Value;

                OnPropertyChanged(() => UnitTypeSelected);
            }
        }

        #endregion

        #region PeriodQualityControl

        private List<DirectoryItem<PeriodQualityControlType>> _periodQualityControlDirectory;
        public List<DirectoryItem<PeriodQualityControlType>> PeriodQualityControlDirectory
        {
            get { return _periodQualityControlDirectory; }
            set
            {
                _periodQualityControlDirectory = value;
                OnPropertyChanged(() => PeriodQualityControlDirectory);
            }
        }

        private DirectoryItem<PeriodQualityControlType> _periodQualityControlTypeSelected;
        public DirectoryItem<PeriodQualityControlType> PeriodQualityControlTypeSelected
        {
            get { return _periodQualityControlTypeSelected; }
            set
            {
                _periodQualityControlTypeSelected = value;
                Model.PeriodQualityControl = value.Value;
                OnPropertyChanged(() => PeriodQualityControlTypeSelected);
            }
        }

        #endregion

        #region Errors

        private ObservableCollection<ErrorInfo> _errors;
        public ObservableCollection<ErrorInfo> Errors
        {
            get { return _errors; }
            set
            {
                _errors = value;
                OnPropertyChanged(() => Errors);
            }
        }

        #endregion

        #endregion

        #region Commands

        public ICommand SaveCommand
        {
            get { return new DelegateCommand(SaveCommandExecute); }
        }

        private void SaveCommandExecute()
        {
            IsBusy = true;

            Application.Current.Dispatcher.BeginInvoke(new Action(delegate
            {
                Errors.Clear();

                ThreadPool.QueueUserWorkItem(delegate
                {
                    try
                    {
                        if (!Model.Save())
                        {
                            Errors.Add(new ErrorInfo
                            {
                                ErrorContent = "Не удалось сохранить запись. Обратитесь к администратору системы.",
                                SourceFieldDisplayName = "Применение настроек"
                            });
                        }

                        Thread.Sleep(1000);
                    }
                    finally
                    {
                        IsBusy = false;
                    }
                });
            }));
        }

        #endregion
    }
}
