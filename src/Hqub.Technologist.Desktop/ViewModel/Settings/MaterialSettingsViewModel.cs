﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using AutoMapper;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Dialogs.Settings;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store.Repository;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.ViewModel.Settings
{
    public class MaterialSettingsViewModel : BaseViewModel<MaterialModel>
    {
        private readonly IMaterialRepository _materialRepository;

        public MaterialSettingsViewModel(IMaterialRepository materialRepository)
        {
            _materialRepository = materialRepository;
        }

        public override object GetEntity(Guid id)
        {
            return _materialRepository.Get(SelectedItem.Id);
        }

        public override IRepositoryCommon GetRepository()
        {
            return _materialRepository;
        }

        public override BaseEditDialog GetDialog()
        {
            return new MaterialEntityDialog();
        }

        public override void Refresh(bool force = false)
        {
            var materials = _materialRepository.List();
            var wrapperMaterials = materials.Select(Mapper.Map<MaterialModel>).ToList();

            Items = new ObservableCollection<MaterialModel>(wrapperMaterials);
        }
    }
}
