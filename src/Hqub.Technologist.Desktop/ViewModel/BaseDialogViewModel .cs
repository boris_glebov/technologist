﻿using System;

namespace Hqub.Technologist.Desktop.ViewModel
{
    public class BaseDialogViewModel : BindableViewModel
    {
        #region .ctor

        public BaseDialogViewModel()
        {

        }

        public BaseDialogViewModel(Action closeAction)
        {
            SetCloseAction(closeAction);
        }

        #endregion

        #region Close dialog

        public void SetCloseAction(Action closeAction)
        {
            Close = closeAction;
        }

        //
        //Безопасный метод закрытия диалогового окна
        //
        protected void CloseDialog()
        {
            /*
             * Метод не должен содержать ни какой логики.
             * Если требуется сделать, что то после закрытия требуется заместить метод
             * ClosingCommandExecute
             */
            if (Close != null)
                Close();
        }

        private Action Close { get; set; }

        #endregion

        public override void Refresh(bool force = false)
        {
            throw new NotImplementedException();
        }
    }
}
