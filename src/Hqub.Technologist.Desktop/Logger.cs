﻿using System;

namespace Hqub.Technologist.Desktop
{
    public class Logger 
    {
        public static NLog.Logger Main
        {
            get { return NLog.LogManager.GetLogger("Main"); }
        }

        public void Error(string message,Exception exception = null)
        {
            Main.Error(message);
        }

        public void Warning(string message)
        {
            Main.Warn(message);
        }

        public void Fatal(string message, Exception exception)
        {
            Main.Fatal(message, exception);
        }
    }
}
