﻿using System;
using System.ComponentModel;
using AutoMapper;
using Hqub.Technologist.Desktop.Events;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class SampleModel : EntityBase, IHasValidate
    {
        private int _order;
        private string _orderDescription;
        private Guid _productTestId;
        private float _weight;
        private float _length;
        private float _height;
        private float _breakingLoad;
        private float _wetDensity;
        private float _dryDensity;
        private float _strenghtLimit;
        private float _width;
        private RecalcEvent _recalcEvent;

        #region .ctor

        public SampleModel()
        {
            _recalcEvent = AggregationEventService.Instance.GetEvent<RecalcEvent>();
        }

        #endregion

        #region Properties

        [Description("Номер образца")]
        public int Order
        {
            get { return _order; }
            set
            {
                _order = value; 
                OnPropertyChanged();
            }
        }

        [Description("Распол-е образцов в массиве")]
        public string OrderDescription
        {
            get { return _orderDescription; }
            set
            {
                _orderDescription = value; 
                OnPropertyChanged();
            }
        }

        [Description("Ссылка на Исптыание ГП")]
        public Guid ProductTestId
        {
            get { return _productTestId; }
            set
            {
                _productTestId = value;
                OnPropertyChanged();
            }
        }

        [Description("Масса образца в г.")]
        public float Weight
        {
            get { return _weight; }
            set
            {
                _weight = value;
                OnPropertyChanged();
                Update();
            }
        }

        [Description("Длина")]
        public float Length
        {
            get { return _length; }
            set
            {
                _length = value;
                OnPropertyChanged();
                Update();
            }
        }

        [Description("Ширина")]
        public float Width
        {
            get { return _width; }
            set
            {
                _width = value; 
                OnPropertyChanged();
                Update();
            }
        }

        [Description("Высота")]
        public float Height
        {
            get { return _height; }
            set
            {
                Update();
                _height = value;
                OnPropertyChanged();
                Update();
            }
        }

        [Description("Разрушающая нагрузка, kN")]
        public float BreakingLoad
        {
            get { return _breakingLoad; }
            set
            {
                _breakingLoad = value;
                OnPropertyChanged();
                Update();
            }
        }

        [Description("Плотность, кг/м.куб влажн.")]
        public float WetDensity
        {
            get { return _wetDensity; }
            set
            {
                _wetDensity = value;
                OnPropertyChanged();
                Update();
            }
        }

        [Description("Плотность, кг/м куб сух.")]
        public float DryDensity
        {
            get { return _dryDensity; }
            set
            {
                _dryDensity = value; 
                OnPropertyChanged();
                Update();
            }
        }

        [Description("Предел прочности при сжатии кг/см. кв")]        
        public float StrenghtLimit
        {
            get { return _strenghtLimit; }
            set
            {
                _strenghtLimit = value; 
                OnPropertyChanged();
                Update();
            }
        }

        #endregion

        private void Update(bool isChange = true)
        {
            System.Diagnostics.Debug.WriteLine("Update from SampleModel");
            _recalcEvent.Publish(null);
        }

        public override bool Save()
        {
            try
            {
                var sampleService = Locator.Get<ISampleRepository>();
                var sample = Mapper.Map<Sample>(this);

                sampleService.Save(sample);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("Sample.Save({0})", Id), exception);

                return false;
            }

            return true;
        }

        public string this[string columnName]
        {
            get { return string.Empty; }
        }

        public string Error { get; private set; }
    }
}
