﻿using System;
using System.ComponentModel;

namespace Hqub.Technologist.Desktop.Model.Store
{
    [Serializable]
    public class StatisticModel : EntityBase, IHasValidate
    {
        private Guid _markId;
        private string _markClass;
        private float _strengthRequired;
        private float _densityRequired;
        private DateTime _startDate;
        private string _markName;

        #region Properties

        [Description("Марка")]
        public Guid MarkId
        {
            get { return _markId; }
            set
            {
                _markId = value;
                OnPropertyChanged();
            }
        }

        [Description("Класс")]
        public string MarkClass
        {
            get { return _markClass; }
            set
            {
                _markClass = value; 
                OnPropertyChanged();
            }
        }

        [Description("Требумая прочность класса")]
        public float StrengthRequired
        {
            get { return _strengthRequired; }
            set
            {
                _strengthRequired = value;
                OnPropertyChanged();
            }
        }

        [Description("Требумая плотность")]
        public float DensityRequired
        {
            get { return _densityRequired; }
            set
            {
                _densityRequired = value;
                OnPropertyChanged();
            }
        }

        [Description("Месяц действия")]
        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                _startDate = value; 
                OnPropertyChanged();
            }
        }

        [Description("")]
        public string MarkName
        {
            get { return _markName; }
            set
            {
                _markName = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        #region EntityBase

        public override bool Save()
        {
            throw new NotImplementedException();
        }

        public string this[string columnName]
        {
            get { throw new NotImplementedException(); }
        }

        public string Error { get; private set; }

        #endregion
    }
}
