﻿using System;
using System.Xml.Serialization;
using Hqub.Technologist.Desktop.Utitlities.Extensions;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class PositionModel : EntityBase, IHasValidate
    {
        #region Properties

        public string Name { get; set; }

        #endregion

        #region Implementation IHasValidate

        public override bool Save()
        {
            try
            {
                var positionService = Locator.Get<IPositionRepository>();
                var position = AutoMapper.Mapper.Map<Position>(this);

                positionService.Save(position);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("Position.Save({0})", this.SerializeObject()), exception);
                return false;
            }

            return true;
        }

        #endregion

        #region IHasValidate

        public string this[string columnName]
        {
            get
            {
                var errorMessage = string.Empty;
                
                switch (columnName)
                {
                    case "Name":
                        if (string.IsNullOrEmpty(Name))
                        {
                            errorMessage = "Поле обязательно к заполнению.";
                        }
                        break;
                }

                return errorMessage;
            }
        }

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string Error { get; private set; }

        #endregion
    }
}
