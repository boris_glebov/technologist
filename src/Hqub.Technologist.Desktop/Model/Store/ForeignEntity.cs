﻿using System;

namespace Hqub.Technologist.Desktop.Model.Store
{
    /// <summary>
    /// Описывает представление для связанных сущностей.
    /// Используется в генерации контролов для редактирования сущности.
    /// </summary>
    public class ForeignEntity
    {
        public ForeignEntity()
        {
            
        }

        public ForeignEntity(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
