﻿using System;
using System.ComponentModel;
using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class ProductCharacterModel : EntityBase, IHasValidate
    {
        private string _notes;
        private string _specificActiivity;
        private float _dryingShrinkage;
        private float _waterVaporPermeability;
        private string _frostResist;
        private float _thermalConductivity;
        private Guid _productDensity;
        private DateTime _createStamp;

        public ProductCharacterModel()
        {
            Id = Guid.NewGuid();
            CreateStamp = DateTime.Now;
        }

        #region Properties

        [Description("Плотность")]
        public Guid ProductDensity
        {
            get { return _productDensity; }
            set
            {
                _productDensity = value;
                OnPropertyChanged();
            }
        }

        [Description("Теплопроводность")]
        public float ThermalConductivity
        {
            get { return _thermalConductivity; }
            set
            {
                _thermalConductivity = value;
                OnPropertyChanged();
            }
        }

        [Description("Морозостойкость")]
        public string FrostResist
        {
            get { return _frostResist; }
            set
            {
                _frostResist = value;
                OnPropertyChanged();
            }
        }

        [Description("Паропроницаемость")]
        public float WaterVaporPermeability
        {
            get { return _waterVaporPermeability; }
            set
            {
                _waterVaporPermeability = value;
                OnPropertyChanged();
            }
        }

        [Description("Усадка при высыхании")]
        public float DryingShrinkage
        {
            get { return _dryingShrinkage; }
            set
            {
                _dryingShrinkage = value;
                OnPropertyChanged();
            }
        }

        [Description("Удельная активность")]
        public string SpecificActivity
        {
            get { return _specificActiivity; }
            set
            {
                _specificActiivity = value;
                OnPropertyChanged();
            }
        }

        [Description("Примечание")]
        public string Notes
        {
            get { return _notes; }
            set
            {
                _notes = value;
                OnPropertyChanged();
            }
        }

        [Description("Дата последнего изменения или создания записи")]
        public DateTime CreateStamp
        {
            get { return _createStamp; }
            set { _createStamp = value; }
        }

        #endregion

        #region Extend

        private Brand _brandEntity;
        public Brand BrandEntity
        {
            get
            {
                if (_brandEntity != null)
                    return _brandEntity;

                var repository = Locator.GetBrandRepository();
                return _brandEntity = repository.Get(ProductDensity);
            }
        }

        #endregion

        public override bool Save()
        {
            try
            {
                var repository = Locator.GetProductCharacterRepository();
                var productCharacter = AutoMapper.Mapper.Map<ProductCharacter>(this);

                repository.Save(productCharacter);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("ProductCharacter.Save({0})", Id), exception);

                return false;
            }

            return true;
        }

        public string this[string columnName]
        {
            get { return string.Empty; }
        }

        public string Error { get; private set; }
    }
}
