﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Hqub.Technologist.Desktop.Utitlities.Extensions;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    [Serializable]
    public class SlimeTestInheritResultModel : EntityBase
    {
        private DateTime _dateTest;
        private Guid _laborantId;
        private float _massCylinder;
        private float _volumeCylinder;
        private float _massSlime;
        private float _density;

        public SlimeTestInheritResultModel()
        {
            DateTest = DateTime.Now;
        }

        #region Properties
        [Description("Дата")]
        public DateTime DateTest
        {
            get { return _dateTest; }
            set
            {
                _dateTest = value;
                OnPropertyChanged();
            }
        }

        [Description("Лаборант")]
        public Guid LaborantId
        {
            get { return _laborantId; }
            set
            {
                _laborantId = value;
                OnPropertyChanged();
            }
        }

        [Description("Масса пустого цилиндра")]
        public float MassCylinder
        {
            get { return _massCylinder; }
            set
            {
                _massCylinder = value;
                OnPropertyChanged();
            }
        }
        [Description("Объем цилиндра")]
        public float VolumeCylinder
        {
            get { return _volumeCylinder; }
            set
            {
                _volumeCylinder = value;
                OnPropertyChanged();
            }
        }

        [Description("Масса цилиндра со шламом")]
        public float MassSlime
        {
            get { return _massSlime; }
            set
            {
                _massSlime = value;
                OnPropertyChanged();
            }
        }
        [Description("Плотность")]
        public float Density
        {
            get { return _density; }
            set
            {
                _density = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Extend
        private string _laborantName;
        private EmployeeModel _laborantEntity;


        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string ReviewerName
        {
            get
            {
                if (_laborantName != null)

                    return _laborantName;

                var repository = Locator.GetEmployeeRepository();
                var reviwer = AutoMapper.Mapper.Map<EmployeeModel>(repository.Get(_laborantId));
                return _laborantName = reviwer != null ? reviwer.FullName : String.Empty;

            }
        }

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public EmployeeModel LaborantEntity
        {
            get
            {
                if (_laborantEntity != null)
                    return _laborantEntity;

                var repository = Locator.GetEmployeeRepository();
                return _laborantEntity = AutoMapper.Mapper.Map<EmployeeModel>(repository.Get(_laborantId));
            }
        }

        #endregion
        #region Implemented IEditable

        public override bool Save()
        {
            try
            {
                var slimeService = Locator.Get<ISlimeTestInheritResultRepository>();
                var slime= AutoMapper.Mapper.Map<SlimeTestInheritResult>(this);

                slimeService.Save(slime);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("SlimeTestInheritResultModel.Save({0})", this.SerializeObject()), exception);

                return false;
            }

            return true;
        }

        #endregion
        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string Error { get; private set; }
    }
}
