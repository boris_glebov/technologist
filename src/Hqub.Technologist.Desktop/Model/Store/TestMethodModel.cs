﻿using System;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class TestMethodModel : EntityBase, IHasValidate
    {
        private SlakedLimeMethodType _slakedLimeMethod;
        private SpecificHeatCapacityType _specificHeatCapacityMethod;
        private SampleType _sampleType;
        private PeriodQualityControlType _periodQualityControl;
        private DateTime _createStamp;
        private UnitType _unitsPress;

        #region Properties

        public SlakedLimeMethodType SlakedLimeMethod
        {
            get { return _slakedLimeMethod; }
            set
            {
                _slakedLimeMethod = value; 
                OnPropertyChanged();
            }
        }

        public SpecificHeatCapacityType SpecificHeatCapacityMethod
        {
            get { return _specificHeatCapacityMethod; }
            set
            {
                _specificHeatCapacityMethod = value;
                OnPropertyChanged();
            }
        }

        public SampleType SampleType
        {
            get { return _sampleType; }
            set
            {
                _sampleType = value;
                OnPropertyChanged();
            }
        }

        public UnitType UnitsPress
        {
            get { return _unitsPress; }
            set
            {
                _unitsPress = value; 
                OnPropertyChanged();
            }
        }

        public PeriodQualityControlType PeriodQualityControl
        {
            get { return _periodQualityControl; }
            set
            {
                _periodQualityControl = value; 
                OnPropertyChanged();
            }
        }

        public DateTime CreateStamp
        {
            get { return _createStamp; }
            set
            {
                _createStamp = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        public override bool Save()
        {
            try
            {
                var testMethodService = Locator.Get<ITestMethodRepository>();
                var testMethod = AutoMapper.Mapper.Map<TestMethod>(this);

                testMethodService.Save(testMethod);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("TestMethod.Save({0})", Id), exception);

                return false;
            }

            return true;
        }

        public string this[string columnName]
        {
            get { return string.Empty; }
        }

        public string Error { get; private set; }
    }
}
