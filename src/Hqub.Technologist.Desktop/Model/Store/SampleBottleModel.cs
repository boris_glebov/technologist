﻿using System;
using System.ComponentModel;
using System.Data.Odbc;
using AutoMapper;
using Hqub.Technologist.Desktop.Events;
using Hqub.Technologist.Desktop.Properties;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class SampleBottleModel : EntityBase, IHasValidate
    {
        private Guid _productTestId;
        private int _order;
        private float _weight;
        private float _sampleWeight;
        private float _afterDryingWeight;
        private float _wetness;
        private RecalcEvent _recalcEvent;

        public SampleBottleModel()
        {
            _recalcEvent = AggregationEventService.Instance.GetEvent<RecalcEvent>();
        }

        #region Properties

        [Description("Ссылка на Исптыание ГП")]
        public Guid ProductTestId
        {
            get { return _productTestId; }
            set
            {
                _productTestId = value; 
                OnPropertyChanged();
            }
        }

        [Description("Номер бюкса")]
        public int Order
        {
            get { return _order; }
            set
            {
                _order = value;
                OnPropertyChanged();
                Update();
            }
        }

        [Description("масса бюкса, г.")]
        public float Weight
        {
            get { return _weight; }
            set
            {
                _weight = value;
                OnPropertyChanged();
                Update();
            }
        }

        [Description("масса навески, исх")]
        public float SampleWeight
        {
            get { return _sampleWeight; }
            set
            {
                _sampleWeight = value;
                OnPropertyChanged();
                Update();
            }
        }

        [Description("масса после сушки")]
        public float AfterDryingWeight
        {
            get { return _afterDryingWeight; }
            set
            {
                _afterDryingWeight = value;
                OnPropertyChanged();
                Update();
            }
        }

        [Description("влажность, %")]
        public float Wetness
        {
            get { return Math.TestProductMath.GetHumidity(Weight, SampleWeight, AfterDryingWeight); }
            set
            {
                _wetness = value;
                Update();
            }
        }

        #endregion

        private void Update()
        {
            System.Diagnostics.Debug.WriteLine("Update from SampleBottleModel");

            _recalcEvent.Publish(null);
            OnPropertyChanged("Wetness");
        }

        public override bool Save()
        {
            try
            {
                var sampleBottleRepository = Locator.Get<ISampleBottleRepository>();
                var sampleBottle = Mapper.Map<SampleBottle>(this);

                sampleBottleRepository.Save(sampleBottle);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("SampleBottle.Save({0})", Id), exception);

                return false;
            }

            return true;
        }

        public string this[string columnName]
        {
            get
            {
                var errorMessage = "";
                switch (columnName)
                {
                    case "Weight":
                        if (Weight < 0)
                        {
                            errorMessage = Settings.Default.ValueError;
                        }
                        break;

                    case "SampleWeight":
                        if (SampleWeight < 0)
                        {
                            errorMessage = Settings.Default.ValueError;
                        }
                        break;

                    case "AfterDryingWeight":
                        if (AfterDryingWeight < 0)
                        {
                            errorMessage = Settings.Default.ValueError;
                        }
                        break;
                }

                return errorMessage;
            }
        }

        public string Error { get; private set; }
    }
}
