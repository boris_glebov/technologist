﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class MaterialModel : EntityBase, IHasValidate
    {
        private string _name;
        private string _provider;
        private MaterialTypeEnum _materialType;
        private int? _izvestType;

        #region Properties

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value; 
                OnPropertyChanged();
            }
        }

        [Description("Поставщик")]
        public string Provider
        {
            get { return _provider; }
            set
            {
                _provider = value; 
                OnPropertyChanged();
            }
        }

        [Description("Вид материала")]
        public MaterialTypeEnum MaterialType
        {
            get { return _materialType; }
            set
            {
                _materialType = value; 
                OnPropertyChanged();
            }
        }

        public int? IzvestType
        {
            get { return _izvestType; }
            set
            {
                _izvestType = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        #region EntityBase

        public override bool Save()
        {
            try
            {
                var materialService = Locator.Get<IMaterialRepository>();
                var material = AutoMapper.Mapper.Map<Material>(this);

                materialService.Save(material);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("Material.Save({0})", Id), exception);

                return false;
            }

            return true;
        }

        #endregion

        #region Implementation IHasValidate

        public string this[string columnName]
        {
            get
            {
                var errorMessage = string.Empty;
                switch (columnName)
                {
                    case "Name":
                        if (string.IsNullOrEmpty(Name))
                            errorMessage = Properties.Settings.Default.RequiredField;
                        break;

                    case "Provider":
                    {
                        if (string.IsNullOrEmpty(Provider))
                            errorMessage = Properties.Settings.Default.RequiredField;
                    }
                        break;
                }

                return errorMessage;
            }
        }

        public string Error { get; private set; }

        #endregion
    }
}
