﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using Hqub.Technologist.Desktop.Model.Wrappers;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    [Serializable]
    public class BrandModel : EntityBase, IHasValidate
    {
        #region Fields

        private string _name;
        private string _productClasses;

        #endregion

        #region Properties

        [Description("Марка по плотности")]
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value; 
                OnPropertyChanged();
            }
        }

        [Description("Возможный класс по прочности")]
        public string ProductClasses
        {
            get
            {
                return _productClasses;
            }
            set
            {
                _productClasses = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Properties for Views

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string ProductClassesPresent
        {
            get { return ProductClasses.Replace(";", " "); }
        }

        #endregion

        #region Static

        public static List<BrandModel> List()
        {
            var brandRepository = Locator.GetBrandRepository();
         
            return AutoMapper.Mapper.Map<List<BrandModel>>(brandRepository.List());
        }

        public static BrandModel Get(Guid id)
        {
            var brandRepository = Locator.GetBrandRepository();

            return AutoMapper.Mapper.Map<BrandModel>(brandRepository.Get(id));
        }

        #endregion

        #region Methods

        public List<SelectedWrapper<string>> GetClasses()
        {
            return _productClasses.Split(';').Select(x => new SelectedWrapper<string>(x)).ToList();
        }

        #endregion

        #region Implementation EntityBase

        public override bool Save()
        {
            try
            {
                var brandService = Locator.Get<IBrandRepository>();
                var brand = AutoMapper.Mapper.Map<Brand>(this);

                brandService.Save(brand);

            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("Brand.Save({0})", this.Id), exception);

                return false;
            }

            return true;
        }

        #endregion

        #region Implementation IHasValidate

        public string this[string columnName]
        {
            get { return string.Empty; }
        }

       
        public string Error { get; private set; }

        #endregion
    }
}
