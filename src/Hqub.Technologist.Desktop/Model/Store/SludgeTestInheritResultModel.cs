﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Hqub.Technologist.Desktop.Utitlities.Extensions;
using Hqub.Technologist.Math;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    [Serializable]
    public class SludgeTestInheritResultModel: EntityBase
    {
        private DateTime _dateTest;
        private Guid _laborantId;
        private float _bedVolume;
        private float _trueDensity;
        private float _hitch;
        private float _constantDevice;
        private float _timeDrainingFluid;
        private float _ambientTemperature;

        public SludgeTestInheritResultModel()
        {
            DateTest = DateTime.Now;
        }

        #region Properties

        [Description("Дата")]
        public DateTime DateTest
        {
            get { return _dateTest; }
            set
            {
                _dateTest = value;
                OnPropertyChanged();
            }
        }

        [Description("Лаборант")]
        public Guid LaborantId
        {
            get { return _laborantId; }
            set
            {
                _laborantId = value;
                OnPropertyChanged();
            }
        }

        
        [Description("Объем слоя")]
        public float BedVolume
        {
            get { return _bedVolume; }
            set
            {
                _bedVolume = value;
                OnPropertyChanged();
            }
        }

        [Description("Истинная плотность")]
        public float TrueDensity
        {
            get { return _trueDensity; }
            set
            {
                _trueDensity = value;
                OnPropertyChanged();
            }
        }

        [Description("Навеска")]
        public float Hitch
        {
            get { return _hitch; }
            set
            {
                _hitch = value;
                OnPropertyChanged();
            }
        }

        [Description("Постоянная прибора")]
        public float ConstantDevice
        {
            get { return _constantDevice; }
            set
            {
                _constantDevice = value;
                OnPropertyChanged();
            }
        }

        [Description("Время стекания жидкости")]
        public float TimeDrainingFluid
        {
            get { return _timeDrainingFluid; }
            set
            {
                _timeDrainingFluid = value;
                OnPropertyChanged();
            }
        }

        [Description("Температура окружающей среды")]
        public float AmbientTemperature
        {
            get { return _ambientTemperature; }
            set
            {
                _ambientTemperature = value;
                OnPropertyChanged();
            }
        }

        [Description("Удельная поверхность")]
        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public float SpecificSurface
        {
            get
            {
                return TestMaterialMath.CalcSpecificSurfaceByTovarovMethod(ConstantDevice, TrueDensity, Hitch, 1f,
                    TimeDrainingFluid, BedVolume);
            }
        }
        #endregion

        #region Extend
        private string _laborantName;
        private EmployeeModel _laborantEntity;


        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string ReviewerName
        {
            get
            {
                if (_laborantName != null)

                    return _laborantName;

                var repository = Locator.GetEmployeeRepository();
                var reviwer = AutoMapper.Mapper.Map<EmployeeModel>(repository.Get(_laborantId));
                return _laborantName = reviwer != null ? reviwer.FullName : String.Empty;

            }
        }

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public EmployeeModel LaborantEntity
        {
            get
            {
                if (_laborantEntity != null)
                    return _laborantEntity;

                var repository = Locator.GetEmployeeRepository();
                return _laborantEntity = AutoMapper.Mapper.Map<EmployeeModel>(repository.Get(_laborantId));
            }
        }

        #endregion

        #region Implemented IEditable

        public override bool Save()
        {
            try
            {
                var sludgeService = Locator.Get<ISludgeTestInheritResultRepository>();
                var sludge = AutoMapper.Mapper.Map<SludgeTestInheritResult>(this);

                sludgeService.Save(sludge);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("SludgeTestInheritResultModel.Save({0})", this.SerializeObject()), exception);

                return false;
            }

            return true;
        }

        #endregion
        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string Error { get; private set; }
    }
}
