﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;
using Hqub.Technologist.Desktop.Utitlities.Extensions;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class PassportModel : EntityBase, IHasValidate
    {
        private DateTime _dateRelease;
        private string _lotNumber;
        private Guid _nomenclatureId;
        private float _strenghtLimit;
        private float _averageDensity;
        private float _wetness;
        private string _classStrenghtName;
        private float _requiredStrenght;
        private float _variationStrenght;
        private string _markName;
        private float _thermalConductivity;
        private float _dryingShrinkage;
        private string _frostResist;
        private string _specificActivity;
        private string _notes;
        private NomenclatureModel _nomenclature;
        private float _waterVaporPermeability;

        #region Properties

        [Description("Номер партии")]
        public string LotNumber
        {
            get { return _lotNumber; }
            set
            {
                _lotNumber = value;
                OnPropertyChanged();
            }
        }

        [Description("Дата выпуска")]
        public DateTime DateRelease
        {
            get { return _dateRelease; }
            set
            {
                _dateRelease = value; 
                OnPropertyChanged();
            }
        }

        [Description("Номенклатура")]
        public Guid NomenclatureId
        {
            get { return _nomenclatureId; }
            set
            {
                _nomenclatureId = value; 
                OnPropertyChanged();
            }
        }

        [Description("Прочность")]
        public float StrenghtLimit
        {
            get { return _strenghtLimit; }
            set
            {
                _strenghtLimit = value;
                OnPropertyChanged();
            }
        }

        [Description("Среняя Плотность")]
        public float AverageDensity
        {
            get { return _averageDensity; }
            set
            {
                _averageDensity = value;
                OnPropertyChanged();
            }
        }

        [Description("Влажность")]
        public float Wetness
        {
            get { return _wetness; }
            set
            {
                _wetness = value;
                OnPropertyChanged();
            }
        }

        [Description("Имя Класса")]
        public string ClassStrenghtName
        {
            get { return _classStrenghtName; }
            set
            {
                _classStrenghtName = value;
                OnPropertyChanged();
            }
        }

        [Description("Требумая прочность")]
        public float RequiredStrenght
        {
            get { return _requiredStrenght; }
            set
            {
                _requiredStrenght = value; 
                OnPropertyChanged();
            }
        }

        [Description("Вариация прочности")]
        public float VariationStrenght
        {
            get { return _variationStrenght; }
            set
            {
                _variationStrenght = value; 
                OnPropertyChanged();
            }
        }

        [Description("Имя марки")]
        public string MarkName
        {
            get { return _markName; }
            set
            {
                _markName = value; 
                OnPropertyChanged();
            }
        }

        [Description("К. Теплопроводности")]
        public float ThermalConductivity
        {
            get { return _thermalConductivity; }
            set
            {
                _thermalConductivity = value;
                OnPropertyChanged();
            }
        }

        [Description("")]
        public float DryingShrinkage
        {
            get { return _dryingShrinkage; }
            set
            {
                _dryingShrinkage = value; 
                OnPropertyChanged();
            }
        }

        [Description("Марка по морозостойкости")]
        public string FrostResist
        {
            get { return _frostResist; }
            set
            {
                _frostResist = value;
                OnPropertyChanged();
            }
        }

        [Description("Удельная эффективность")]
        public string SpecificActivity
        {
            get { return _specificActivity; }
            set
            {
                _specificActivity = value;
                OnPropertyChanged();
            }
        }

        [Description("Паропроницаемость")]
        public float WaterVaporPermeability
        {
            get { return _waterVaporPermeability; }
            set
            {
                _waterVaporPermeability = value;
                OnPropertyChanged();
            }
        }

        [Description("Примечание")]
        public string Notes
        {
            get { return _notes; }
            set
            {
                _notes = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        #region Extended

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public NomenclatureModel NomenclatureEntity
        {
            get
            {
                if (_nomenclature != null)
                    return _nomenclature;

                var repository = Locator.GetNomenclatureRepository();
                return _nomenclature = AutoMapper.Mapper.Map<NomenclatureModel>(repository.Get(NomenclatureId));
            }
        }
       
        #endregion

        public override bool Save()
        {
            try
            {
                var passportRepository = Locator.Get<IPassportRepository>();
                var position = AutoMapper.Mapper.Map<Passport>(this);

                passportRepository.Save(position);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("Passport.Save({0})", Id), exception);
                return false;
            }

            return true;
        }

        public string this[string columnName]
        {
            get { return string.Empty; }
        }

        public string Error { get; private set; }
    }
}
