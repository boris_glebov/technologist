﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Hqub.Technologist.Desktop.Properties;
using Hqub.Technologist.Desktop.Utitlities.Extensions;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    [Serializable]
    public class LotModel : EntityBase, IHasValidate
    {
        private string _number;
        private DateTime _dateReview;
        private Guid _productType;
        private int _order;
        private int _autoclaveNumber;
        private bool _isGeometrySizeAccepted;
        private string _deffectType;
        private int _deffectIntensivity;
        private string _notes;
        private Guid _reviewer;
        private Dictionary<string, string> _xExtend;
        private string _extend;

        public LotModel()
        {
            Id = Guid.NewGuid();
            DateReview = DateTime.Now;
        }

        #region Properties

        [Description("Номер партии")]
        public string LotNumber
        {
            get { return _number; }
            set
            {
                _number = value;
                OnPropertyChanged();
            }
        }

        [Description("Дата и время просмотра")]
        public DateTime DateReview
        {
            get { return _dateReview; }
            set
            {
                _dateReview = value; 
                OnPropertyChanged();
            }
        }

        [Description("Порядковый номер состава в партии")]
        public int Order
        {
            get { return _order; }
            set
            {
                _order = value;
                OnPropertyChanged();
            }
        }

        [Description("Номер автоклава")]
        public int AutoclaveNumber
        {
            get { return _autoclaveNumber; }
            set
            {
                _autoclaveNumber = value; 
                OnPropertyChanged();
            }
        }

        [Description("Соответствие геометрическим размерам")]
        public bool IsGeometrySizeAccepted
        {
            get { return _isGeometrySizeAccepted; }
            set
            {
                _isGeometrySizeAccepted = value; 
                OnPropertyChanged();
            }
        }

        [Description("Дефекты.Вид")]
        public string DeffectType
        {
            get { return _deffectType; }
            set
            {
                _deffectType = value; 
                OnPropertyChanged();
            }
        }

        [Description("Дефекты.Интенсивность")]
        public int DeffectIntensivity
        {
            get { return _deffectIntensivity; }
            set
            {
                _deffectIntensivity = value;
                OnPropertyChanged();
            }
        }

        [Description("Примечания")]
        public string Notes
        {
            get { return _notes; }
            set
            {
                _notes = value; 
                OnPropertyChanged();
            }
        }

        [Description("Осмотр провел")]
        public Guid Reviewer
        {
            get { return _reviewer; }
            set
            {
                _reviewer = value;
                OnPropertyChanged();
            }
        }

       

        [Description("Номенклатура")]
        public Guid NomenclatureId
        {
            get { return _nomenclatureId; }
            set
            {
                _nomenclatureId = value; 
                OnPropertyChanged();
            }
        }
        public string Extend
        {
            get { return _extend; }
            set
            {
                _extend = value;

                if (!string.IsNullOrEmpty(_extend))
                    XExtend = SerializeExtension.DeserializeDictionary(value);

                OnPropertyChanged();
            }
        }

        #endregion

        #region Extend

        private BrandModel _brand;
        private NomenclatureModel _nomenclature;
        private Guid _nomenclatureId;
        private string _reviewerName;
        private EmployeeModel _reviewerEntity;

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public BrandModel BrandEntity
        {
            get
            {
                if (_brand != null)
                    return _brand;

                return _brand = NomenclatureEntity.BrandEntity;
            }
        }

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public NomenclatureModel NomenclatureEntity
        {
            get
            {
                if (_nomenclature != null)
                    return _nomenclature;

                var repository = Locator.GetNomenclatureRepository();
                return _nomenclature = AutoMapper.Mapper.Map<NomenclatureModel>(repository.Get(NomenclatureId));
            }
        }

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string ReviewerName
        {
            get
            {
                if (_reviewerName != null)
                
                    return _reviewerName;

                var repository = Locator.GetEmployeeRepository();
                var reviwer = AutoMapper.Mapper.Map<EmployeeModel>(repository.Get(_reviewer));
                return _reviewerName = reviwer != null ? reviwer.FullName : String.Empty;

            }
        }

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public EmployeeModel ReviewerEntity
        {
            get
            {
                if (_reviewerEntity != null)
                    return _reviewerEntity;

                var repository = Locator.GetEmployeeRepository();
                return _reviewerEntity = AutoMapper.Mapper.Map<EmployeeModel>(repository.Get(_reviewer));
            }
        }

        public Dictionary<string, string> XExtend
        {
            get { return _xExtend; }
            set
            {
                _xExtend = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Implemented IEditable

        public override bool Save()
        {
            try
            {
                var lotService = Locator.Get<ILotRepository>();
                var lot = AutoMapper.Mapper.Map<Lot>(this);
                _extend = SerializeExtension.SerializeDictionary(XExtend);

                lotService.Save(lot);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("LotModel.Save({0})", this.SerializeObject()), exception);

                return false;
            }

            return true;
        }

        #endregion

        #region Static

        public static LotModel Create()
        {
            var fc = new LotModel();
            fc.Extend = "";

            fc.XExtend = Utitlities.DynamicEntityUtils.GetSchema(QualityControlType.QualityArray);
            return fc;
        }

        #endregion

        public string this[string columnName]
        {
            get
            {
                var errorMessage = string.Empty;

                switch (columnName)
                {
                    case "LotNumber":
                        if (string.IsNullOrEmpty(LotNumber))
                            errorMessage = Settings.Default.RequiredField;
                        break;

                   
                }

                return errorMessage;
            }
        }

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string Error { get; private set; }
    }
}
