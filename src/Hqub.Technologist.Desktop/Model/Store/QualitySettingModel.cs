﻿using System;
using Hqub.Technologist.Desktop.Properties;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class QualitySettingModel : EntityBase, IHasValidate
    {
        private string _name;
        private string _unit;
        private bool _isDefault;
        private int _minValue;
        private int _maxValue;
        private QualityControlType _controlType;
        private bool _isAnaliseInclude;
        private string _mask;
        private int _order;

        #region .ctor 

        public QualitySettingModel()
        {
            Mask = "#2.2";
        }

        #endregion


        #region Properties

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value; 
                OnPropertyChanged();
            }
        }

        public string Unit
        {
            get { return _unit; }
            set
            {
                _unit = value;
                OnPropertyChanged();
            }
        }

        public bool IsAnaliseInclude
        {
            get { return _isAnaliseInclude; }
            set
            {
                _isAnaliseInclude = value; 
                OnPropertyChanged();
            }
        }

        public bool IsDefault
        {
            get { return _isDefault; }
            set
            {
                _isDefault = value;
                OnPropertyChanged();
            }
        }

        public int MinValue
        {
            get { return _minValue; }
            set
            {
                _minValue = value;
                OnPropertyChanged();
                OnPropertyChanged("MaxValue");
            }
        }

        public int MaxValue
        {
            get { return _maxValue; }
            set
            {
                _maxValue = value;
                OnPropertyChanged();
                OnPropertyChanged("MinValue");
            }
        }

        public string Mask
        {
            get { return _mask; }
            set
            {
                _mask = value; 
                OnPropertyChanged();
            }
        }

        public QualityControlType ControlType
        {
            get { return _controlType; }
            set
            {
                _controlType = value; 
                OnPropertyChanged();
            }
        }

        public int Order
        {
            get { return _order; }
            set
            {
                _order = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public override bool Save()
        {
            try
           {
                var qualityService = Locator.Get<IQualityControlSettingsRepository>();
                var qualitySettings = AutoMapper.Mapper.Map<QualitySetting>(this);

                qualityService.Save(qualitySettings);
           }
            catch (Exception exception)
           {
             Logger.Main.ErrorException(string.Format("QualitySettingModel.Save({0})", Id), exception);
                return false;
            }

            return true;
        }

        public string this[string columnName]
        {
            get
            {
                var errorMessage = string.Empty;
                switch (columnName)
                {
                    case "Name":
                        if (string.IsNullOrEmpty(Name))
                        {
                            errorMessage = Settings.Default.RequiredField;
                        }
                        break;

//                    case "MinValue":
//                        if (MinValue > MaxValue)
//                        {
//                            errorMessage = string.Format("Значения поля должно быть меньше '{0}'", MaxValue);
//                        }
//                        break;
//                    case "MaxValue":
//                        if (MaxValue < MinValue)
//                        {
//                            errorMessage = string.Format("Значение поля должно быть больше '{0}'", MinValue);
//                        }
//                        break;
                }
                
                return errorMessage;
            }
        }

        public string Error { get; private set; }
    }
}
