﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Hqub.Technologist.Desktop.Properties;
using Hqub.Technologist.Desktop.Utitlities.Extensions;
using Hqub.Technologist.Math;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;



namespace Hqub.Technologist.Desktop.Model.Store
{
    [Serializable]
    public class CementTestInheritResultModel : EntityBase
    {
        private DateTime _dateTest;
        private Guid _laborantId;
        private float _waterConsumption;
        private TimeSpan _timeMixing;
        private TimeSpan _initialSet;
        private TimeSpan _finalSet;
        private float _bedVolume;
        private float _trueDensity;
        private float _hitch;
        private float _constantDevice;
        private float _timeDrainingFluid;
        private float _ambientTemperature;
        private DateTime _dateFormation;
        private float _durationExposure;
        private DateTime _dateAssay;
        private float _numberSample;
        private float _sizeA;
        private float _sizeB;
        private float _sizeC;
        private float _loadBend;
        private float _load;

        public CementTestInheritResultModel()
        {
            DateTest = DateTime.Now;
            DateFormation = DateTime.Now;
            DateAssay = DateTime.Now;
        }

        #region Properties

        [Description("Дата")]
        public DateTime DateTest
        {
            get { return _dateTest; }
            set
            {
                _dateTest = value;
                OnPropertyChanged();
            }
        }

        [Description("Лаборант")]
        public Guid LaborantId
        {
            get { return _laborantId; }
            set
            {
                _laborantId = value;
                OnPropertyChanged();
            }
        }

        [Description("Расход воды")]
        public float WaterConsumption
        {
            get { return _waterConsumption; }
            set
            {
                _waterConsumption = value;
                OnPropertyChanged();
            }
        }

        [Description("Время затворения")]
        public TimeSpan TimeMixing
        {
            get { return _timeMixing; }
            set
            {
                _timeMixing = value;
                OnPropertyChanged();
            }
        }

        [Description("Начало схватывания")]
        public TimeSpan InitialSet
        {
            get { return _initialSet; }
            set
            {
                _initialSet = value;
                OnPropertyChanged();
            }
        }
        [Description("Конец схватывания")]
        public TimeSpan FinalSet
        {
            get { return _finalSet; }
            set
            {
                _finalSet = value;
                OnPropertyChanged();
            }
        }

        [Description("Объем слоя")]
        public float BedVolume
        {
            get { return _bedVolume; }
            set
            {
                _bedVolume = value;
                OnPropertyChanged();
            }
        }

        [Description("Истинная плотность")]
        public float TrueDensity
        {
            get { return _trueDensity; }
            set
            {
                _trueDensity = value;
                OnPropertyChanged();
            }
        }

        [Description("Навеска")]
        public float Hitch
        {
            get { return _hitch; }
            set
            {
                _hitch = value;
                OnPropertyChanged();
            }
        }

        [Description("Постоянная прибора")]
        public float ConstantDevice
        {
            get { return _constantDevice; }
            set
            {
                _constantDevice = value;
                OnPropertyChanged();
            }
        }

        [Description("Время стекания жидкости")]
        public float TimeDrainingFluid
        {
            get { return _timeDrainingFluid; }
            set
            {
                _timeDrainingFluid = value;
                OnPropertyChanged();
            }
        }

        [Description("Температура окружающей среды")]
        public float AmbientTemperature
        {
            get { return _ambientTemperature; }
            set
            {
                _ambientTemperature = value;
                OnPropertyChanged();
            }
        }

        [Description("Дата формирования")]
        public DateTime DateFormation
        {
            get { return _dateFormation; }
            set
            {
                _dateFormation = value;
                OnPropertyChanged();
            }
        }

        [Description("Продолжительность выдержки")]
        public float DurationExposure
        {
            get { return _durationExposure; }
            set
            {
                _durationExposure = value;
                OnPropertyChanged();
            }
        }

        [Description("Дата испытания")]
        public DateTime DateAssay
        {
            get { return _dateAssay; }
            set
            {
                _dateAssay = value;
                OnPropertyChanged();
            }
        }

        [Description("№ образца")]
        public float NumberSample
        {
            get { return _numberSample; }
            set
            {
                _numberSample = value;
                OnPropertyChanged();
            }
        }
        [Description("Размер А")]
        public float SizeA
        {
            get { return _sizeA; }
            set
            {
                _sizeA = value;
                OnPropertyChanged();
            }
        }
        [Description("Размер B")]
        public float SizeB
        {
            get { return _sizeB; }
            set
            {
                _sizeB = value;
                OnPropertyChanged();
            }
        }

        [Description("Размер C")]
        public float SizeC
        {
            get { return _sizeC; }
            set
            {
                _sizeC = value;
                OnPropertyChanged();
            }
        }

        [Description("На изгиб")]
        public float LoadBend
        {
            get { return _loadBend; }
            set
            {
                _loadBend = value;
                OnPropertyChanged();
            }
        }

        [Description("На сжатие")]
        public float Load
        {
            get { return _load; }
            set
            {
                _load = value;
                OnPropertyChanged();
            }
        }

        [Description("Нормальная густота")]
        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public float NormalDensity 
        {
            get { return WaterConsumption/4; }
        }

        [Description("Начало схватывания Итого")]
        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public TimeSpan InitialSetTotal
        {
            get { return InitialSet - TimeMixing; }
        }

        [Description("Начало схватывания Итого строка")]
        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string InitialSetTotalString
        {
            get { return string.Format("{0} ч {1} мин", InitialSetTotal.Hours,InitialSetTotal.Minutes); }
        }

        [Description("Конец схватывания Итого")]
        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public TimeSpan FinalSetTotal
        {
            get { return FinalSet - TimeMixing; }
        }

        [Description("Начало схватывания Итого строка")]
        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string FinalSetTotalString
        {
            get { return string.Format("{0} ч {1} мин", FinalSetTotal.Hours, FinalSetTotal.Minutes); }
        }

        [Description("Удельная поверхность")]
        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public float SpecificSurface
        {
            get {  return TestMaterialMath.CalcSpecificSurfaceByTovarovMethod(ConstantDevice, TrueDensity, Hitch, 1f,
                    TimeDrainingFluid, BedVolume); }
        }

        [Description("Прочность при изгибе")]
        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public float LimitBend
        {
            get { return 0.0234375f*LoadBend; }
        }

        [Description("Прочность при сжатии")]
        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public float LimitCompress
        {
            get { return Load/25; }
        }

        #endregion


        #region Extend
        private string _laborantName;
        private EmployeeModel _laborantEntity;

      
        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string ReviewerName
        {
            get
            {
                if (_laborantName != null)

                    return _laborantName;

                var repository = Locator.GetEmployeeRepository();
                var reviwer = AutoMapper.Mapper.Map<EmployeeModel>(repository.Get(_laborantId));
                return _laborantName = reviwer != null ? reviwer.FullName : String.Empty;

            }
        }

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public EmployeeModel LaborantEntity
        {
            get
            {
                if (_laborantEntity != null)
                    return _laborantEntity;

                var repository = Locator.GetEmployeeRepository();
                return _laborantEntity = AutoMapper.Mapper.Map<EmployeeModel>(repository.Get(_laborantId));
            }
        }

        #endregion

        #region Implemented IEditable

        public override bool Save()
        {
            try
            {
                var cementService = Locator.Get<ICementTestInheritResultRepository>();
                var cement = AutoMapper.Mapper.Map<CementTestInheritResult>(this);

                cementService.Save(cement);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("CementTestInheritResultModel.Save({0})", this.SerializeObject()), exception);

                return false;
            }

            return true;
        }

        #endregion

       

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string Error { get; private set; }
       
    }
}
