﻿using System;
using System.Xml.Serialization;
using AutoMapper;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class EmployeeModel : EntityBase, IHasValidate
    {
        private string _firstName;
        private string _lastName;
        private string _middleName;
        private Guid _positionId;

        #region Properties

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value; 
                OnPropertyChanged();
            }
        }

        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                OnPropertyChanged();
            }
        }

        public string MiddleName
        {
            get { return _middleName; }
            set
            {
                _middleName = value; 
                OnPropertyChanged();
            }
        }

        public Guid PositionId
        {
            get { return _positionId; }
            set
            {
                _positionId = value;
                OnPropertyChanged();
            }
        }

        private Position _position;

        [XmlIgnore]
        [IgnoreMap]
        public Position PositionEntity
        {
            get { return _position = _position ?? GetPosition(PositionId); }
        }

        [XmlIgnore]
        [IgnoreMap]
        public string FullName { get { return string.Format("{0} {1} {2}", LastName, FirstName, MiddleName); } }

        #endregion

        #region Methods

        private Position GetPosition(Guid positionId)
        {
            var repository = Locator.Get<IPositionRepository>();
            return repository.Get(positionId);
        }

        #endregion

        #region Implementation EntityBase

        public override bool Save()
        {
            try
            {
                var employeeService = Locator.Get<IEmployeeRepository>();
                var employee = Mapper.Map<Employee>(this);

                employeeService.Save(employee);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("Employee.Save({0})", Id), exception);

                return false;
            }

            return true;
        }

        #endregion

        #region Implementation IHasValidate

        public string this[string columnName]
        {
            get
            {
                const string fieldRequired = "Поле обязательно к заполнению";

                var errorMessage = string.Empty;

                switch (columnName)
                {
                    case "FirstName":
                        if (string.IsNullOrEmpty(FirstName))
                            errorMessage = fieldRequired;
                        break;
                    case "LastName":
                        if (string.IsNullOrEmpty(LastName))
                            errorMessage = fieldRequired;
                        break;
                    case "MiddleName":
                        if (string.IsNullOrEmpty(MiddleName))
                            errorMessage = fieldRequired;
                        break;
                    case "PositionId":
                        if (PositionId == Guid.Empty)
                            errorMessage = fieldRequired;
                        break;
                }

                return errorMessage;
            }
        }

        [XmlIgnore]
        [IgnoreMap]
        public string Error { get; private set; }

        #endregion
    }
}
