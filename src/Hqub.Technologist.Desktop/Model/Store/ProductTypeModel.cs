﻿using System;
using Hqub.Technologist.Desktop.Properties;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class ProductTypeModel : EntityBase, IHasValidate
    {
        private string _name;
        private string _notes;

        #region Properties

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value; 
                OnPropertyChanged();
            }
        }

        public string Note
        {
            get { return _notes; }
            set
            {
                _notes = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        public override bool Save()
        {
            try
            {
                var productTypeService = Locator.Get<IProductTypeRepository>();
                var productType = AutoMapper.Mapper.Map<ProductType>(this);

                productTypeService.Save(productType);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("ProductTypeModel.Save error"), exception);
                return false;
            }

            return true;
        }

        public string this[string columnName]
        {
            get
            {
                var errorMessage = string.Empty;
                if (columnName == "Name" && string.IsNullOrEmpty(Name))
                {
                    errorMessage = Settings.Default.RequiredField;
                }

                return errorMessage;
            }
        }

        public string Error { get; private set; }
    }
}
