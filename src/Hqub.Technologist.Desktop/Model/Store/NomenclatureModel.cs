﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Hqub.Technologist.Desktop.Properties;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class NomenclatureModel : EntityBase, IHasValidate
    {
        private string _productName;
        private Guid _brandDensity;
        private float _length;
        private float _height;
        private float _width;
        private float _palletVolume;
        private float _arrayVolume;
        private bool _isGroove;
        private string _notes;
        private BrandModel _brandEntity;
        private Guid _productTypeId;
        private ProductTypeModel _productTypeEntity;

        #region Properties

        [Description("Наименование")]
        public string ProductName
        {
            get { return _productName; }
            set
            {
                _productName = value;
                OnPropertyChanged();
            }
        }

        [Description("Вид продукта")]
        public Guid ProductTypeId
        {
            get { return _productTypeId; }
            set
            {
                _productTypeId = value;
                OnPropertyChanged();
            }
        }

        [Description("Плотность")]
        public Guid BrandDensity
        {
            get { return _brandDensity; }
            set
            {
                _brandDensity = value;
                OnPropertyChanged();
            }
        }

        [Description("Длина")]
        public float Length
        {
            get { return _length; }
            set
            {
                _length = value; 
                OnPropertyChanged();
            }
        }

        [Description("Высота")]
        public float Height
        {
            get { return _height; }
            set
            {
                _height = value;
                OnPropertyChanged();
            }
        }

        [Description("Ширина")]
        public float Width
        {
            get { return _width; }
            set
            {
                _width = value;
                OnPropertyChanged();
            }
        }

        [Description("Объем поддона")]
        public float PalletVolume
        {
            get { return _palletVolume; }
            set
            {
                _palletVolume = value; 
                OnPropertyChanged();
            }
        }

        [Description("Объем массива")]
        public float ArrayVolume
        {
            get { return _arrayVolume; }
            set
            {
                _arrayVolume = value;
                OnPropertyChanged();
            }
        }

        [Description("Наличие паза/гребня")]
        public bool IsGroove
        {
            get { return _isGroove; }
            set
            {
                _isGroove = value; 
                OnPropertyChanged();
            }
        }

        [Description("Примечание")]
        public string Notes
        {
            get { return _notes; }
            set
            {
                _notes = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        #region Extend

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public BrandModel BrandEntity
        {
            get
            {
                if (_brandEntity != null)
                    return _brandEntity;

                var brandRepsoitory = Locator.GetBrandRepository();
                _brandEntity = AutoMapper.Mapper.Map<BrandModel>(brandRepsoitory.Get(BrandDensity));

                return _brandEntity;
            }
        }

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public ProductTypeModel ProductTypeEntity
        {
            get
            {
                if (_productTypeEntity != null)
                    return _productTypeEntity;

                var productTypeRepository = Locator.GetProductTypeRepository();
                _productTypeEntity = AutoMapper.Mapper.Map<ProductTypeModel>(productTypeRepository.Get(ProductTypeId));

                return _productTypeEntity;
            }
        }

        #endregion

        #region Static

        public static List<NomenclatureModel> List()
        {
            var nomenclatureRepository = Locator.GetNomenclatureRepository();

            return AutoMapper.Mapper.Map<List<NomenclatureModel>>(nomenclatureRepository.List());
        }

        #endregion

        #region Implementation IHasValidate

        public override bool Save()
        {
            try
            {
                var nomenclatureService = Locator.Get<INomenclatureRepository>();
                var nomenclature = AutoMapper.Mapper.Map<Nomenclature>(this);

                nomenclatureService.Save(nomenclature);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format(""), exception);
                return false;
            }

            return true;
        }

        public string this[string columnName]
        {
            get
            {
                var errorMessage = string.Empty;

                switch (columnName)
                {
                    case "ProductName":
                        errorMessage = string.IsNullOrEmpty(ProductName) ? Settings.Default.RequiredField : string.Empty;

                        if (errorMessage == String.Empty)
                        {
                            var repository = Locator.GetNomenclatureRepository();
                            errorMessage = repository.Exists(Id, ProductName) ? "Номенклатура с таким именем продукта уже существует." : String.Empty;
                        }
                        
                        break;
                    case "ProductTypeId":
                        errorMessage = ProductTypeId == Guid.Empty ? Settings.Default.RequiredField : string.Empty;
                        break;
                    case "BrandDensity":
                        errorMessage = BrandDensity == Guid.Empty ? Settings.Default.RequiredField : string.Empty;
                        break;
                }

                return errorMessage;
            }
        }

        [XmlIgnore]
        [AutoMapper.IgnoreMap]
        public string Error { get; private set; }

        #endregion
    }
}
