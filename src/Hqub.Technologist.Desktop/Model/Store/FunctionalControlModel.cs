﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using AutoMapper;
using Hqub.Technologist.Desktop.Utitlities.Extensions;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class FunctionalControlModel : EntityBase, IHasValidate
    {
        private DateTime _dateTest;
        private Guid _employeeId;
        private string _extend;
        private EmployeeModel _employeeEntity;
        private Dictionary<string, string> _xExtend;

        public FunctionalControlModel()
        {
            Id = Guid.NewGuid();
        }

        #region Properties

        public DateTime DateTest
        {
            get { return _dateTest; }
            set
            {
                _dateTest = value; 
                OnPropertyChanged();
            }
        }

        public Guid EmployeeId
        {
            get { return _employeeId; }
            set
            {
                _employeeId = value;
                OnPropertyChanged();
            }
        }

        public string Extend
        {
            get { return _extend; }
            set
            {
                _extend = value;

                if (!string.IsNullOrEmpty(_extend))
                    XExtend = SerializeExtension.DeserializeDictionary(value);

                OnPropertyChanged();
            }
        }

        #endregion

        #region Static

        public static FunctionalControlModel Create()
        {
            var fc = new FunctionalControlModel();
            fc.DateTest = DateTime.Now;
            fc.Extend = "";

            fc.XExtend = Utitlities.DynamicEntityUtils.GetSchema(QualityControlType.FunctionalControl);
            return fc;
        }

        #endregion

        #region Extend

        public Dictionary<string, string> XExtend
        {
            get { return _xExtend; }
            set
            {
                _xExtend = value;
                OnPropertyChanged();
            }
        }

        public EmployeeModel EmployeeEntity
        {
            get
            {
                if (_employeeEntity != null)
                    return _employeeEntity;

                var employeeRepository = Locator.GetEmployeeRepository();
                return _employeeEntity = Mapper.Map<EmployeeModel>(employeeRepository.Get(EmployeeId));
            }
        }

        #endregion

        public override bool Save()
        {
            try
            {
                var functionalControlRepository = Locator.Get<IFunctionControlRepository>();
                _extend = SerializeExtension.SerializeDictionary(XExtend);

                var brand = Mapper.Map<FunctionalControl>(this);
                functionalControlRepository.Save(brand);

            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("Brand.Save({0})", this.Id), exception);

                return false;
            }

            return true;
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "EmployeeId" && EmployeeId == Guid.Empty)
                {
                    return "Требуется выбрать проверяющего сотрудника.";
                }

                return String.Empty;
            }
        }

        public string Error { get; private set; }
    }
}
