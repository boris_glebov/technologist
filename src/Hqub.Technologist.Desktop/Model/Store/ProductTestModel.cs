﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using AutoMapper;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class ProductTestModel : EntityBase, IHasValidate
    {
        private Guid _lot;
        private DateTime _testDate;
        private Guid _user;
        private float _wetDensityAverage;
        private float _dryDensityAverage;
        private Guid _markDensity;
        private float _densityVariation;
        private float _markStrenght;
        private float _strenghtVariation;
        private float _strenghtLimitAverage;
        private bool _isAccepted;
        private float _strenghtRequired;
        private float _densityRequired;
        private string _markNameRequired;
        private string _classNameRequired;

        public ProductTestModel()
        {
            TestDate = DateTime.Now;
        }

        #region Properties

        [Description("Партия")]
        public Guid Lot
        {
            get { return _lot; }
            set
            {
                _lot = value;
                OnPropertyChanged();
            }
        }

        [Description("Дата проведения испытаний")]
        public DateTime TestDate
        {
            get { return _testDate; }
            set
            {
                _testDate = value;
                OnPropertyChanged();
            }
        }

        [Description("Пользователь")]
        public Guid User
        {
            get { return _user; }
            set
            {
                _user = value;
                OnPropertyChanged();
            }
        }

        [Description("срде. зн. плотности (вл.)")]
        public float WetDensityAverage
        {
            get { return _wetDensityAverage; }
            set
            {
                _wetDensityAverage = value;
                OnPropertyChanged();
            }
        }

        [Description("сред. зн. плотности (сух.)")]
        public float DryDensityAverage
        {
            get { return _dryDensityAverage; }
            set
            {
                _dryDensityAverage = value;
                OnPropertyChanged();
            }
        }

        [Description("марка по плотности")]
        public Guid MarkDensity
        {
            get { return _markDensity; }
            set
            {
                _markDensity = value;
                OnPropertyChanged();
            }
        }

        [Description("текущий коэф. вариации в партии (плотность)")]
        public float DensityVariation
        {
            get { return _densityVariation; }
            set
            {
                _densityVariation = value;
                OnPropertyChanged();
            }
        }

        [Description("Класс прочности")]
        public float MarkStrenght
        {
            get { return _markStrenght; }
            set
            {
                _markStrenght = value;
                OnPropertyChanged();
            }
        }

        [Description("текущий коэф. вариации в партии (прочности)")]
        public float StrenghtVariation
        {
            get { return _strenghtVariation; }
            set
            {
                _strenghtVariation = value;
                OnPropertyChanged();
            }
        }

        [Description("текущий коэф. вариации в партии (прочности)")]
        public float StrenghtLimitAverage
        {
            get { return _strenghtLimitAverage; }
            set
            {
                _strenghtLimitAverage = value;
                OnPropertyChanged();
            }
        }

        [Description("принять текущий коэф. вариации")]
        public bool IsAccepted
        {
            get { return _isAccepted; }
            set
            {
                _isAccepted = value; 
                OnPropertyChanged();
            }
        }

        [Description("Требумая прочность")]
        public float StrenghtRequired
        {
            get { return _strenghtRequired; }
            set
            {
                _strenghtRequired = value; 
                OnPropertyChanged();
            }
        }

        [Description("Влажность")]
        public float Wetness
        {
            get { return _wetness; }
            set
            {
                _wetness = value; 
                OnPropertyChanged();
            }
        }

        [Description("Требумая плотность")]
        public float DensityRequired
        {
            get { return _densityRequired; }
            set
            {
                _densityRequired = value; 
                OnPropertyChanged();
            }
        }

        [Description("Имя марки")]
        public string MarkNameRequired
        {
            get { return _markNameRequired; }
            set
            {
                _markNameRequired = value; 
                OnPropertyChanged();
            }
        }

        [Description("Имя класса")]
        public string ClassNameRequired
        {
            get { return _classNameRequired; }
            set
            {
                _classNameRequired = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Extend

        [XmlIgnore]
        [IgnoreMap]
        public List<SampleModel> SampleModels
        {
            get
            {
                var repository = Locator.GetSampleRepository();
                var samples = Mapper.Map<List<SampleModel>>(repository.Where(x => x.ProductTestId == Id).OrderBy(x=>x.Order).ToList()); 
                
                return samples;
            }
        }

        [XmlIgnore]
        [IgnoreMap]
        public List<SampleBottle> SampleBottles
        {
            get
            {
                var repository = Locator.GetSampleBottleRepository();
                var sampleBottles = Mapper.Map<List<SampleBottle>>(repository.Where(x => x.ProductTestId == Id).ToList());

                return sampleBottles;
            }
        }

        private LotModel _selectedLot;

        [XmlIgnore]
        [IgnoreMap]
        public LotModel SelectedLot
        {
            get
            {
                if (_selectedLot != null)
                    return _selectedLot;

                var repository = Locator.GetLotRepository();
                return _selectedLot = Mapper.Map<LotModel>(repository.First(x => x.Id == Lot));
            }
        }

        private ProductCharacterModel _selectedCharacter;
        private float _wetness;

        [XmlIgnore]
        [IgnoreMap]
        public ProductCharacterModel SelectedCharacter
        {
            get
            {
                if (_selectedCharacter != null)
                    return _selectedCharacter;

                var repository = Locator.GetProductCharacterRepository();
                return
                    _selectedCharacter =
                        Mapper.Map<ProductCharacterModel>(
                            repository.FirstOrDefault(x => x.ProductDensity == SelectedLot.BrandEntity.Id));
            }
        }

        [XmlIgnore]
        [IgnoreMap]
        public EmployeeModel SelectedEmployee
        {
            get
            {
                var repository = Locator.GetEmployeeRepository();
                var employee = Mapper.Map<EmployeeModel>(repository.First(x => x.Id == User));

                return employee;
            }
        }

        #endregion

        #region Implementation EntityBase

        public override bool Save()
        {
            try
            {
                var productTestService = Locator.Get<IProductTestRepository>();
                var productTest = Mapper.Map<ProductTest>(this);

                productTestService.Save(productTest);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("ProductTest.Save({0})", Id), exception);

                return false;
            }

            return true;
        }


        public bool Save(List<SampleModel> samples, List<SampleBottleModel> sampleBottles)
        {
            try
            {
                var productTestService = Locator.Get<IProductTestRepository>();
                var productTest = Mapper.Map<ProductTest>(this);

                productTestService.Save(productTest, Mapper.Map<List<Sample>>(samples),
                    Mapper.Map<List<SampleBottle>>(sampleBottles));
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("ProductTest.Save({0})", Id), exception);

                return false;
            }

            return true;
        }

        #endregion

        #region Implementation IHasValidate

        public string this[string columnName]
        {
            get
            {
                const string fieldRequired = "Поле обязательно к заполнению";

                var errorMessage = string.Empty;

                switch (columnName)
                {
                       case "User":
                        if (User == Guid.Empty)
                            errorMessage = fieldRequired;
                        break;
                }

                return errorMessage;
            }
        }

        [XmlIgnore]
        [IgnoreMap]
        public string Error { get; private set; }

        #endregion

    }
}
