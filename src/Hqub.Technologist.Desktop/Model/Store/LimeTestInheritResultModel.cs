﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Xml.Serialization;
using AutoMapper;
using Hqub.Technologist.Desktop.Properties;
using Hqub.Technologist.Desktop.Utitlities.Extensions;
using Hqub.Technologist.Math;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Desktop.Model.Store
{
    public class LimeTestInheritResultModel : EntityBase, IHasValidate
    {
        #region Fields

        private DateTime _dateTest;
        private string _selectionPlace;
        private string _manufacture;
        private float _volume;
        private float _titreMgO;
        private float _layerVolume;
        private float _density;
        private float _deviceConstant;
        private float _drainFluidTime;
        private float _hitch;
        private float _airTemp;
        private Guid _laborantId;
        private EmployeeModel _laborant;
        private float _titre;
        private float _weight;
        private float _volumeCaMg;
        private float _volumeCa;
        private float _weightMgO;
        private string _limingSteps;

        #endregion

        #region .ctor

        public LimeTestInheritResultModel()
        {
            DateTest = DateTime.Now;
        }

        #endregion

        #region Properties

        [Description("Дата проведения испытаний")]
        public DateTime DateTest
        {
            get { return _dateTest; }
            set
            {
                _dateTest = value; 
                OnPropertyChanged();
            }
        }

        [Description("Место отбора")]
        public string SelectionPlace
        {
            get { return _selectionPlace; }
            set
            {
                _selectionPlace = value;
                OnPropertyChanged();
            }
        }

        [Description("Производитель")]
        public string Manufacture
        {
            get { return _manufacture; }
            set
            {
                _manufacture = value;
                OnPropertyChanged();
            }
        }

        #region Определение CaO + MgO

        [Description("Обем HCI")]
        public float Volume
        {
            get { return _volume; }
            set
            {
                _volume = value;
                OnPropertyChanged();
            }
        }

        [Description("Титр CaO")]
        public float Titre
        {
            get { return _titre; }
            set
            {
                _titre = value; 
                OnPropertyChanged();
            }
        }

        [Description("Масса навески")]
        public float Weight
        {
            get { return _weight; }
            set
            {
                _weight = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        #region Определение MgO

        [Description("Объем трилона Б  CaO+MgO")]
        public float VolumeCaMg
        {
            get { return _volumeCaMg; }
            set
            {
                _volumeCaMg = value;
                OnPropertyChanged();
            }
        }

        [Description("Объем трилона Б CaO")]
        public float VolumeCa
        {
            get { return _volumeCa; }
            set
            {
                _volumeCa = value;
                OnPropertyChanged();
            }
        }

        [Description("Титр CaO")]
        public float TitreMgO
        {
            get { return _titreMgO; }
            set
            {
                _titreMgO = value; 
                OnPropertyChanged();
            }
        }

        [Description("Масса навески MgO")]
        public float WeightMgO
        {
            get { return _weightMgO; }
            set
            {
                _weightMgO = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Оценка гашения извести

        public string LimingSteps
        {
            get { return _limingSteps; }
            set
            {
                _limingSteps = value; 
                OnPropertyChanged();
            }
        }

        private int? _limingTime;
        [Description("Время гашения извести")]
        public int LimingTime
        {
            get
            {
                var settings = Locator.GetTestMethodRepository();
                var entity = settings.Get();

                if (entity.SlakedLimeMethod == (int) SlakedLimeMethodType.T60)
                    return  (_limingTime = 60).Value;
                
                // Определяем время гашения по ГОСТу
                var temperatures = LimeStepsExtend.Select(x => float.Parse(x.Value, CultureInfo.InvariantCulture)).ToList();
                var maxTemp = temperatures.Max();
                var maxTempIdx = temperatures.IndexOf(maxTemp);
                    
                return (_limingTime = int.Parse(LimeStepsExtend.ElementAt(maxTempIdx).Id)).Value;
            }

        }

        private float? _limingTemp;
        [Description("Температура гашения")]
        public float LimingTemp
        {
            get
            {
                if (_limingTemp.HasValue)
                    return _limingTemp.Value;

                var settings = Locator.GetTestMethodRepository();
                var entity = settings.Get();

                if (entity.SlakedLimeMethod == (int) SlakedLimeMethodType.T60)
                {
                    var tempItem = LimeStepsExtend.First(x => x.Id == "60");
                    return (_limingTemp = float.Parse(tempItem.Value, CultureInfo.InvariantCulture)).Value;
                }

                // Определяем время гашения по ГОСТу
                var temperatures = LimeStepsExtend.Select(x => float.Parse(x.Value, CultureInfo.InvariantCulture)).ToList();
                var maxTemp = temperatures.Max();

                return (_limingTemp = maxTemp).Value;
            }
        }

        #endregion

        #region Оценка удельной теплоемкости

        [Description("Объем слоя")]
        public float LayerVolume
        {
            get { return _layerVolume; }
            set
            {
                _layerVolume = value; 
                OnPropertyChanged();
            }
        }

        [Description("Истинная плотность")]
        public float Density
        {
            get { return _density; }
            set
            {
                _density = value;
                OnPropertyChanged();
            }
        }

        [Description("Постоянная прибора")]
        public float DeviceConstant
        {
            get { return _deviceConstant; }
            set
            {
                _deviceConstant = value;
                OnPropertyChanged();
            }
        }

        [Description("Время стекания жидкости")]
        public float DrainFluidTime
        {
            get { return _drainFluidTime; }
            set
            {
                _drainFluidTime = value;
                OnPropertyChanged();
            }
        }

        [Description("Навеска")]
        public float Hitch
        {
            get { return _hitch; }
            set
            {
                _hitch = value;
                OnPropertyChanged();
            }
        }

        [Description("Температура воздуха")]
        public float AirTemp
        {
            get { return _airTemp; }
            set
            {
                _airTemp = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        [Description("Лаборант")]
        public Guid LaborantId
        {
            get { return _laborantId; }
            set
            {
                _laborantId = value; 
                OnPropertyChanged();
            }
        }

        [Description("Содержание MgO")]
        [XmlIgnore]
        [IgnoreMap]
        public float ContentsMgO
        {
            get
            {
                return TestMaterialMath.CalcContentsMgO(VolumeCaMg, VolumeCa, TitreMgO, WeightMgO);
            }
        }

        [Description("Содержание CaO")]
        [XmlIgnore]
        [IgnoreMap]
        public float ContentsCaO
        {
            get
            {
                return TestMaterialMath.CalcContentsCaO(Volume, Titre, Weight);
            }
        }

        [Description("Удельная поверхность")]
        [XmlIgnore]
        [IgnoreMap]
        public float SpecificSurface
        {
            get
            {
                return TestMaterialMath.CalcSpecificSurfaceByTovarovMethod(DeviceConstant,Density, Hitch, 1f,
                    DrainFluidTime, LayerVolume);
                
            }
        }

        #endregion

        #region Extend

        private ObservableCollection<Item> _limeStepsExtend;
        private bool _isChecked;

        [XmlIgnore]
        [IgnoreMap]
        public IzvestType IzvestType
        {
            get
            {
                var repository = Locator.GetMaterialRepository();
                return repository.GetIzvestType(Manufacture);
            }
        }

        [XmlIgnore]
        [IgnoreMap]
        public ObservableCollection<Item> LimeStepsExtend
        {
            get
            {
                if (_limeStepsExtend != null)
                    return _limeStepsExtend;

                if (string.IsNullOrEmpty(LimingSteps))
                {
                    _limeStepsExtend = new ObservableCollection<Item>
                    {
                        new Item("30", "0,0"),
                        new Item("40", "0,0"),
                        new Item("50", "0,0"),
                        new Item("60", "0,0"),
                        new Item("70", "0,0"),
                        new Item("80", "0,0"),
                        new Item("100", "0,0"),
                        new Item("120", "0,0"),
                        new Item("140", "0,0"),
                        new Item("160", "0,0"),
                        new Item("180", "0,0"),
                        new Item("200", "0,0"),
                        new Item("220", "0,0"),
                        new Item("240", "0,0"),
                        new Item("260", "0,0"),
                        new Item("280", "0,0"),
                        new Item("300", "0,0"),
                    };
                }
                else
                {
                    _limeStepsExtend = new ObservableCollection<Item>();
                    foreach (var item in SerializeExtension.DeserializeDictionary(LimingSteps))
                    {
                        _limeStepsExtend.Add(new Item(item.Key, item.Value));
                    }
                }

                return _limeStepsExtend;
            }
        }

        [XmlIgnore]
        [IgnoreMap]
        public EmployeeModel Laborant
        {
            get
            {
                if (_laborant != null)
                    return _laborant;

                var repository = Locator.GetEmployeeRepository();
                return
                    _laborant = Mapper.Map<EmployeeModel>(repository.FirstOrDefault(x => x.Id == LaborantId));
            }
        }

        [XmlIgnore]
        [IgnoreMap]
        public bool IsChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        public override bool Save()
        {
            try
            {
                var d = LimeStepsExtend.ToDictionary(item => item.Id, item => item.Value);
                _limingSteps = SerializeExtension.SerializeDictionary(d);

                var repository = Locator.Get<ILimeTestInheritResultRepository>();
                var limeTestResult = Mapper.Map<LimeTestInheritResult>(this);

                repository.Save(limeTestResult);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("LimeTestInheritResultModel.Save({0})", Id), exception);

                return false;
            }

            return true;
        }

        public string this[string columnName]
        {
            get
            {
                var errorMessage = String.Empty;

                switch (columnName)
                {
                    case "LaborantId":
                        if (LaborantId == Guid.Empty)
                            errorMessage = Properties.Settings.Default.RequiredField;
                        break;
                    case "Manufacture":
                        if (string.IsNullOrEmpty(Manufacture))
                            errorMessage = Settings.Default.RequiredField;
                        break;
                    case "SelectionPlace":
                        if (string.IsNullOrEmpty(SelectionPlace))
                            errorMessage = Settings.Default.RequiredField;
                        break;
                }

                return errorMessage;
            }
        }

        public string Error { get; private set; }
    }
}
