﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using Hqub.Technologist.Desktop.Model.Store;

namespace Hqub.Technologist.Desktop.Model
{
    [Serializable]
    public abstract class EntityBase : ICloneable, INotifyPropertyChanged, ISaver, IHasId
    {

        protected List<string> AllowBlocks;

        protected EntityBase()
        {
            Id = Guid.NewGuid();
            AllowBlocks = new List<string>();
        }

        #region Implemented IEditable
        
        public abstract bool Save();

        #endregion

        #region Implemented INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Implemented ICloneable

        public object Clone()
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                
                formatter.Serialize(ms, this);
                ms.Position = 0;

                return formatter.Deserialize(ms);
            }
        }

        #endregion

        public Guid Id { get; set; }
    }
}
