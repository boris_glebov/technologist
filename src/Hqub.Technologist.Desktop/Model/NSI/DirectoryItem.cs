﻿namespace Hqub.Technologist.Desktop.Model.NSI
{
    public class DirectoryItem<T> : Notifyable
    {
        private T _value;
        private string _name;

        public DirectoryItem(string name, T value)
        {
            Name = name;
            Value = value;
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value; 
                OnPropertyChanged();
            }
        }

        public T Value
        {
            get { return _value; }
            set
            {
                _value = value; 
                OnPropertyChanged();
            }
        }
    }
}
