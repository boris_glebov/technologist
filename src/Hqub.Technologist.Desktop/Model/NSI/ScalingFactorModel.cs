﻿using Hqub.Technologist.Store;


namespace Hqub.Technologist.Desktop.Model.NSI
{
    public class ScalingFactorModel : Notifyable
    {
        private SampleType _value;
        private float _factor;

        public ScalingFactorModel(SampleType value, float factor)
        {
            Value = value;
            Factor = factor;
        }

        public SampleType Value
        {
            get { return _value; }
            set
            {
                _value = value; 
                OnPropertyChanged();
            }
        }

        public float Factor
        {
            get { return _factor; }
            set
            {
                _factor = value; 
                OnPropertyChanged();
            }
        }

    }
}
