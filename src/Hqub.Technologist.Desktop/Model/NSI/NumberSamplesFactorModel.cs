﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Technologist.Desktop.Model.NSI
{
    public class NumberSamplesFactorModel: Notifyable
    {
        private int _n;
        private float _factor;

        public NumberSamplesFactorModel(int n, float factor)
        {
            N = n;
            Factor = factor;
        }

        public int N
        {
            get { return _n; }
            set
            {
                _n = value; 
                OnPropertyChanged();
            }
        }

        public float Factor
        {
            get { return _factor; }
            set
            {
                _factor = value; 
                OnPropertyChanged();
            }
        }
    }
}
