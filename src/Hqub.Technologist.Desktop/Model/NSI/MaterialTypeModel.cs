﻿using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.Model.NSI
{
    public class MaterialTypeModel : DirectoryItem<MaterialTypeEnum>
    {
        public MaterialTypeModel(string name, MaterialTypeEnum materialType) : base(name, materialType)
        {
        }
    }
}
