﻿
namespace Hqub.Technologist.Desktop.Model.NSI
{
    public  class CorrectionFactorModel : Notifyable
    {
        private int _density;
        private float _factor;

        public CorrectionFactorModel(int density, float factor)
        {
            Density = density;
            Factor = factor;
        }

        public int Density
        {
            get { return _density; }
            set
            {
                _density = value; 
                OnPropertyChanged();
            }
        }

        public float Factor
        {
            get { return _factor; }
            set
            {
                _factor = value; 
                OnPropertyChanged();
            }
        }
    }
}
