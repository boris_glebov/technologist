﻿using System;
using System.Collections.Generic;

namespace Hqub.Technologist.Desktop.Model.Navigation
{
    public class NavigationItemModel : Notifyable
    {
        private string _imagePath;
        private string _title;
        private string _description;

        public string ImagePath
        {
            get { return _imagePath; }
            set
            {
                _imagePath = value; 
                OnPropertyChanged();
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }

        public Action GoToAction { get; set; }
    }

    public class NavigationModel : Notifyable
    {
        private string _name;
        private List<NavigationItemModel> _items;

        public NavigationModel()
        {
            Items = new List<NavigationItemModel>();
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public List<NavigationItemModel> Items
        {
            get { return _items; }
            set
            {
                _items = value; 
                OnPropertyChanged();
            }
        }
    }
}
