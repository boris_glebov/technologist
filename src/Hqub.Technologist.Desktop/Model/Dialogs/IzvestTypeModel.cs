﻿using Hqub.Technologist.Store;

namespace Hqub.Technologist.Desktop.Model.Dialogs
{
    public class IzvestTypeModel : Notifyable
    {
        private string _name;
        private IzvestType _izvestType;

        public IzvestTypeModel(string name, IzvestType type)
        {
            Name = name;
            IzvestType = type;
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        public IzvestType IzvestType
        {
            get { return _izvestType; }
            set
            {
                _izvestType = value;
                OnPropertyChanged();
            }
        }
    }
}
