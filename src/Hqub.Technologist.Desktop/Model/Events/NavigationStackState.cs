﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Desktop.Modules;

namespace Hqub.Technologist.Desktop.Model.Events
{
    public class NavigationStackState
    {
        private readonly List<BaseNavigationModule> _modules; 

        public NavigationStackState(List<BaseNavigationModule> modules)
        {
            _modules = modules;
        }

        public List<BaseNavigationModule> Modules
        {
            get { return _modules; }
        } 
    }
}
