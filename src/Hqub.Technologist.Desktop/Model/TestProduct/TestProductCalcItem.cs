﻿using System.ComponentModel;

namespace Hqub.Technologist.Desktop.Model.TestProduct
{
    public class TestProductCalcItem : Notifyable
    {
        private int _order;
        private float _dryDensity;
        private float _wetDensity;

        private string _markByDensity;
        private float _strenghtLimit;

        private float _classByStrength;
        private float _currentCoefficientStrengthVariation;

        [Description("Номер образца")]
        public int Order
        {
            get { return _order; }
            set
            {
                _order = value;
                OnPropertyChanged();
            }
        }

        [Description("Плотность, кг/м.куб влажн.")]
        public float WetDensity
        {
            get { return _wetDensity; }
            set
            {
                _wetDensity = value;
                OnPropertyChanged();
            }
        }

        [Description("Плотность, кг/м куб сух.")]
        public float DryDensity
        {
            get { return _dryDensity; }
            set
            {
                _dryDensity = value;
                OnPropertyChanged();
            }
        }

        [Description("Марка по плотности")]
        public string MarkByDensity
        {
            get { return _markByDensity; }
            set
            {
                _markByDensity = value;
                OnPropertyChanged();
            }
        }

        [Description("Предел прочности при сжатии кг/см. кв")]
        public float StrenghtLimit
        {
            get { return _strenghtLimit; }
            set
            {
                _strenghtLimit = value;
                OnPropertyChanged();
            }
        }

        [Description("Класс по прочности")]
        public float ClassByStrength
        {
            get { return _classByStrength; }
            set
            {
                _classByStrength = value; 
                OnPropertyChanged();
            }
        }

        [Description("Текущий коэф. вариации в партии (плот.)")]
        public float CurrentCoefficientDensityVariation
        {
            get { return _currentCoefficientStrengthVariation; }
            set
            {
                _currentCoefficientStrengthVariation = value;
                OnPropertyChanged();
            }
        }

        [Description("Текущий коэф. вариации в партии (прочн.)")]
        public float CurrentCoefficientStrengthVariation
        {
            get { return _currentCoefficientStrengthVariation; }
            set
            {
                _currentCoefficientStrengthVariation = value; 
                OnPropertyChanged();
            }
        }
    }
}
