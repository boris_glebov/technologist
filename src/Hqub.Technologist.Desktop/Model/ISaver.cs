﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Technologist.Desktop.Model
{
    public interface ISaver
    {
        bool Save();
    }
}
