﻿using Hqub.Technologist.Desktop.Model.Store;

namespace Hqub.Technologist.Desktop.Model.Wrappers
{
    public class ReportProductTestWrapper : Notifyable
    {
        private ProductTestModel _product;
        private LotModel _lot;
        private ProductCharacterModel _productCharacter;

        #region .ctor


        #endregion

        #region Properties

        public ProductTestModel Product
        {
            get { return _product; }
            set
            {
                _product = value;
                OnPropertyChanged();
            }
        }

        public LotModel Lot
        {
            get { return _lot; }
            set
            {
                _lot = value;
                OnPropertyChanged();
            }
        }

        public ProductCharacterModel ProductCharacter
        {
            get { return _productCharacter; }
            set
            {
                _productCharacter = value;
                OnPropertyChanged();
            }
        }

        #endregion
    }
}
