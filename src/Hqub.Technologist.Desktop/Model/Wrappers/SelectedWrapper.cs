﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Technologist.Desktop.Model.Wrappers
{
    public class SelectedWrapper<T> : BasePropertyChanged
    {
        public SelectedWrapper(T val)
        {
            Value = val;
        }

        public T Value { get; set; }
        public bool IsSelected { get; set; }
    }
}
