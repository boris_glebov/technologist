﻿using System;

namespace Hqub.Technologist.Desktop.Model
{
    public interface IHasId
    {
        Guid Id { get; set; }
    }
}
