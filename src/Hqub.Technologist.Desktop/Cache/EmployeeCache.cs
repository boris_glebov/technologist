﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Desktop.Model.Store;

namespace Hqub.Technologist.Desktop.Cache
{
    public static class EmployeeCache
    {
        private static List<EmployeeModel> _employee;

        public static List<EmployeeModel> List(bool reload = false)
        {
            if (_employee != null && !reload)
            {
                return _employee;
            }

            var repository = Locator.GetEmployeeRepository();

            return _employee = AutoMapper.Mapper.Map<List<EmployeeModel>>(repository.List());
        }

        public static EmployeeModel Get(Guid id)
        {
            if (_employee == null)
                _employee = List();

            return _employee.FirstOrDefault(e => e.Id == id);
        }
    }
}
