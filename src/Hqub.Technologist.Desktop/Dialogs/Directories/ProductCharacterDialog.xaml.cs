﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store.Repository.Interfaces;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.Dialogs.Directories
{
    /// <summary>
    /// Interaction logic for ProductCharacterDialog.xaml
    /// </summary>
    public partial class ProductCharacterDialog : BaseEditDialog
    {
        private ProductCharacterModel _model;
        private List<BrandModel> _brands;
        private BrandModel _selectedBrand;

        private IProductCharacterRepository _productCharacterRepository;

        public ProductCharacterDialog()
        {
            InitializeComponent();

            var brandRepsoitory = Locator.GetBrandRepository();
            Brands = AutoMapper.Mapper.Map<List<BrandModel>>(brandRepsoitory.List());

            _productCharacterRepository = Locator.GetProductCharacterRepository();
        }

        public override void SetModel(EntityBase model)
        {
            if (model == null) return;

            Model = (ProductCharacterModel) model;
            SetSelectedBrand(Brands.FirstOrDefault(x => x.Id == Model.ProductDensity));
        }

        private void LoadModel(Guid mark)
        {
            Model = AutoMapper.Mapper.Map<ProductCharacterModel>(_productCharacterRepository.GetByMark(mark));
            if (Model != null) return;

            Model = new ProductCharacterModel {ProductDensity = SelectedBrand.Id};
        }

        private void SetSelectedBrand(BrandModel brand)
        {
            _selectedBrand = brand;
            OnPropertyChanged("SelectedBrand");
            OnPropertyChanged("IsEditAllow");
        }

        #region Commands

        public ICommand SaveCommand { get { return new DelegateCommand(SaveCommandExecute); } }

        private void SaveCommandExecute()
        {
            Save(Model);
        }

        #endregion

        #region Properties

        public List<BrandModel> Brands
        {
            get { return _brands; }
            set
            {
                _brands = value;
                OnPropertyChanged();
            }
        }

        public BrandModel SelectedBrand
        {
            get { return _selectedBrand; }
            set
            {
                _selectedBrand = value;
                LoadModel(value.Id);

                OnPropertyChanged();
                OnPropertyChanged("IsEditAllow");
            }
        }

        public ProductCharacterModel Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged();
            }
        }

        public bool IsEditAllow
        {
            get { return SelectedBrand != null; }
            set
            {
                
            }
        }

        #endregion
    }
}
