﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.Dialogs.Directories
{
    /// <summary>
    /// Interaction logic for EmployeeEntityDialog.xaml
    /// </summary>
    public partial class EmployeeEntityDialog : BaseEditDialog
    {
        private EmployeeModel _model;
        private List<Position> _positions;
        private Position _selectedPosition;

        public EmployeeEntityDialog()
        {
            InitializeComponent();
        }

        public override void SetModel(EntityBase model)
        {
            Model = model == null ? new EmployeeModel() : (EmployeeModel)model;

            LoadEmployees();
        }

        private void LoadEmployees()
        {
            var positionRepository = Locator.Get<IPositionRepository>();
            Positions = new List<Position>(positionRepository.List());

            SelectedPosition = Positions.FirstOrDefault(e => e.Id == Model.PositionId);
        }

        protected override void Validate()
        {
            base.Validate();

            if (Model.PositionId == Guid.Empty)
            {
                AddError("Должность", "Поле обязательно к заполнению");
            }
        }

        #region Commands

        public ICommand SaveCommand { get { return new DelegateCommand(SaveCommandExecute);} }

        private void SaveCommandExecute()
        {
            Save(Model);
        }

        #endregion

        #region Properties

        public List<Position> Positions
        {
            get { return _positions; }
            set
            {
                _positions = value; 
                OnPropertyChanged();
            }
        }

        public Position SelectedPosition
        {
            get { return _selectedPosition; }
            set
            {
                _selectedPosition = value;
                if (value != null)
                    Model.PositionId = value.Id;
                OnPropertyChanged();
            }
        }

        public EmployeeModel Model
        {
            get { return _model; }
            set
            {
                _model = value; 
                OnPropertyChanged();
            }
        }

        #endregion
    }
}
