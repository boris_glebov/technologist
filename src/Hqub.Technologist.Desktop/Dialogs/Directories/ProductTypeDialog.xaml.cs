﻿using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model;
using Hqub.Technologist.Desktop.Model.Store;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.Dialogs.Directories
{
    /// <summary>
    /// Interaction logic for ProductTypeDialog.xaml
    /// </summary>
    public partial class ProductTypeDialog : BaseEditDialog
    {
        private ProductTypeModel _model;

        public ProductTypeDialog()
        {
            InitializeComponent();
        }

        public override void SetModel(EntityBase model)
        {
            Model = model == null ? new ProductTypeModel() : (ProductTypeModel)model;
        }

        #region Commands

        public ICommand SaveCommand { get { return new DelegateCommand(SaveCommandExecute); } }

        private void SaveCommandExecute()
        {
            Save(Model);
        }

        #endregion

        #region Properties

        public ProductTypeModel Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged();
            }
        }

        #endregion
    }
}
