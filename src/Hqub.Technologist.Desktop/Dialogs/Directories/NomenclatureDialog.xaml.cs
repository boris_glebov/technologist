﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model;
using Hqub.Technologist.Desktop.Model.Store;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.Dialogs.Directories
{
    /// <summary>
    /// Interaction logic for NomenclatureDialog.xaml
    /// </summary>
    public partial class NomenclatureDialog : BaseEditDialog
    {
        private List<BrandModel> _brands;
        private BrandModel _selectedBrand;
        private NomenclatureModel _model;
        private List<ProductTypeModel> _productTypes;
        private ProductTypeModel _selectedProductType;

        public NomenclatureDialog()
        {
            InitializeComponent();

            var brandRepsoitory = Locator.GetBrandRepository();
            Brands = AutoMapper.Mapper.Map<List<BrandModel>>(brandRepsoitory.List());

            var productTypeRepository = Locator.GetProductTypeRepository();
            ProductTypes = AutoMapper.Mapper.Map<List<ProductTypeModel>>(productTypeRepository.List());
        }

        public override void SetModel(EntityBase model)
        {
            Model = model == null ? new NomenclatureModel() : (NomenclatureModel)model;

            SelectedBrand = Brands.FirstOrDefault(x => x.Id == Model.BrandDensity);
            SelectedProductType = ProductTypes.FirstOrDefault(x => x.Id == Model.ProductTypeId);
        }

        #region Methods

        protected override void Validate()
        {
            Errors.Clear();

            if (SelectedBrand == null)
            {
                AddError("Плотность", "Значение поля не выбрано");
            }

            if (SelectedProductType == null)
            {
                AddError("Вид номенклатуры", "Значение поля не выбрано");
            }
        }

        #endregion

        #region Commands

        public ICommand SaveCommand { get { return new DelegateCommand(SaveCommandExecute); } }

        private void SaveCommandExecute()
        {
            Save(Model);
        }

        #endregion

        #region Properties

        public List<BrandModel> Brands
        {
            get { return _brands; }
            set
            {
                _brands = value;
                OnPropertyChanged();
            }
        }

        public BrandModel SelectedBrand
        {
            get { return _selectedBrand; }
            set
            {
                _selectedBrand = value;
                if (value != null)
                    Model.BrandDensity = value.Id;

                OnPropertyChanged();
            }
        }

        public List<ProductTypeModel> ProductTypes
        {
            get { return _productTypes; }
            set
            {
                _productTypes = value;
                OnPropertyChanged();
            }
        }

        public ProductTypeModel SelectedProductType
        {
            get { return _selectedProductType; }
            set
            {
                _selectedProductType = value;
                if (value != null)
                    Model.ProductTypeId = value.Id;

                OnPropertyChanged();
            }
        }

        public NomenclatureModel Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged();
            }
        }

        public bool IsEditAllow
        {
            get { return SelectedBrand != null; }
            set
            {

            }
        }

        #endregion
    }
}
