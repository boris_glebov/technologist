﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store;
using Hqub.Technologist.Store.Repository.Interfaces;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.Dialogs.Manufacture
{
    /// <summary>
    /// Interaction logic for AssessmentQualityEntityDialog.xaml
    /// </summary>
    public partial class AssessmentQualityEntityDialog : BaseEditDialog
    {
        private LotModel _model;
        private List<BrandModel> _brands;
        private BrandModel _selectedBrand;
        private List<NomenclatureModel> _nomenclatures;
        private NomenclatureModel _selectedNomenclature;
        private List<EmployeeModel> _employees;
        private EmployeeModel _selectedemployee;
        private IEmployeeRepository _employeeRepository;

        public AssessmentQualityEntityDialog()
        {
            InitializeComponent();
        }

        public override void SetModel(EntityBase model)
        {
            Model = model == null ? new LotModel() : (LotModel) model;
            
            Load();
        }

        public void Load()
        {
            LoadNomenclatures();
            LoadEmployees();
        }

   
        private void LoadNomenclatures()
        {
            Nomenclatures = NomenclatureModel.List();
            SelectedNomenclature = Nomenclatures.FirstOrDefault(x => x.Id == Model.NomenclatureId);
        }

        private void LoadEmployees()
        {
            _employeeRepository = Locator.GetEmployeeRepository();
            Employees =
                    new List<EmployeeModel>(AutoMapper.Mapper.Map<List<EmployeeModel>>(_employeeRepository.List()));

            SelectedEmployee = Employees.FirstOrDefault(x => x.Id == Model.Reviewer);
        }
        

        #region Commands

        public ICommand SaveCommand { get { return new DelegateCommand(SaveCommandExecute); } }

        protected override void Validate()
        {
            base.Validate();

            if (Model.NomenclatureId == Guid.Empty)
            {
                AddError("Вид продукта", "Обязательно к заполнению.");
            }

            if (Model.Reviewer == Guid.Empty)
            {
                AddError("Оценку провел", "Обязательно к заполнению.");
            }

        }

        private void SaveCommandExecute()
        {
            Save(Model);
        }

        #endregion

        #region Properties

        public LotModel Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged();
            }
        }

        public List<NomenclatureModel> Nomenclatures
        {
            get { return _nomenclatures; }
            set
            {
                _nomenclatures = value; 
                OnPropertyChanged();
            }
        }

        public List<EmployeeModel> Employees
        {
            get { return _employees ; }
            set
            {
                _employees = value;
                OnPropertyChanged();
            }
        }

        public NomenclatureModel SelectedNomenclature
        {
            get { return _selectedNomenclature; }
            set
            {
                _selectedNomenclature = value;
                if (_selectedNomenclature != null)
                    Model.NomenclatureId = _selectedNomenclature.Id;

                OnPropertyChanged();
            }
        }
        public EmployeeModel SelectedEmployee
        {
            get { return _selectedemployee; }
            set
            {
                _selectedemployee = value;
                if (_selectedemployee != null)
                    Model.Reviewer = _selectedemployee.Id;

                OnPropertyChanged();
            }
        }

        #endregion
    }
}
