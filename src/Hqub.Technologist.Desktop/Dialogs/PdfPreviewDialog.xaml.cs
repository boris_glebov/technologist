﻿using System.IO;
using System.Windows;
using Telerik.Windows.Documents.Fixed;

namespace Hqub.Technologist.Desktop.Dialogs
{
    /// <summary>
    /// Interaction logic for PdfPreviewDialog.xaml
    /// </summary>
    public partial class PdfPreviewDialog : Window
    {
        public PdfPreviewDialog(Stream fileStream)
        {
            InitializeComponent();

            PdfViewer.DocumentSource = new PdfDocumentSource(fileStream);
        }

        private void Print(object sender, RoutedEventArgs e)
        {
            PdfViewer.Print();
        }
    }
}
