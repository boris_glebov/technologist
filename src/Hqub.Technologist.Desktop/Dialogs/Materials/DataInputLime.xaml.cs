﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Desktop.Utitlities.Extensions;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.Dialogs.Materials
{
    /// <summary>
    /// Interaction logic for DataInputLime.xaml
    /// </summary>
    public partial class DataInputLime : BaseEditDialog
    {
        private List<EmployeeModel> _employees;
        private EmployeeModel _selectedEmployee;
        private LimeTestInheritResultModel _model;
        private List<string> _places;
        private ObservableCollection<Item> _liming;
        private List<string> _manufactures;

        public DataInputLime()
        {
            InitializeComponent();
        }

        public override void SetModel(EntityBase model)
        {
            Model = model == null ? new LimeTestInheritResultModel() : (LimeTestInheritResultModel) model;

            LoadEmployees();
            LoadPlaces();
            LoadManufactures();

            Liming = Model.LimeStepsExtend;
        }

        private void LoadEmployees()
        {
            var repository = Locator.GetEmployeeRepository();
            Employees = AutoMapper.Mapper.Map<List<EmployeeModel>>(repository.List());

            SelectedEmployee = Employees.FirstOrDefault(e => e.Id == Model.LaborantId);
        }

        private void LoadPlaces()
        {
            Places = NSI.PlacesDirectory.GetPlaces();
        }

        private void LoadManufactures()
        {
            Manufactures = Locator.GetMaterialRepository().GetIzvestManufatories();
        }

        #region Commands

        protected override void Validate()
        {
            base.Validate();

            if (Model.LaborantId == Guid.Empty)
            {
                AddError("Лаборант", "Поле обязательно к заполнению");
            }

            if (string.IsNullOrEmpty(Model.SelectionPlace))
            {
                AddError("Место отбора", "Поле обязательно к заполнению");
            }

            if (string.IsNullOrEmpty(Model.Manufacture))
            {
                AddError("Производитель", "Поле обязательно к заполнению");
            }
        }

        public ICommand SaveCommand { get { return new DelegateCommand(SaveCommandExecute); } }

        private void SaveCommandExecute()
        {
            Save(Model);
        }

        public ICommand AddLimingCommand { get { return new DelegateCommand(AddLimingCommandExecute); } }

        private void AddLimingCommandExecute()
        {
            Liming.Add(new Item(int.Parse(Liming[Liming.Count - 1].Id) + 20, 0.0f));
        }

        public ICommand RemoveLimingCommand { get { return new DelegateCommand(RemoveLimingCommandExecute);} }

        private void RemoveLimingCommandExecute()
        {
            var last = Liming.LastOrDefault();
            if (last == null || last.Id == "300")
                return;

            Liming.RemoveAt(Liming.Count - 1);
        }

        #endregion

        #region Properties

        public LimeTestInheritResultModel Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged();
            }
        }

        public List<EmployeeModel> Employees
        {
            get { return _employees; }
            set
            {
                _employees = value; 
                OnPropertyChanged();
            }
        }

        public EmployeeModel SelectedEmployee
        {
            get { return _selectedEmployee; }
            set
            {
                _selectedEmployee = value;
                if(value != null)
                    Model.LaborantId = value.Id; 

                OnPropertyChanged();
            }
        }

        public List<string> Places
        {
            get { return _places; }
            set
            {
                _places = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<Item> Liming
        {
            get { return _liming; }
            set
            {
                _liming = value;
                OnPropertyChanged();
            }
        }

        public List<string> Manufactures
        {
            get { return _manufactures; }
            set
            {
                _manufactures = value;
                OnPropertyChanged();
            }
        }

        #endregion
    }
}
