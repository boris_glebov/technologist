﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store.Repository.Interfaces;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.Dialogs.Materials
{
    /// <summary>
    /// Interaction logic for DataInputSludge.xaml
    /// </summary>
    public partial class DataInputSludge : BaseEditDialog
    {
        private List<EmployeeModel> _employees;
        private EmployeeModel _selectedemployee;
        private IEmployeeRepository _employeeRepository;
        private SludgeTestInheritResultModel _model;

        public DataInputSludge()
        {
            InitializeComponent();
        }
        public override void SetModel(EntityBase model)
        {
            Model = model == null ? new SludgeTestInheritResultModel() : (SludgeTestInheritResultModel)model;

            Load();
        }
        public void Load()
        {
            LoadEmployees();
        }

        private void LoadEmployees()
        {
            _employeeRepository = Locator.GetEmployeeRepository();
            Employees =
                    new List<EmployeeModel>(AutoMapper.Mapper.Map<List<EmployeeModel>>(_employeeRepository.List()));

            SelectedEmployee = Employees.FirstOrDefault(x => x.Id == Model.LaborantId);
        }

        #region Properties

        public SludgeTestInheritResultModel Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged();
            }
        }

        public List<EmployeeModel> Employees
        {
            get { return _employees; }
            set
            {
                _employees = value;
                OnPropertyChanged();
            }
        }
        public EmployeeModel SelectedEmployee
        {
            get { return _selectedemployee; }
            set
            {
                _selectedemployee = value;
                if (_selectedemployee != null)
                    Model.LaborantId = _selectedemployee.Id;

                OnPropertyChanged();
            }
        }
        #endregion

        #region Commands

        public ICommand SaveCommand { get { return new DelegateCommand(SaveCommandExecute); } }

        protected override void Validate()
        {
            base.Validate();


            if (Model.LaborantId == Guid.Empty)
            {
                AddError("Лаборант", "Обязательно к заполнению.");
            }

        }

        private void SaveCommandExecute()
        {
            Save(Model);
        }

        #endregion
    }
}
