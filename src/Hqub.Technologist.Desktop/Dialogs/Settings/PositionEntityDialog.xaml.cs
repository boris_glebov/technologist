﻿using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model;
using Hqub.Technologist.Desktop.Model.Store;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.Dialogs.Settings
{
    /// <summary>
    /// Interaction logic for PositionEntityDialog.xaml
    /// </summary>
    public partial class PositionEntityDialog : BaseEditDialog
    {
        #region Fields

        private PositionModel _model;

        #endregion

        #region .ctor

        public PositionEntityDialog()
        {
            InitializeComponent();
        }

        #endregion

        #region Commands

        public ICommand SaveCommand
        {
            get { return new DelegateCommand(SaveCommandExecute); }
        }

        private void SaveCommandExecute()
        {
            Save(Model);
        }

        #endregion

        #region Methods

        public override void SetModel(EntityBase model)
        {
            Model = model == null ? new PositionModel() : (PositionModel)model;
        }

        #endregion

        #region Properties

        public PositionModel Model
        {
            get { return _model; }
            set
            {
                _model = value;
                OnPropertyChanged();
            }
        }

        #endregion
    }
}

