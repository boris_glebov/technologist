﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model;
using Hqub.Technologist.Desktop.Model.NSI;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Technologist.Desktop.Dialogs.Settings
{
    /// <summary>
    /// Interaction logic for QualitySettingEntityDialog.xaml
    /// </summary>
    public partial class QualitySettingEntityDialog : BaseEditDialog
    {
        private QualitySettingModel _model;
        private DirectoryItem<QualityControlType> _selectedControlType;
        private ObservableCollection<DirectoryItem<QualityControlType>> _controlTypes;
        private QualityControlType _qualityControlType;

        public QualitySettingEntityDialog()
        {
            InitializeComponent();
        }

        public QualitySettingEntityDialog(QualityControlType qualityControlType) : this()
        {
            _qualityControlType = qualityControlType;
        }

        public override void SetModel(EntityBase model)
        {
            IsNew = model == null;
            Model = (QualitySettingModel) model ?? new QualitySettingModel();
            Model.ControlType = _qualityControlType;

            Load();
        }

        private void Load()
        {
            ControlTypes = new ObservableCollection<DirectoryItem<QualityControlType>>(NSI.QualityControlTypeDirectory.GetQualityControlTypes());
            SelectedControlType = ControlTypes.FirstOrDefault(x => x.Value == Model.ControlType);
        }

        #region Properties

        public QualitySettingModel Model
        {
            get { return _model; }
            set
            {
                _model = value; 
                OnPropertyChanged();
            }
        }

        public DirectoryItem<QualityControlType> SelectedControlType
        {
            get { return _selectedControlType; }
            set
            {
                _selectedControlType = value;
                Model.ControlType = value.Value;

                OnPropertyChanged();
            }
        }
        public ObservableCollection<DirectoryItem<QualityControlType>> ControlTypes
        {
            get { return _controlTypes; }
            set
            {
                _controlTypes = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        #region Commands

        public ICommand SaveCommand
        {
            get { return new DelegateCommand(SaveCommandExecute);}
        }

        private void SaveCommandExecute()
        {
            Save(Model);
        }

        #endregion
    }
}
