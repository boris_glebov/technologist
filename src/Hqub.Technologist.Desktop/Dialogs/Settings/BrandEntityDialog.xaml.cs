﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Desktop.Model.Wrappers;
using JetBrains.Annotations;
using Microsoft.Practices.Prism.Commands;
using Telerik.Windows.Controls.Data;

namespace Hqub.Technologist.Desktop.Dialogs.Settings
{
    /// <summary>
    /// Interaction logic for BrandEntityDialog.xaml
    /// </summary>
    public partial class BrandEntityDialog : BaseEditDialog
    {
        #region Fields

        private readonly List<string> _marks = new List<string>
        {
            "D200",
            "D300",
            "D350",
            "D400",
            "D500",
            "D600",
            "D700",
            "D800"
        };

        private readonly List<string> _classes = new List<string>
        {
            "B1,5",
            "B2,0",
            "B2,5",
            "B3,5",
            "B5,0",
            "B7,0"
        };

        private  List<SelectedWrapper<string>> _markWrappers;
        private  List<SelectedWrapper<string>> _classesWrappers; 

        private SelectedWrapper<string> _selectedMark;

        #endregion

        #region .ctor

        public BrandEntityDialog()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        public override void SetModel(EntityBase model)
        {
            Marks = _marks.Select(m => new SelectedWrapper<string>(m)).ToList();
            Classes = _classes.Select(m => new SelectedWrapper<string>(m)).ToList();

            if (model == null)
            {
                IsNew = true;
                Model = new BrandModel();
            }
            else
            {
                Model = (BrandModel) model.Clone();
                SelectedMark = Marks.Find(x => x.Value == Model.Name);

                foreach (var markClass in Model.GetClasses().Select(m => Classes.Find(x => x.Value == m.Value)))
                {
                    markClass.IsSelected = true;
                }

                IsNew = false;
            }
        }

        protected override void Validate()
        {
            Errors.Clear();

            if (SelectedMark == null || !SelectedMark.IsSelected)
            {
                AddError("Марка по плотности", "Значение поля не выбрано");
            }

            if (!Classes.Any(x => x.IsSelected))
            {
                AddError("Класс по прочности", "Значение поля не выбрано");
            }
        }

        protected override void BeforeSave()
        {
            base.BeforeSave(); 
            FillClasses();
        }

        protected override void AfterSave()
        {
            Utitlities.StatisticHelper.Update();
        }

        #endregion

        #region Commands

        public ICommand SaveCommand
        {
            get { return new DelegateCommand(SaveCommandExecute); }
        }

        private void SaveCommandExecute()
        {
            Save(Model);
        }

        private void FillClasses()
        {
            var selectedClasses = Classes.Where(x => x.IsSelected);
            Model.ProductClasses = string.Join(";", selectedClasses.Select(x=>x.Value));
        }

        #endregion

        #region Properties

        public List<SelectedWrapper<string>> Marks
        {
            get { return _markWrappers; }
            set
            {
                _markWrappers = value;
                OnPropertyChanged();
            }
        }

        public List<SelectedWrapper<string>> Classes
        {
            get { return _classesWrappers; }
            set
            {
                _classesWrappers = value;
                OnPropertyChanged();
            }
        }

        public BrandModel Model { get; set; }

        public SelectedWrapper<string> SelectedMark
        {
            get { return _selectedMark; }
            set
            {
                _selectedMark = value;
                Model.Name = _selectedMark.Value;

                OnPropertyChanged();
            }
        } 

        #endregion
    }
}
