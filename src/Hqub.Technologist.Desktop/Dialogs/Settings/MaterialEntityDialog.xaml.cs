﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Hqub.Technologist.Desktop.Controls;
using Hqub.Technologist.Desktop.Model;
using Hqub.Technologist.Desktop.Model.Dialogs;
using Hqub.Technologist.Desktop.Model.NSI;
using Hqub.Technologist.Desktop.Model.Store;
using Hqub.Technologist.Store;
using Microsoft.Practices.Prism.Commands;
using Telerik.Windows.Controls.Data;

namespace Hqub.Technologist.Desktop.Dialogs.Settings
{
    /// <summary>
    /// Interaction logic for MaterialEntityDialog.xaml
    /// </summary>
    public partial class MaterialEntityDialog : BaseEditDialog
    {
        private MaterialTypeModel _selectedMaterialType;
        private ObservableCollection<MaterialTypeModel> _materialTypeModels;
        private MaterialModel _model;

        private ObservableCollection<IzvestTypeModel> _izvestTypeModels;
        private IzvestTypeModel _selectedIzvestType;

        public MaterialEntityDialog()
        {
            InitializeComponent();

            DataContext = this;
        }

        public override void SetModel(EntityBase model)
        {
            SetNewFlag(model);
            Model = model == null ? new MaterialModel() : (MaterialModel)model;
            MaterialTypeModels = new ObservableCollection<MaterialTypeModel>(NSI.MaterialTypeDirectory.GetMaterialTypes());
            IzvestTypeModels = new ObservableCollection<IzvestTypeModel>
            {
                new IzvestTypeModel("Кальцевая", IzvestType.Calcium),
                new IzvestTypeModel("Магнивая", IzvestType.Magnium)
            };

            if (IsNew)
            {
                SelectedMaterialType = MaterialTypeModels.First();
            }
            else
            {
                SelectedMaterialType =
                    MaterialTypeModels.First(x => x.Value == (MaterialTypeEnum) Model.MaterialType);
                SelectedIzvestType = IzvestTypeModels.FirstOrDefault(x => x.IzvestType == (IzvestType?) Model.IzvestType);
            }
        }

        #region Methods

        protected override void BeforeSave()
        {
            if (SelectedMaterialType.Value != MaterialTypeEnum.Izvest && Model.IzvestType != null)
            {
                Model.IzvestType = null;
            }
        }

        protected override void Validate()
        {
            base.Validate();

            if (SelectedMaterialType.Value == MaterialTypeEnum.Izvest && Model.IzvestType == null)
            {
                AddError("Примечание", "Для извести не обходимо указать к какому виду она относится.");
            }
        }

        private void UpdateMaterialName()
        {
            if (IsNew)
                Model.Name = SelectedMaterialType.Name;
        }

        private void SetNewFlag(EntityBase model)
        {
            IsNew = model == null;
        }

        #endregion

        #region Properties

        public MaterialModel Model
        {
            get { return _model; }
            set
            {
                _model = value; 
                OnPropertyChanged();
            }
        }

        public MaterialTypeModel SelectedMaterialType
        {
            get { return _selectedMaterialType; }
            set
            {
                _selectedMaterialType = value;

                if (value != null)
                    Model.MaterialType = value.Value;

                UpdateMaterialName();

                OnPropertyChanged();
                OnPropertyChanged("IsIzvest");
            }
        }

        public ObservableCollection<MaterialTypeModel> MaterialTypeModels
        {
            get { return _materialTypeModels; }
            set
            {
                _materialTypeModels = value; 
                OnPropertyChanged();
            }
        }

        public IzvestTypeModel SelectedIzvestType
        {
            get { return _selectedIzvestType; }
            set
            {
                _selectedIzvestType = value;
                if (value != null)
                    Model.IzvestType = (int) value.IzvestType;

                OnPropertyChanged();
            }
        }

        public ObservableCollection<IzvestTypeModel> IzvestTypeModels
        {
            get { return _izvestTypeModels; }
            set
            {
                _izvestTypeModels = value; 
                OnPropertyChanged();
            }
        }

        public bool IsIzvest
        {
            get
            {
                if (SelectedMaterialType == null)
                    return false;

                return SelectedMaterialType.Value == MaterialTypeEnum.Izvest;
            }
        }

        #endregion

        #region Commands

        public ICommand SaveCommand { get { return new DelegateCommand(SaveCommandExecute); } }
        private void SaveCommandExecute()
        {
            Save(Model);
        }

        #endregion
    }
}
