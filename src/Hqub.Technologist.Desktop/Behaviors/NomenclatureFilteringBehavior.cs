﻿using System.Collections.Generic;
using System.Linq;
using Hqub.Technologist.Desktop.Model.Store;
using Telerik.Windows.Controls;

namespace Hqub.Technologist.Desktop.Behaviors
{
    public class NomenclatureFilteringBehavior : ComboBoxFilteringBehavior
    {
        public override List<int> FindMatchingIndexes(string searchText)
        {
            return
                ComboBox.Items.OfType<NomenclatureModel>()
                    .Where(i => i.ProductName.Contains(searchText))
                    .Select(i => this.ComboBox.Items.IndexOf(i))
                    .ToList();
        }
    }
}
