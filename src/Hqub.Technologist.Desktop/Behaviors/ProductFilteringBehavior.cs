﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Desktop.Model.Store;
using Telerik.Windows.Controls;

namespace Hqub.Technologist.Desktop.Behaviors
{
    public class ProductFilteringBehavior : ComboBoxFilteringBehavior
    {
        public override List<int> FindMatchingIndexes(string searchText)
        {
            return
                ComboBox.Items.OfType<ProductTestModel>()
                    .Where(i => i.SelectedLot.LotNumber.Contains(searchText))
                    .Select(i => this.ComboBox.Items.IndexOf(i))
                    .ToList();
        }
    }
}
