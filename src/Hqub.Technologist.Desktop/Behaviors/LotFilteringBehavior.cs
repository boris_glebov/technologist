﻿using System.Collections.Generic;
using System.Linq;
using Hqub.Technologist.Desktop.Model.Store;
using Telerik.Windows.Controls;

namespace Hqub.Technologist.Desktop.Behaviors
{
    public class LotFilteringBehavior : ComboBoxFilteringBehavior
    {
        public override List<int> FindMatchingIndexes(string searchText)
        {
            return this.ComboBox.Items.OfType<LotModel>().Where(i => i.LotNumber.Contains(searchText)).Select(i => this.ComboBox.Items.IndexOf(i)).ToList();
        }
    }
    
}
