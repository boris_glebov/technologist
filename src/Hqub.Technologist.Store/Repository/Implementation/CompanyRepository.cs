﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class CompanyRepository : ICompanyRepository
    {
        public Company Get(Guid id)
        {
            using (var contex = Context.GetDb())
            {
                return contex.Companies.First(c => c.Id == id);
            }
        }

        public List<Company> List()
        {
            using (var context = Context.GetDb())
            {
                return context.Companies.OrderBy(c => c.Name).ToList();
            }
        }

        public List<Company> List(int count, int offset = 0)
        {
            using (var context = Context.GetDb())
            {
                return context.Companies.OrderBy(c => c.Name).Skip(offset).Take(count).ToList();
            }
        }

        public List<Company> Where(Func<Company, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.Companies.Where(expression).ToList();
            }
        }

        public Company First(Func<Company, bool> expression)
        {
            return Where(expression).First();
        }

        public Company FirstOrDefault(Func<Company, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public Company Create(Company entity)
        {
            using (var context = Context.GetDb())
            {
                context.Companies.Add(entity);
                context.SaveChanges();

                return entity;
            }
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public void Delete(Company entity)
        {
            using (var context = Context.GetDb())
            {
                context.Companies.Attach(entity);
                context.Companies.Remove(entity);
                context.SaveChanges();
            }
        }

        public Company Save(Company entity)
        {
            using (var context = Context.GetDb())
            {
                context.Companies.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
