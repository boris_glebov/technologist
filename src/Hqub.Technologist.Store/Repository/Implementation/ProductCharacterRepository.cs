﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class ProductCharacterRepository : IProductCharacterRepository
    {
        public ProductCharacter GetByMark(Guid mark)
        {
            using (var context = Context.GetDb())
            {
                return
                    context.ProductCharacters.Where(x => x.ProductDensity == mark)
                        .OrderByDescending(x => x.CreateStamp)
                        .FirstOrDefault();
            }
        }

        public ProductCharacter Get()
        {
            using (var context = Context.GetDb())
            {
                return context.ProductCharacters.OrderByDescending(x => x.CreateStamp).FirstOrDefault();
            }
        }

        public ProductCharacter Get(Guid id)
        {
            return First(m => m.Id == id);
        }

        public List<ProductCharacter> List()
        {
            using (var context = Context.GetDb())
            {
                var groups = context.ProductCharacters.GroupBy(x => x.ProductDensity);
                var productCharacters =
                    groups.Select(@group => @group.OrderByDescending(x => x.CreateStamp).FirstOrDefault()).ToList();

                return productCharacters;
            }
        }

        public List<ProductCharacter> List(int count, int offset)
        {
            using (var context = Context.GetDb())
            {
                return context.ProductCharacters.Skip(offset).Take(count).ToList();
            }
        }

        public List<ProductCharacter> Where(Func<ProductCharacter, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.ProductCharacters.Where(expression).ToList();
            }
        }

        public ProductCharacter First(Func<ProductCharacter, bool> expression)
        {
            return Where(expression).First();
        }

        public ProductCharacter FirstOrDefault(Func<ProductCharacter, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(ProductCharacter entity)
        {
            using (var context = Context.GetDb())
            {
                context.ProductCharacters.Attach(entity);
                context.ProductCharacters.Remove(entity);
                context.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public ProductCharacter Save(ProductCharacter entity)
        {
            using (var context = Context.GetDb())
            {
                context.ProductCharacters.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
