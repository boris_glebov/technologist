﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class SlimeTestInheritResultRepository : ISlimeTestInheritResultRepository
    {
        public SlimeTestInheritResult Get(Guid id)
        {
            return FirstOrDefault(e => e.Id==id);
        }

        public void Delete(SlimeTestInheritResult entity)
        {
            using (var context = Context.GetDb())
            {
                context.SlimeTestInheritResults.Attach(entity);
                context.SlimeTestInheritResults.Remove(entity);
                context.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public List<SlimeTestInheritResult> List()
        {
            using (var context = Context.GetDb())
            {
                return context.SlimeTestInheritResults.OrderBy(c => c.DateTest).ToList();
            }
        }

        public List<SlimeTestInheritResult> List(int count, int offset)
        {
            using (var context = Context.GetDb())
            {
                return context.SlimeTestInheritResults.OrderBy(c => c.DateTest).Skip(offset).Take(count).ToList();
            }
        }

        public List<SlimeTestInheritResult> Where(Func<SlimeTestInheritResult, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.SlimeTestInheritResults.Where(expression).ToList();
            }
        }

        public SlimeTestInheritResult First(Func<SlimeTestInheritResult, bool> expression)
        {
            return Where(expression).First();
        }

        public SlimeTestInheritResult FirstOrDefault(Func<SlimeTestInheritResult, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }


        public SlimeTestInheritResult Save(SlimeTestInheritResult entity)
        {
            using (var context = Context.GetDb())
            {
                context.SlimeTestInheritResults.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
