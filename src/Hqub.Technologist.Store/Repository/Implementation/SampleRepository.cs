﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class SampleRepository : ISampleRepository
    {
        public Sample Get(Guid id)
        {
            return First(e => e.Id == id);
        }

        public List<Sample> List()
        {
            using (var context = Context.GetDb())
            {
                return context.Samples.ToList();
            }
        }

        public List<Sample> List(int count, int offset = 0)
        {
            using (var context = Context.GetDb())
            {
                return context.Samples.Skip(offset).Take(count).ToList();
            }
        }

        public List<Sample> Where(Func<Sample, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.Samples.Where(expression).ToList();
            }
        }

        public Sample First(Func<Sample, bool> expression)
        {
            return Where(expression).First();
        }

        public Sample FirstOrDefault(Func<Sample, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public void Delete(Sample entity)
        {
            using (var context = Context.GetDb())
            {
                context.Samples.Attach(entity);
                context.Samples.Remove(entity);
                context.SaveChanges();
            }
        }

        public Sample Save(Sample entity)
        {
            using (var context = Context.GetDb())
            {
                context.Samples.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
