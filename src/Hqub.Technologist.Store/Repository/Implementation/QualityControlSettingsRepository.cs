﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class QualityControlSettingsRepository : IQualityControlSettingsRepository
    {
        public QualitySetting Get(Guid id)
        {
            return First(e => e.Id == id);
        }

        public List<QualitySetting> List()
        {
            using (var context = Context.GetDb())
            {
                return context.QualitySettings.OrderBy(c=>c.Name).ToList();
            }
        }

        public List<QualitySetting> List(int count, int offset)
        {
            using (var context = Context.GetDb())
            {
                return context.QualitySettings.OrderBy(c => c.Name).Skip(offset).Take(count).ToList();
            }
        }

        public List<QualitySetting> Where(Func<QualitySetting, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.QualitySettings.Where(expression).ToList();
            }
        }

        public QualitySetting First(Func<QualitySetting, bool> expression)
        {
            return Where(expression).First();
        }

        public QualitySetting FirstOrDefault(Func<QualitySetting, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(QualitySetting entity)
        {
            using (var context = Context.GetDb())
            {
                context.QualitySettings.Attach(entity);
                context.QualitySettings.Remove(entity);
                context.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public QualitySetting Save(QualitySetting entity)
        {
            using (var context = Context.GetDb())
            {
                context.QualitySettings.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }

    }
}
