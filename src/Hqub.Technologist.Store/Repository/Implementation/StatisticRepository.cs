﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class StatisticRepository : IStatisticRepository
    {
        public Statistic Get(Guid id)
        {
            using (var contex = Context.GetDb())
            {
                return contex.Statistics.First(s => s.Id == id);
            }
        }

        public Statistic Get(Guid mark, string markClass, int month, int year)
        {
            using (var context = Context.GetDb())
            {
                return
                    context.Statistics.FirstOrDefault(
                        x => x.MarkId == mark && x.MarkClass == markClass && x.Month == month && x.Year == year);
            }
        }

        public string GetMarkName(float density, int month, int year)
        {
            var mark = GetStatisticMark(density, month, year);

            return mark.Item1;
        }

        public string GetMarkClassName(float strenght, int month, int year)
        {
            var markClass = GetStatisticMarkClass(strenght, month, year);
            return markClass.Item1;
        }

        public Tuple<string, float> GetStatisticMark(float density, int month, int year)
        {
            using (var context = Context.GetDb())
            {
                var statictic =
                    context.Statistics.Where(x => x.Month == month && x.Year == year && x.DensityRequired >= density)
                        .OrderBy(x => x.DensityRequired)
                        .FirstOrDefault();

                return statictic != null
                    ? new Tuple<string, float>(statictic.Brand.Name, (float) statictic.DensityRequired)
                    : null;
            }
        }

        public Tuple<string, float> GetStatisticMarkClass(float strenght, int month, int year)
        {
            using (var context = Context.GetDb())
            {
                var statictic =
                    context.Statistics.Where(x => x.Month == month && x.Year == year && x.StrengthRequired >= strenght)
                        .OrderBy(x => x.StrengthRequired)
                        .FirstOrDefault();

                return statictic != null
                    ? new Tuple<string, float>(statictic.MarkClass, (float) statictic.StrengthRequired)
                    : null;
            }
        }

        public List<Statistic> List()
        {
            using (var context = Context.GetDb())
            {
                return context.Statistics.ToList();
            }
        }

        public List<Statistic> List(int count, int offset)
        {
            using (var context = Context.GetDb())
            {
                return context.Statistics.Skip(offset).Take(count).ToList();
            }
        }

        public List<Statistic> Where(Func<Statistic, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.Statistics.Where(expression).ToList();
            }
        }

        public Statistic First(Func<Statistic, bool> expression)
        {
            return Where(expression).First();
        }

        public Statistic FirstOrDefault(Func<Statistic, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(Statistic entity)
        {
            using (var context = Context.GetDb())
            {
                context.Statistics.Attach(entity);
                context.Statistics.Remove(entity);
                context.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public Statistic Save(Statistic entity)
        {
            using (var context = Context.GetDb())
            {
                context.Statistics.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
