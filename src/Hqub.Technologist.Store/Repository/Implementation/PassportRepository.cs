﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class PassportRepository : IPassportRepository
    {
        public Passport Get(Guid id)
        {
            using (var contex = Context.GetDb())
            {
                return contex.Passports.First(p => p.Id == id);
            }
        }

        public List<Passport> List()
        {
            using (var context = Context.GetDb())
            {
                return context.Passports.ToList();
            }
        }

        public List<Passport> List(int count, int offset)
        {
            using (var context = Context.GetDb())
            {
                return context.Passports.Skip(offset).Take(count).ToList();
            }
        }

        public List<Passport> Where(Func<Passport, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.Passports.Where(expression).ToList();
            }
        }

        public Passport First(Func<Passport, bool> expression)
        {
            return Where(expression).First();
        }

        public Passport FirstOrDefault(Func<Passport, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(Passport entity)
        {
            using (var context = Context.GetDb())
            {
                context.Passports.Attach(entity);
                context.Passports.Remove(entity);
                context.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public Passport Save(Passport entity)
        {
            using (var context = Context.GetDb())
            {
                context.Passports.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
