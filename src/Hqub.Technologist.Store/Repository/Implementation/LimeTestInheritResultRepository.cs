﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class LimeTestInheritResultRepository : ILimeTestInheritResultRepository
    {
        public LimeTestInheritResult Get(Guid id)
        {
            using (var contex = Context.GetDb())
            {
                return contex.LimeTestInheritResults.First(brand => brand.Id == id);
            }
        }

        public List<LimeTestInheritResult> List()
        {
            using (var context = Context.GetDb())
            {
                return context.LimeTestInheritResults.OrderBy(c => c.DateTest).ToList();
            }
        }

        public List<LimeTestInheritResult> List(int count, int offset)
        {
            using (var context = Context.GetDb())
            {
                return context.LimeTestInheritResults.OrderBy(c => c.DateTest).Skip(offset).Take(count).ToList();
            }
        }

        public List<LimeTestInheritResult> Where(Func<LimeTestInheritResult, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.LimeTestInheritResults.Where(expression).ToList();
            }
        }

        public LimeTestInheritResult First(Func<LimeTestInheritResult, bool> expression)
        {
            return Where(expression).First();
        }

        public LimeTestInheritResult FirstOrDefault(Func<LimeTestInheritResult, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(LimeTestInheritResult entity)
        {
            using (var context = Context.GetDb())
            {
                context.LimeTestInheritResults.Attach(entity);
                context.LimeTestInheritResults.Remove(entity);
                context.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public LimeTestInheritResult Save(LimeTestInheritResult entity)
        {
            using (var context = Context.GetDb())
            {
                context.LimeTestInheritResults.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
