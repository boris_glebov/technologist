﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class PositionRepository : IPositionRepository
    {
        public Position Get(Guid id)
        {
            return First(p => p.Id == id);
        }

        public List<Position> List()
        {
            using (var context = Context.GetDb())
            {
                return context.Positions.OrderBy(c=>c.Name).ToList();
            }
        }

        public List<Position> List(int count, int offset = 0)
        {
            using (var context = Context.GetDb())
            {
                return context.Positions.OrderBy(c => c.Name).Skip(offset).Take(count).ToList();
            }
        }

        public List<Position> Where(Func<Position, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.Positions.Where(expression).ToList();
            }
        }

        public Position First(Func<Position, bool> expression)
        {
            return Where(expression).First();
        }

        public Position FirstOrDefault(Func<Position, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public void Delete(Position entity)
        {
            using (var context = Context.GetDb())
            {
                context.Positions.Attach(entity);
                context.Positions.Remove(entity);
                context.SaveChanges();
            }
        }

        public Position Save(Position entity)
        {
            using (var context = Context.GetDb())
            {
                context.Positions.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
