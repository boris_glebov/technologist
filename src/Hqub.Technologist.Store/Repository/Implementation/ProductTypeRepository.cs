﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class ProductTypeRepository : IProductTypeRepository
    {
        public ProductType Get(Guid id)
        {
            using (var contex = Context.GetDb())
            {
                return contex.ProductTypes.First(entity => entity.Id == id);
            }
        }

        public List<ProductType> List()
        {
            using (var context = Context.GetDb())
            {
                return context.ProductTypes.OrderBy(c=>c.Name).ToList();
            }
        }

        public List<ProductType> List(int count, int offset)
        {
            using (var context = Context.GetDb())
            {
                return context.ProductTypes.OrderBy(c => c.Name).Skip(offset).Take(count).ToList();
            }
        }

        public List<ProductType> Where(Func<ProductType, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.ProductTypes.Where(expression).ToList();
            }
        }

        public ProductType First(Func<ProductType, bool> expression)
        {
            return Where(expression).First();
        }

        public ProductType FirstOrDefault(Func<ProductType, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public ProductType Save(ProductType entity)
        {
            using (var context = Context.GetDb())
            {
                context.ProductTypes.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }

        public void Delete(ProductType entity)
        {
            using (var context = Context.GetDb())
            {
                context.ProductTypes.Attach(entity);
                context.ProductTypes.Remove(entity);
                context.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }
    }
}
