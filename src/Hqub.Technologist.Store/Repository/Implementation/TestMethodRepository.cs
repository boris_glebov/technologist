﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class TestMethodRepository : ITestMethodRepository
    {
        public TestMethod Get()
        {
            using (var context = Context.GetDb())
            {
                return context.TestMethods.OrderByDescending(x => x.CreateStamp).FirstOrDefault();
            }
        }

        public TestMethod Get(Guid id)
        {
            using (var contex = Context.GetDb())
            {
                return contex.TestMethods.First(m => m.Id == id);
            }
        }

        public List<TestMethod> List()
        {
            using (var context = Context.GetDb())
            {
                return context.TestMethods.ToList();
            }
        }

        public List<TestMethod> List(int count, int offset)
        {
            using (var context = Context.GetDb())
            {
                return context.TestMethods.Skip(offset).Take(count).ToList();
            }
        }

        public List<TestMethod> Where(Func<TestMethod, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.TestMethods.Where(expression).ToList();
            }
        }

        public TestMethod First(Func<TestMethod, bool> expression)
        {
            return Where(expression).First();
        }

        public TestMethod FirstOrDefault(Func<TestMethod, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public void Delete(TestMethod entity)
        {
            using (var context = Context.GetDb())
            {
                context.TestMethods.Attach(entity);
                context.TestMethods.Remove(entity);
                context.SaveChanges();
            }
        }

        public TestMethod Save(TestMethod entity)
        {
            using (var context = Context.GetDb())
            {
                context.TestMethods.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
