﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
   public class EmployeeRepository : IEmployeeRepository
    {
        public Employee Get(Guid id)
        {
            return FirstOrDefault(e => e.Id == id);
        }

        public List<Employee> List()
        {
            using (var context = Context.GetDb())
            {
                return context.Employees.OrderBy(x=>x.LastName).ToList();
            }
        }

        public List<Employee> List(int count, int offset = 0)
        {
            using (var context = Context.GetDb())
            {
                return context.Employees.OrderBy(x=>x.LastName).Skip(offset).Take(count).ToList();
            }
        }

        public List<Employee> Where(Func<Employee, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.Employees.Where(expression).ToList();
            }
        }

        public Employee First(Func<Employee, bool> expression)
        {
            return Where(expression).First();
        }

        public Employee FirstOrDefault(Func<Employee, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public void Delete(Employee entity)
        {
            using (var context = Context.GetDb())
            {
                context.Employees.Attach(entity);
                context.Employees.Remove(entity);
                context.SaveChanges();
            }
        }

        public Employee Save(Employee entity)
        {
            using (var context = Context.GetDb())
            {
                context.Employees.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
