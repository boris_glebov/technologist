﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using Hqub.Technologist.Store.Repository.Interfaces;
using ELEMDESC = System.Runtime.InteropServices.ELEMDESC;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class LotRepository : ILotRepository
    {
        public Lot Get(Guid id)
        {
            return First(lot => lot.Id == id);
        }

        public List<Lot> List()
        {
            using (var context = Context.GetDb())
            {
                return context.Lots.OrderByDescending(c => c.DateReview).ToList();
            }
        }

        public List<Lot> List(int count, int offset)
        {
            using (var context = Context.GetDb())
            {
                return context.Lots.OrderByDescending(c => c.DateReview).Skip(offset).Take(count).ToList();
            }
        }

        public List<Lot> Where(Func<Lot, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.Lots.Where(expression).ToList();
            }
        }

        public Lot First(Func<Lot, bool> expression)
        {
            return Where(expression).First();
        }

        public Lot FirstOrDefault(Func<Lot, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public void Delete(Lot entity)
        {
            using (var context = Context.GetDb())
            {
                context.Lots.Attach(entity);
                context.Lots.Remove(entity);
                context.SaveChanges();
            }
        }

        public Lot Save(Lot entity)
        {
            using (var context = Context.GetDb())
            {
                context.Lots.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
