﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class SampleBottleRepository : ISampleBottleRepository
    {
        public SampleBottle Get(Guid id)
        {
            return First(e => e.Id == id);
        }

        public List<SampleBottle> List()
        {
            using (var context = Context.GetDb())
            {
                return context.SampleBottles.ToList();
            }
        }

        public List<SampleBottle> List(int count, int offset = 0)
        {
            using (var context = Context.GetDb())
            {
                return context.SampleBottles.Skip(offset).Take(count).ToList();
            }
        }

        public List<SampleBottle> Where(Func<SampleBottle, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.SampleBottles.Where(expression).ToList();
            }
        }

        public SampleBottle First(Func<SampleBottle, bool> expression)
        {
            return Where(expression).First();
        }

        public SampleBottle FirstOrDefault(Func<SampleBottle, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public void Delete(SampleBottle entity)
        {
            using (var context = Context.GetDb())
            {
                context.SampleBottles.Attach(entity);
                context.SampleBottles.Remove(entity);
                context.SaveChanges();
            }
        }

        public SampleBottle Save(SampleBottle entity)
        {
            using (var context = Context.GetDb())
            {
                context.SampleBottles.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
