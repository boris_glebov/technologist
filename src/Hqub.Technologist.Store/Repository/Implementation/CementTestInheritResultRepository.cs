﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class CementTestInheritResultRepository: ICementTestInheritResultRepository
    {
        
        public CementTestInheritResult Get(Guid id)
        {
            return FirstOrDefault(e => e.Id == id);
        }

        public void Delete(CementTestInheritResult entity)
        {
            using (var context = Context.GetDb())
            {
                context.CementTestInheritResults.Attach(entity);
                context.CementTestInheritResults.Remove(entity);
                context.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public List<CementTestInheritResult> List()
        {
            using (var context = Context.GetDb())
            {
                return context.CementTestInheritResults.OrderBy(c => c.DateTest).ToList();
            }
        }

        public List<CementTestInheritResult> List(int count, int offset)
        {
            using (var context = Context.GetDb())
            {
                return context.CementTestInheritResults.OrderBy(c => c.DateTest).Skip(offset).Take(count).ToList();
            }
        }

        public List<CementTestInheritResult> Where(Func<CementTestInheritResult, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.CementTestInheritResults.Where(expression).ToList();
            }
        }

        public CementTestInheritResult First(Func<CementTestInheritResult, bool> expression)
        {
            return Where(expression).First();
        }

        public CementTestInheritResult FirstOrDefault(Func<CementTestInheritResult, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        
        public CementTestInheritResult Save(CementTestInheritResult entity)
        {
            using (var context = Context.GetDb())
            {
                context.CementTestInheritResults.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
