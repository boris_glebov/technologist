﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class NomenclatureRepository : INomenclatureRepository
    {
        public Nomenclature Get(Guid id)
        {
            return First(m => m.Id == id);
        }

        public List<Nomenclature> List()
        {
            using (var context = Context.GetDb())
            {
                return context.Nomenclatures.OrderBy(c => c.ProductName).ToList();
            }
        }

        public List<Nomenclature> List(int count, int offset = 0)
        {
            using (var context = Context.GetDb())
            {
                return context.Nomenclatures.OrderBy(c => c.ProductName).Skip(offset).Take(count).ToList();
            }
        }

        public List<Nomenclature> Where(Func<Nomenclature, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.Nomenclatures.Where(expression).ToList();
            }
        }

        public Nomenclature First(Func<Nomenclature, bool> expression)
        {
            return Where(expression).First();
        }

        public Nomenclature FirstOrDefault(Func<Nomenclature, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public void Delete(Nomenclature entity)
        {
            using (var context = Context.GetDb())
            {
                context.Nomenclatures.Attach(entity);
                context.Nomenclatures.Remove(entity);
                context.SaveChanges();
            }
        }

        public Nomenclature Save(Nomenclature entity)
        {
            using (var context = Context.GetDb())
            {
                context.Nomenclatures.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }

        public bool Exists(Guid id, string name)
        {
            using (var context = Context.GetDb())
            {
                return context.Nomenclatures.Any(x => x.Id != id && x.ProductName == name);
            }
        }
    }
}
