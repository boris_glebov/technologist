﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class MaterialRepository : IMaterialRepository
    {
        public Material Get(Guid id)
        {
            return First(m => m.Id == id);
        }

        public List<Material> List()
        {
            using (var context = Context.GetDb())
            {
                return context.Materials.OrderBy(c => c.Name).ToList();
            }
        }

        public List<Material> List(int count, int offset = 0)
        {
            using (var context = Context.GetDb())
            {
                return context.Materials.OrderBy(c=>c.Name).Skip(offset).Take(count).ToList();
            }
        }

        public List<Material> Where(Func<Material, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.Materials.Where(expression).ToList();
            }
        }

        public Material First(Func<Material, bool> expression)
        {
            return Where(expression).First();
        }

        public Material FirstOrDefault(Func<Material, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public void Delete(Material entity)
        {
            using (var context = Context.GetDb())
            {
                context.Materials.Attach(entity);
                context.Materials.Remove(entity);
                context.SaveChanges();
            }
        }

        public Material Save(Material entity)
        {
            using (var context = Context.GetDb())
            {
                context.Materials.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }

        public List<string> GetIzvestManufatories()
        {
            using (var context = Context.GetDb())
            {
                return context.Materials.Where(x => x.IzvestType != null).Select(x => x.Provider).ToList();
            }
        }

        public IzvestType GetIzvestType(string manufacture)
        {
            using (var context = Context.GetDb())
            {
                var m = context.Materials.First(x => x.IzvestType.HasValue && x.Provider == manufacture);
                return (IzvestType) m.IzvestType;
            }
        }
    }
}
