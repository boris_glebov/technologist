﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class BrandRepository : IBrandRepository
    {
        public Brand Get(Guid id)
        {
            using (var contex = Context.GetDb())
            {
                return contex.Brands.First(brand => brand.Id == id);
            }
        }

        public List<Brand> List()
        {
            using (var context = Context.GetDb())
            {
                return context.Brands.OrderBy(c => c.Name).ToList();
            }
        }

        public List<Brand> List(int count, int offset = 0)
        {
            using (var context = Context.GetDb())
            {
                return context.Brands.OrderBy(c => c.Name).Skip(offset).Take(count).ToList();
            }
        }

        public List<Brand> Where(Func<Brand, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.Brands.Where(expression).ToList();
            }
        }

        public Brand First(Func<Brand, bool> expression)
        {
            return Where(expression).First();
        }

        public Brand FirstOrDefault(Func<Brand, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public void Delete(Brand entity)
        {
            using (var context = Context.GetDb())
            {
                context.Brands.Attach(entity);
                context.Brands.Remove(entity);
                context.SaveChanges();
            }
        }

        public Brand Save(Brand entity)
        {
            using (var context = Context.GetDb())
            {
                context.Brands.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
