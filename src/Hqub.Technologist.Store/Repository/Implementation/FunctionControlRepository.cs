﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class FunctionControlRepository : IFunctionControlRepository
    {
        public FunctionalControl Get(Guid id)
        {
            using (var contex = Context.GetDb())
            {
                return contex.FunctionalControls.First(entity => entity.Id == id);
            }
        }

        public List<FunctionalControl> List()
        {
            using (var context = Context.GetDb())
            {
                return context.FunctionalControls.OrderBy(c => c.DateTest).ToList();
            }
        }

        public List<FunctionalControl> List(int count, int offset)
        {
            using (var context = Context.GetDb())
            {
                return context.FunctionalControls.OrderBy(c => c.DateTest).Skip(offset).Take(count).ToList();
            }
        }

        public List<FunctionalControl> Where(Func<FunctionalControl, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.FunctionalControls.Where(expression).ToList();
            }
        }

        public FunctionalControl First(Func<FunctionalControl, bool> expression)
        {
            return Where(expression).First();
        }

        public FunctionalControl FirstOrDefault(Func<FunctionalControl, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(FunctionalControl entity)
        {
            using (var context = Context.GetDb())
            {
                context.FunctionalControls.Attach(entity);
                context.FunctionalControls.Remove(entity);
                context.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }


        public FunctionalControl Save(FunctionalControl entity)
        {
            using (var context = Context.GetDb())
            {
                context.FunctionalControls.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
