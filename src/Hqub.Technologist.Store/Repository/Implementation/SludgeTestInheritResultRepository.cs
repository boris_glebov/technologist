﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Technologist.Store.Repository.Interfaces;
namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class SludgeTestInheritResultRepository :ISludgeTestInheritResultRepository
    {
        public SludgeTestInheritResult Get(Guid id)
        {
            return FirstOrDefault(e => e.Id == id);
        }

        public void Delete(SludgeTestInheritResult entity)
        {
            using (var context = Context.GetDb())
            {
                context.SludgeTestInheritResults.Attach(entity);
                context.SludgeTestInheritResults.Remove(entity);
                context.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public List<SludgeTestInheritResult> List()
        {
            using (var context = Context.GetDb())
            {
                return context.SludgeTestInheritResults.OrderBy(c => c.DateTest).ToList();
            }
        }

        public List<SludgeTestInheritResult> List(int count, int offset)
        {
            using (var context = Context.GetDb())
            {
                return context.SludgeTestInheritResults.OrderBy(c => c.DateTest).Skip(offset).Take(count).ToList();
            }
        }

        public List<SludgeTestInheritResult> Where(Func<SludgeTestInheritResult, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.SludgeTestInheritResults.Where(expression).ToList();
            }
        }

        public SludgeTestInheritResult First(Func<SludgeTestInheritResult, bool> expression)
        {
            return Where(expression).First();
        }

        public SludgeTestInheritResult FirstOrDefault(Func<SludgeTestInheritResult, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }


        public SludgeTestInheritResult Save(SludgeTestInheritResult entity)
        {
            using (var context = Context.GetDb())
            {
                context.SludgeTestInheritResults.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }
    }
}
