﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Hqub.Technologist.Store.Repository.Interfaces;

namespace Hqub.Technologist.Store.Repository.Implementation
{
    public class ProductTestRepository : IProductTestRepository
    {
        public ProductTest Get(Guid id)
        {
            return First(e => e.Id == id);
        }

        public ProductTest GetByMarkId(Guid productId)
        {
            return FirstOrDefault(m => m.Lot1.Id == productId);
        }

        public List<ProductTest> List()
        {
            using (var context = Context.GetDb())
            {
                return context.ProductTests.ToList();
            }
        }

        public List<ProductTest> List(int count, int offset = 0)
        {
            using (var context = Context.GetDb())
            {
                return context.ProductTests.Skip(offset).Take(count).ToList();
            }
        }

        public List<ProductTest> Where(Func<ProductTest, bool> expression)
        {
            using (var context = Context.GetDb())
            {
                return context.ProductTests.Where(expression).ToList();
            }
        }

        public ProductTest First(Func<ProductTest, bool> expression)
        {
            return Where(expression).First();
        }

        public ProductTest FirstOrDefault(Func<ProductTest, bool> expression)
        {
            return Where(expression).FirstOrDefault();
        }

        public void Delete(Guid id)
        {
            Delete(Get(id));
        }

        public void Delete(ProductTest entity)
        {
            using (var context = Context.GetDb())
            {
                context.ProductTests.Attach(entity);
                context.ProductTests.Remove(entity);
                context.SaveChanges();
            }
        }

        public ProductTest Save(ProductTest entity)
        {
            using (var context = Context.GetDb())
            {
                context.ProductTests.AddOrUpdate(entity);
                context.SaveChanges();

                return entity;
            }
        }

        public ProductTest Save(ProductTest entity, List<Sample> samples, List<SampleBottle> sampleBottles)
        {
            using (var context = Context.GetDb())
            {
                context.ProductTests.AddOrUpdate(entity);

                foreach (var sample in samples)
                    context.Samples.AddOrUpdate(sample);

                foreach (var sampleBottle in sampleBottles)
                    context.SampleBottles.AddOrUpdate(sampleBottle);

                context.SaveChanges();
              

                return entity;
            }
        }
    }
}
