﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Hqub.Technologist.Store.Repository
{
    public interface IRepositoryBase<T> : IRepositoryCommon
    {
        T Get(Guid id);

        List<T> List();
        List<T> List(int count, int offset);

        List<T> Where(Func<T, bool> expression);
        T First(Func<T, bool> expression);
        T FirstOrDefault(Func<T, bool> expression);

        void Delete(T entity);

        T Save(T entity);
    }
}
