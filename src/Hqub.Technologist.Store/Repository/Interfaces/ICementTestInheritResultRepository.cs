﻿namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface ICementTestInheritResultRepository : IRepositoryBase<CementTestInheritResult>
    {
    }
}
