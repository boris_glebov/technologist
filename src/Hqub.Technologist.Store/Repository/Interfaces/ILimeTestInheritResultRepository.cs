﻿namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface ILimeTestInheritResultRepository : IRepositoryBase<LimeTestInheritResult>
    {

    }
}
