﻿using System.Collections.Generic;

namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface IMaterialRepository : IRepositoryBase<Material>
    {
        List<string> GetIzvestManufatories();
        IzvestType GetIzvestType(string manufacture);
    }
}
