﻿namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface ISludgeTestInheritResultRepository : IRepositoryBase<SludgeTestInheritResult>
    {
    }
}
