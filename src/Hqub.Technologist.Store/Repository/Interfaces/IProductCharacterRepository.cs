﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface IProductCharacterRepository : IRepositoryBase<ProductCharacter>
    {
        ProductCharacter GetByMark(Guid mark);
        ProductCharacter Get();
    }
}
