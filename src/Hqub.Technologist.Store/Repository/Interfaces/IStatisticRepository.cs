﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface IStatisticRepository : IRepositoryBase<Statistic>
    {
        Statistic Get(Guid mark, string markClass, int month, int year);
        string GetMarkName(float density, int month, int year);
        string GetMarkClassName(float strenght, int month, int year);
        Tuple<string, float> GetStatisticMark(float density, int month, int year);
        Tuple<string, float> GetStatisticMarkClass(float strenght, int month, int year);
    }
}
