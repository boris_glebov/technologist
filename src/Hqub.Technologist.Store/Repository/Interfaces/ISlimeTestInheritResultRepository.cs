﻿namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface ISlimeTestInheritResultRepository : IRepositoryBase<SlimeTestInheritResult>
    {
    }
}
