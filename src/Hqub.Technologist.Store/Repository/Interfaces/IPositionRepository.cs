﻿namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface IPositionRepository : IRepositoryBase<Position>
    {
    }
}
