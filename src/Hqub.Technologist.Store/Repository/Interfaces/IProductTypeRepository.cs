﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface IProductTypeRepository : IRepositoryBase<ProductType>
    {
    }
}
