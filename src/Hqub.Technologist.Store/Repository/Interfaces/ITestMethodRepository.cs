﻿using System.Security.Cryptography.X509Certificates;

namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface ITestMethodRepository : IRepositoryBase<TestMethod>
    {
        TestMethod Get();
    }
}
