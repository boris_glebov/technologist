﻿namespace Hqub.Technologist.Store.Repository.Interfaces
{
   public interface IPassportRepository : IRepositoryBase<Passport>
   {
    }
}
