﻿namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface IFunctionControlRepository : IRepositoryBase<FunctionalControl>
    {
    }
}
