﻿using System;
using System.Collections.Generic;

namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface IProductTestRepository : IRepositoryBase<ProductTest>
    {
        ProductTest GetByMarkId(Guid productId);

        ProductTest Save(ProductTest entity, List<Store.Sample> samples, List<SampleBottle> sampleBottles);
    }
}
