﻿namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface IQualityControlSettingsRepository : IRepositoryBase<QualitySetting>
    {

    }
}
