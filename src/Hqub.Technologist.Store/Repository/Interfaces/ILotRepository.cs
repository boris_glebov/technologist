﻿namespace Hqub.Technologist.Store.Repository.Interfaces
{
    public interface ILotRepository : IRepositoryBase<Lot>
    {

    }
}
