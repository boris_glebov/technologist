//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hqub.Technologist.Store
{
    using System;
    using System.Collections.Generic;
    
    public partial class Statistic
    {
        public System.Guid Id { get; set; }
        public System.Guid MarkId { get; set; }
        public string MarkClass { get; set; }
        public double StrengthRequired { get; set; }
        public double DensityRequired { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string MarkName { get; set; }
    
        public virtual Brand Brand { get; set; }
    }
}
