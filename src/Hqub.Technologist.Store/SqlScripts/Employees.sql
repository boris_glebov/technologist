﻿CREATE TABLE [dbo].[Employees] (
    [Id]         UNIQUEIDENTIFIER NOT NULL,
    [FirstName]  NVARCHAR (100)   NULL,
    [LastName]   NVARCHAR (100)   NULL,
    [MiddleName] NVARCHAR (100)   NULL,
    [PositionId] UNIQUEIDENTIFIER NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_Employees_Positions] FOREIGN KEY ([PositionId]) REFERENCES [Positions]([Id])
);
