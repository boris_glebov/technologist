﻿CREATE TABLE [dbo].[SampleBottle]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [ProductTestId] UNIQUEIDENTIFIER NOT NULL, 
    [Order] INT NULL, 
    [Weight] FLOAT NULL, 
    [SampleWeight] FLOAT NULL, 
    [AfterDryingWeight] FLOAT NULL, 
    [Wetness] FLOAT NULL, 
    CONSTRAINT [FK_SampleBottle_ProductTest] FOREIGN KEY ([ProductTestId]) REFERENCES [ProductTest]([Id])
)