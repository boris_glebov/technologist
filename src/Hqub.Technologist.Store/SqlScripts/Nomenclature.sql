﻿CREATE TABLE [dbo].[Nomenclature] (
    [Id]           UNIQUEIDENTIFIER              NOT NULL,
    [ProductName]  NVARCHAR (100)   NULL,
    [BrandDensity] UNIQUEIDENTIFIER NULL,
    [Length]       FLOAT (53)       NULL,
    [Height]       FLOAT (53)       NULL,
    [Width]        FLOAT (53)       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Nomenclature_Brands] FOREIGN KEY ([BrandDensity]) REFERENCES [dbo].[Brands] ([Id])
);

