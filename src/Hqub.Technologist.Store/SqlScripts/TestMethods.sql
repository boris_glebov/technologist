﻿CREATE TABLE [dbo].[TestMethods] (
    [Id]                         UNIQUEIDENTIFIER NOT NULL,
    [SlakedLimeMethod]           INT              NOT NULL,
    [SpecificHeatCapacityMethod] INT              NOT NULL,
    [SampleType]                 INT              NOT NULL,
    [UnitsPress]                 INT NOT NULL,
    [PeriodQualityControl]       INT              NOT NULL,
    [CreateStamp]                DATETIME         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
