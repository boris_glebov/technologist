﻿CREATE TABLE [dbo].[Lots] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [LotNumber]   NVARCHAR (50)    NOT NULL,
    [DateReview]  DATETIME         NULL,
    [ProductType] UNIQUEIDENTIFIER NOT NULL,
    [Extended]    XML              NOT NULL,
    [Order] INT NOT NULL DEFAULT 0, 
    [AutoclaveNumber] INT NOT NULL DEFAULT 0, 
    [IsGeometrySizeAccepted] INT NOT NULL DEFAULT 0, 
    [DeffectType] NVARCHAR(MAX) NULL, 
    [DeffectIntensivity] INT NULL, 
    [Notes] NVARCHAR(MAX) NULL, 
    [Reviewer] UNIQUEIDENTIFIER NOT NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Lots_Brands] FOREIGN KEY ([ProductType]) REFERENCES [dbo].[Brands] ([Id])
);