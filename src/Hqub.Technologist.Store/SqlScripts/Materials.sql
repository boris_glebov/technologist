﻿CREATE TABLE [dbo].[Materials] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [Name]         NVARCHAR (100)   NOT NULL,
    [Provider]     NVARCHAR (100)   NOT NULL,
    [MaterialType] INT NOT NULL, 
	[IzvestType] INT NULL
    PRIMARY KEY CLUSTERED ([Id] ASC)
);
