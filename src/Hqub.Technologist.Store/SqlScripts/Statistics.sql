﻿CREATE TABLE [dbo].[Statistics] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [MarkId]             UNIQUEIDENTIFIER NOT NULL,
    [MarkClass]        NVARCHAR (50)    NOT NULL,
    [StrengthRequired] FLOAT (53)       DEFAULT ((0)) NOT NULL,
    [DensityRequired]  FLOAT (53)       DEFAULT ((0)) NOT NULL,
    [StartDate]        DATETIME         NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_Statistics_Brand] FOREIGN KEY ([MarkId]) REFERENCES [Brands]([Id])
);

