﻿CREATE TABLE [dbo].[Samples] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [ProductTestId]    UNIQUEIDENTIFIER NOT NULL,
    [Order]            INT              NULL,
    [OrderDescription] NVARCHAR (50)    NULL,
    [Weight]           FLOAT (53)       NULL,
    [Length]           FLOAT (53)       NULL,
    [Width]            FLOAT (53)       NULL,
    [Height]           FLOAT (53)       NULL,
    [BreakingLoad]     FLOAT (53)       NULL,
    [WetDensity]       FLOAT (53)       NULL,
    [DryDensity]       FLOAT (53)       NULL,
    [StrenghtLimit]    FLOAT (53)       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_Samples_ProductTest] FOREIGN KEY ([ProductTestId]) REFERENCES [ProductTest]([Id]) 
);