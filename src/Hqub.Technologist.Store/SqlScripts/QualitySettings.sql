﻿CREATE TABLE [dbo].[QualitySettings]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Unit] UNIQUEIDENTIFIER NULL, 
    [IsDefault] BIT NOT NULL DEFAULT 0, 
    [MinValue] FLOAT NOT NULL DEFAULT 0, 
    [MaxValue] FLOAT NOT NULL DEFAULT 0, 
    [Mask] NVARCHAR(50) NULL, 
    [ControlType] INT NOT NULL, 
    CONSTRAINT [FK_QualitySettings_Units] FOREIGN KEY ([Unit]) REFERENCES [Units]([Id])
)