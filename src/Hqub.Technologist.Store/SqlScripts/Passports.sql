﻿CREATE TABLE [dbo].[Passports]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [LotNumber] NVARCHAR(50) NOT NULL, 
    [DateRelease] DATETIME NULL, 
    [NomenclatureId] UNIQUEIDENTIFIER NOT NULL, 
    [StrenghtLimit] FLOAT NOT NULL DEFAULT 0, 
    [AverageDensity] FLOAT NOT NULL DEFAULT 0, 
    [Wetness] FLOAT NOT NULL DEFAULT 0, 
    [ClassStrenghtName] NVARCHAR(50) NOT NULL, 
    [RequiredStrenght] FLOAT NOT NULL DEFAULT 0, 
    [VariationStrenght] FLOAT NOT NULL DEFAULT 0, 
    [MarkName] NVARCHAR(50) NOT NULL, 
    [ThermalConductivity] FLOAT NOT NULL DEFAULT 0, 
    [DryingShrinkage] FLOAT NOT NULL DEFAULT 0, 
    [SpecificActivity] FLOAT NOT NULL DEFAULT 0, 
    [Notes] FLOAT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_Pasports_ToTable] FOREIGN KEY ([NomenclatureId]) REFERENCES [Nomenclature]([Id])
)
