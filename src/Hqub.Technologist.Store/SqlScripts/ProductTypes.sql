﻿CREATE TABLE [dbo].[ProductTypes]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(100) NOT NULL, 
    [Note] NVARCHAR(MAX) NULL
)
