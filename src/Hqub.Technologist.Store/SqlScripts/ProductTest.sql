﻿CREATE TABLE [dbo].[ProductTest] (
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [Lot]               UNIQUEIDENTIFIER NOT NULL,
    [TestDate]          DATETIME         NULL,
    [User]              UNIQUEIDENTIFIER NOT NULL,
    [WetDensityAverage] FLOAT (53)       NULL,
    [DryDensityAverage] FLOAT (53)       NULL,
    [MarkDensity]       UNIQUEIDENTIFIER NULL,
    [DensityVariation]  FLOAT (53)       NULL,
    [MarkStrenght]      FLOAT (53)       NULL,
    [StrenghtVariation] FLOAT (53)       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_ProductTest_Employees] FOREIGN KEY ([User]) REFERENCES [Employees]([Id]), 
    CONSTRAINT [FK_ProductTest_Brands] FOREIGN KEY ([MarkDensity]) REFERENCES [Brands]([Id]), 
    CONSTRAINT [FK_ProductTest_Lots] FOREIGN KEY ([Lot]) REFERENCES [Lots]([Id])
);

