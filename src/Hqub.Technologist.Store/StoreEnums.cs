﻿using System.ComponentModel;

namespace Hqub.Technologist.Store
{
    /// <summary>
    /// Тип материала
    /// </summary>
    public enum MaterialTypeEnum
    {
        Ash, // Зола-унос
        Ash2, // Зола отвал
        Izvest, // Известь
        Sand, // Песок
        Portland, // Портланд
        Cement // Цемент

    }

    /// <summary>
    /// Тип извести
    /// </summary>
    public enum IzvestType
    {
        [Description("Кальцевая")]
        Calcium,        
        [Description("Магнивая")]
        Magnium         
    }

    /// <summary>
    /// Методика оценки гашения извести
    /// </summary>
    public enum SlakedLimeMethodType
    {
        [Description("По ГОСТ22688-77")]
        GOST22688,
        [Description("Т60")]
        T60
    }

    /// <summary>
    /// Методика оценки удельной поверхности
    /// </summary>
    public enum SpecificHeatCapacityType
    {
        [Description("По методу Товарова")]
        TovarovMethod, 
        [Description("Другая")]
        Other               
    }

    /// <summary>
    /// Типы образца для определения прочности АГБ
    /// </summary>
    public enum SampleType
    {
        [Description("7,07 * 7,07 * 7,07")]
        Type07,
        [Description("10 * 10 * 10")]
        Type10,
        [Description("15 15 15")]
        Type15
    }

    /// <summary>
    /// Единицы измерения давления пресса
    /// </summary>
    public enum UnitType
    {
        [Description("кгс")]
        Kgf,
        [Description("кN")]
        kN,
        [Description("МПа")]
        MPa
    }

    /// <summary>
    /// Периодичность контроля
    /// </summary>
    public enum PeriodQualityControlType
    {
        [Description("Каждый массив")]
        Every,      
        [Description("Автоклав")]
        Autoclave,
        [Description("Партия")]
        Party
    }
    
    /// <summary>
    /// Вид контроля
    /// </summary>
    public enum QualityControlType
    {
        [Description("Качество массивов")]
        QualityArray,
        [Description("Параметры процесса")]
        FunctionalControl
    }
}
