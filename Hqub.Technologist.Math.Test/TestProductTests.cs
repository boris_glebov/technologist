﻿using System;
using System.Dynamic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hqub.Technologist.Math.Test
{
    [TestClass]
    public class TestProductTests
    {
        [TestMethod]
        public void GetHumidityTest()
        {
            Assert.AreEqual(0,TestProductMath.GetHumidity(0, 0, 0));
            //Assert.Equals(0, TestProductMath.GetHumidity(28.25f, 20.00f, 43.60f));
            //Assert.AreEqual(30.29, System.Math.Round(TestProductMath.GetHumidity(28.25f, 20.00f, 43.60f), 2));
        }

        [TestMethod]
        public void GetWetDensity()
        {
            //Assert.AreEqual(0, TestProductMath.GetWetDensity(0, 0, 0,0));

            Assert.AreEqual(557.83, TestProductMath.GetWetDensity(10.02f, 9.97f,10.04f,559.50f ));
        }

        [TestMethod]
        public void GetDryDensity()
        {
            //Assert.AreEqual(0,TestProductMath.GetDryDensity(0, 0, 0,0));

            //Assert.AreEqual(428.14, TestProductMath.GetDryDensity(557.83f,30.29f));
        }
    }
}
